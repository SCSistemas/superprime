<?php
if(!isset($_SESSION))
{
    session_start();
}
if (empty($_SESSION['userid'])){
    $redir = "Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/index.php";
    header($redir);
}