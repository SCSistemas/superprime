<?php

if(!isset($_SESSION))
{
    session_start();
    //Para Probar sin hacer Login
    $_SESSION['userid']=1;
}

include '../config/definitions.php';
$categoria = $_GET['categoria'];
$nombrecategoria = $_GET['nombrecateg'];
$subCategoria = $_GET['subcategoria'];
$smarca = $_GET['smarca'];
$nombre = $_GET['nombre'];


$desde = $_GET['desde']== '' ? 1 : $_GET['desde'];
$hasta = $_GET['hasta']== '' ? 10 : $_GET['hasta'];
$oferta = $_GET['oferta'];
$prime = $_GET['prime'];
$descripcion = $_GET['descripcion'];
$rango = explode(";", $_GET['rango']);

$ch = curl_init();
//Obtener Categorias
$url = $urlWS.'service=productoservices&metodo=ObtenerCategorias';
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$categoriasSlider = json_decode($resultData, true);

//Obtener Subcategorias
$url = $urlWS.'service=productoservices&metodo=ObtenerSubCategorias&Categoria='.$categoria;
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$Subcategorias = json_decode($resultData, true);

/** Obtener marcas**/
$url = $urlWS.'service=productoservices&metodo=ObtenerMarcasListado&p_subcategorias='.$subCategoria;
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$marcas = json_decode($resultData, true);


/** Obtener Productos **/

$url = $urlWS.'service=productoservices&metodo=ObtenerListadoProductos&p_id_categoria='.$categoria.'&p_id_subcategoria='.$subCategoria.'&p_number='.$desde.'&p_num_re='.$hasta.'&p_marca='.$smarca.'&p_minimo='.$rango[0].'&p_maximo='.$rango[1].'&p_oferta='.$oferta.'&p_prime='.$prime.'&p_nombre='.$nombre.'&p_id_usuario='.$_SESSION['userid'];
//echo $url;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
$resultData = curl_exec($ch);
$listaProductos = json_decode($resultData, true);

$minimo =$rango[0]== '' ? $listaProductos['Minimo'] : $rango[0];
$maximo =$rango[1]== '' ? $listaProductos['Maximo'] : $rango[1];

curl_close($ch);
include '../views/product-list.php';
?>