<?php
include '../config/definitions.php';

$ch = curl_init();
$url = $urlWS.'service=productoservices&metodo=ObtenerCategorias';
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$categoriasSlider = json_decode($resultData, true);
curl_close($ch);
include '../views/index.php';
?>