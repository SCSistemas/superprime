<?php
include '../config/definitions.php';
if(!isset($_SESSION))
{
    session_start();
}
    $body["p_id_user"]=$_SESSION['userid'];
    $body["p_cod_producto"]=$_GET['codProd'];
    $body["p_cod_color"]=$_GET['codColor'];
    $body["p_cantidad"]=$_GET['cantidad'];
    $body["p_cod_talla"]=$_GET['codTalla'];
    $url = $urlWS.'service=productoservices&metodo=CrearCompraExpressLogin';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));

    $result = json_decode(curl_exec($ch), true);

curl_close($ch);

if ($result['success'] == 1){
    echo "<script>window.location='https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/compraExpressContinue.php?cod_pedido=".$result['cod_pedido']."&id_user=".$result['id_user']."'</script>";
    //$redir = "Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/compraExpressContinue.php?cod_pedido=".$result['cod_pedido']."&id_user=".$result['id_user'];
    //header($redir);
}

?>