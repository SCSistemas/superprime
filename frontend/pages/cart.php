<?php
include '../config/definitions.php';
if(!isset($_SESSION))
{
    session_start();
    //Para Probar sin hacer Login
    $_SESSION['userid']=1;
}

/** Obtener Carro **/
$ch = curl_init();
$url = $urlWS.'service=userservices&metodo=ObtenerCarrito&p_id_usuario='.$_SESSION['userid'];
//echo $url;
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
$resultData = curl_exec($ch);
$listaProductos = json_decode($resultData, true);
curl_close($ch);

include '../views/cart.php';
?>