<?php

include '../config/definitions.php';

if(!isset($_SESSION))
{
    session_start();
}


if (!empty($_SESSION['userid'])) {
    $body["p_id_user"]=$_SESSION['userid'];
    $body["p_cod_producto"]=$_GET['codProd'];
    $body["p_cod_color"]=$_GET['codColor'];
    $body["p_cantidad"]=$_GET['cantidad'];
    $body["p_cod_talla"]=$_GET['codTalla'];
    $url = $urlWS.'service=productoservices&metodo=CrearCompraExpressLogin';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));

    $result = json_decode(curl_exec($ch), true);
}else{
$body['p_tipoid']=$_POST['tipoid'];
$body['p_numid']=$_POST['numid'];
$body['p_nombre_persona']=$_POST['nombre'];
$body['p_correo']=$_POST['p_correo'];

$body['p_estado']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['estadof']:$_POST['estado'];
$body['p_ciudad']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['ciudadf']:$_POST['ciudad'];
$body['p_municipio']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['municipiof']:$_POST['municipio'];
$body['p_parroquia']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['parroquiaf']:$_POST['parroquia'];
$body['p_postal']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['codigoPostalf']:$_POST['codigoPostal'];
$body['p_calle']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['calle1f']:$_POST['calle1'];
$body['p_calledos']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['calle2f']:$_POST['calle2'];
$body['p_tipovi']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['tipoViviendaf']:$_POST['tipoVivienda'];
$body['p_nombre']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['nombrecf']:$_POST['nombrec'];
$body['p_piso']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['pisof']:$_POST['piso'];
$body['p_apto']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?$_POST['apartamentof']:$_POST['apartamento'];
$body['p_telcel']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?($_POST['tcelf']=='('?'':$_POST['tcelf']):($_POST['tcel']=='('?'':$_POST['tcel']);
$body['p_telhab']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?($_POST['thabf']=='('?'':$_POST['thabf']):($_POST['thab']=='('?'':$_POST['thab']);
$body['p_telofi']=($_POST['envio']==''|| $_POST['envio']=='direccion1')?($_POST['tofif']=='('?'':$_POST['tofif']):($_POST['tofi']=='('?'':$_POST['tofi']);

$body['p_estadof']=$_POST['estadof'];
$body['p_ciudadf']=$_POST['ciudadf'];
$body['p_municipiof']=$_POST['municipiof'];
$body['p_parroquiaf']=$_POST['parroquiaf'];
$body['p_postalf']=$_POST['codigoPostalf'];
$body['p_callef']=$_POST['calle1f'];
$body['p_calledosf']=$_POST['calle2f'];
$body['p_tipovif']=$_POST['tipoViviendaf'];
$body['p_nombref']=$_POST['nombrecf'];
$body['p_pisof']=$_POST['pisof'];
$body['p_aptof']=$_POST['apartamentof'];
$body['p_telcelf']=$_POST['tcelf']=='('?'':$_POST['tcelf'];
$body['p_telhabf']=$_POST['thabf']=='('?'':$_POST['thabf'];
$body['p_telofif']=$_POST['tofif']=='('?'':$_POST['tofif'];

$body["p_cod_producto"]=$_POST['cod_producto'];
$body["p_cod_color"]=$_POST['cod_color'];
$body["p_cantidad"]=$_POST['cantidad'];
$body["p_cod_talla"]=$_POST['cod_talla'];
$body["p_id_dir_f"]=$_POST['p_id_dir_f'];
$body["p_id_dir_e"]=$_POST['p_id_dir_e'];
$body["p_id_user"]=$_POST['p_id_user'];

$ch = curl_init();
$url = $urlWS.'service=productoservices&metodo=CrearCompraExpress';
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));

$result = json_decode(curl_exec($ch), true);

}
curl_close($ch);

if ($result['success'] == 1){
echo "<script>window.location='https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/compraExpressContinue.php?cod_pedido=".$result['cod_pedido']."&id_user=".$result['id_user']."'</script>";
    //$redir = "Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/compraExpressContinue.php?cod_pedido=".$result['cod_pedido']."&id_user=".$result['id_user'];
    //header($redir);
}

?>

