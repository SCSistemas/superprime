$(document).ready(function() {

    var templatecarusel = Handlebars.getTemplate('../views/templates/carusel');
    $("#light-slider").lightSlider({
        item: 3,
        autoWidth: true,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 11,
        pauseOnHover: true,

        addClass: '',
        mode: "slide",
        useCSS: true,
        cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
        easing: 'linear', //'for jquery animation',////

        speed: 400, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,

        keyPress: false,
        controls: true,
        prevHtml: '',
        nextHtml: '',

        rtl: false,
        adaptiveHeight: true,

        vertical: false,
        verticalHeight: 500,
        vThumbWidth: 100,

        thumbItem: 4,
        pager: false,
        gallery: false,
        galleryMargin: 5,
        thumbMargin: 5,
        currentPagerPosition: 'middle',

        enableTouch: true,
        enableDrag: true,
        freeMove: true,
        swipeThreshold: 40,

        responsive: [],

    });

    $('ul li').click(function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codColor = $(this).attr('data-CodColor');
        var codTalla=$("#s-talla"+codProducto).val();
        devuelvePrecio(codProducto,codColor);
        devuelveTallas(codProducto,codColor);
        $('#colorselec-'+codProducto).val(codColor);
        devuelveInventario(codProducto,codColor,codTalla);
        devuelveImagenesColor(codProducto,codColor);

    });

    $('.select').off("change").on("change", function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codColor= $('#colorselec-'+codProducto).val();
        var codTalla= $(this).val();
        devuelveInventario(codProducto,codColor,codTalla);

    });



    devuelvePrecio=function(codProducto,codColor){
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerPrecioProducto&p_cod_producto="+codProducto+"&p_cod_color="+codColor,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $('#id-precio-'+codProducto).empty();
                $('#id-precio-'+codProducto).append(ajaxResponse.precio);
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    } ;

    devuelveInventario=function(codProducto,codColor,codTalla){
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerInventarioTallaColor&p_cod_producto="+codProducto+"&p_cod_color="+codColor+"&p_cod_talla="+codTalla,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $('#disponible-'+codProducto).val(ajaxResponse.total);
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    } ;

    marcaFavorito=function(codProducto,codColor,codUsuario,servicio) {
        var parametros={
            'p_cod_producto':codProducto,
            'p_id_user':codUsuario
        };
        $.ajax({
                type: "POST",
                url: "../../admin/index.php?service=userservices&metodo="+servicio,
                data:JSON.stringify(parametros),
                contentType : "application/json",
                success: function (respuesta) {
                    var ajaxResponse = $.parseJSON(respuesta);
                    if(ajaxResponse.success=='1'){
                        mostrarMensajeError('Éxito', ajaxResponse.msg, 'INFO');
                        devuelveCantidadCarrito(codUsuario);
                    }else{
                        mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');
                    }

                },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
            }
        )

    };

    devuelveTallas=function(codProducto,codColor){
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerTallaColor&p_cod_producto="+codProducto+"&p_cod_color="+codColor,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $.each(ajaxResponse,function(v,k){
                    $("#s-talla"+codProducto).empty();
                    $('<option>').val(k.CodMarca).text(k.DescMarca).appendTo('#s-marca');
                });
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    } ;
    devuelveImagenesColor=function(codProducto,codColor){
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerImagenesColor&p_cod_producto="+codProducto+"&p_cod_color="+codColor,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $("#div-carousel-"+codProducto).empty();
                $("#div-carousel-"+codProducto).append(templatecarusel(ajaxResponse));
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    } ;



    $(".favorito").off("change").on("change", function() {
        var servicio=$(this).is(":checked")?"InsertarDeseo":"EliminarDeseo";
        var codProducto = $(this).attr('data-CodProducto');
        var codColor= $('#colorselec-'+codProducto).val();
        var codUsuario= 1;//Hay que meter el usuario que este conectado;
        marcaFavorito(codProducto,codColor,codUsuario,servicio);
    });



 $("#buscar").click(function() {
        var oferta=$('#chk-oferta').is(":checked")?"S":"";
        var prime=$('#chk-prime').is(":checked")?"S":"";
        $("#listado").empty();
        $("#listado").prepend('<div class="loading-indication"><img src="../images/ajax-loader.gif" /> Por favor espere...</div>');
        window.location.href="product-list.php?categoria="+$('#categoria').val()+"&subcategoria="+$('#s-subcategoria').val()+"&nombrecateg="+$('#nombrecateg').val()+"&smarca="+$('#s-marca').val()+"&rango="+$('#s-precios').val()+"&oferta="+oferta+"&prime="+prime+"&descripcion="+$('#descripcion').val()+"&nombre="+$('#nombre').val();


    });

    $(".buyNow").click(function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla= $("#s-talla-"+codProducto).val();
        var codColor= $('#colorselec-'+codProducto).val();
        var cantidad= $('#cantidad-'+codProducto).val();
        var disponible = $('#disponible-' + codProducto).val();
        if (jQuery.isEmpty(cantidad)){
            mostrarMensajeError('Error','Debe Colocar una cantidad','ERROR');
        }else if(cantidad<disponible){
            mostrarMensajeError('Error', 'La cantidad solicitada no está disponible', 'ERROR');
        }else{
            if($('#id_user').val()!=''){
                window.location.href = "process-CompraExpress.php?codProd=" + codProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad;
            }else {
                comprarAhora(codProducto, codTalla, codColor, cantidad);
            }
        }
    });

    $(".addCart").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#s-talla-" + codProducto).val();
        var codColor = $('#colorselec-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var disponible = $('#disponible-'+codProducto).val();
        var codUsuario = $('#id_user').val();
        if (jQuery.isEmpty(cantidad)) {
            mostrarMensajeError('Error', 'Debe colocar una cantidad', 'ERROR');
            return;
        }
        if(cantidad > disponible){
            mostrarMensajeError('Error', 'La cantidad solicitada no está disponible', 'ERROR');
        }else{
            if(codUsuario!=''){
                insertarEnCarro(codProducto, codColor,codTalla,cantidad,codUsuario);
            }else {
                window.location.href = "login.php?codProd=" + CodProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad+'&redirect=process-CompraExpress';
                //Si no está Logueado no se que hacer aún
                //comprarAhora(codProducto, codTalla, codColor, cantidad);
            }
        }

    });



});
