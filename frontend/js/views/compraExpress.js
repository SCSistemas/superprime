$(document).ready(function () {

    $("input[name=p_correo]").change(function () {
        devuelveDatosUsuarios($(this).val());
    });



    devuelveDatosUsuarios = function (usuario) {
        var dfd = jQuery.Deferred();
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=userservices&metodo=ObtenerDatosUsuarioExpress&p_usuario=" + usuario,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                 if(ajaxResponse.success==1){
                     $('#nombre').val(ajaxResponse.Usuario.Nombre);
                     $('#tipoid').val(ajaxResponse.Usuario.TipoId);
                     $('#numid').val(ajaxResponse.Usuario.NumId);
                     $('#estadof').val(ajaxResponse.Usuario.Direcciones[1].CodEstado);
                     cargarCiudades('#estadof', '#ciudadf', ajaxResponse.Usuario.Direcciones[1].CodCiudad).then(
                         function () {
                             return cargarMunicipios('#estadof', '#ciudadf', '#municipiof', ajaxResponse.Usuario.Direcciones[1].CodMunicipio).then(
                                 function () {
                                     return cargarParroquias('#estadof', '#ciudadf', '#municipiof', '#parroquiaf', ajaxResponse.Usuario.Direcciones[1].CodParroquia);
                                 });
                         });

                     $('#codigoPostalf').val(ajaxResponse.Usuario.Direcciones[1].CodPostal);
                     $('#calle1f').val(ajaxResponse.Usuario.Direcciones[1].CalleUno);
                     $('#calle2f').val(ajaxResponse.Usuario.Direcciones[1].CalleDos);
                     $('#tipoViviendaf').val(ajaxResponse.Usuario.Direcciones[1].TipoVivienda);
                     $('#nombrecf').val(ajaxResponse.Usuario.Direcciones[1].Nombre);

                     $('#pisof').val(ajaxResponse.Usuario.Direcciones[1].Piso);
                     $('#apartamentof').val(ajaxResponse.Usuario.Direcciones[1].Apto);
                     $('#tcelf').val(ajaxResponse.Usuario.Direcciones[1].TelCel);
                     $('#thabf').val(ajaxResponse.Usuario.Direcciones[1].TelHab);
                     $('#tofif').val(ajaxResponse.Usuario.Direcciones[1].TelOfi);


                     $('#estado').val(ajaxResponse.Usuario.Direcciones[0].CodEstado);
                     cargarCiudades('#estado', '#ciudad', ajaxResponse.Usuario.Direcciones[0].CodCiudad).then(
                         function () {
                             return cargarMunicipios('#estado', '#ciudad', '#municipio', ajaxResponse.Usuario.Direcciones[0].CodMunicipio).then(
                                 function () {
                                     return cargarParroquias('#estado', '#ciudad', '#municipio', '#parroquia', ajaxResponse.Usuario.Direcciones[0].CodParroquia);
                                 });
                         });

                     $('#codigoPostal').val(ajaxResponse.Usuario.Direcciones[0].CodPostal);
                     $('#calle1').val(ajaxResponse.Usuario.Direcciones[0].CalleUno);
                     $('#calle2').val(ajaxResponse.Usuario.Direcciones[0].CalleDos);
                     $('#tipoVivienda').val(ajaxResponse.Usuario.Direcciones[0].TipoVivienda);
                     $('#nombrec').val(ajaxResponse.Usuario.Direcciones[0].Nombre);

                     $('#piso').val(ajaxResponse.Usuario.Direcciones[0].Piso);
                     $('#apartamento').val(ajaxResponse.Usuario.Direcciones[0].Apto);
                     $('#tcel').val(ajaxResponse.Usuario.Direcciones[0].TelCel);
                     $('#thab').val(ajaxResponse.Usuario.Direcciones[0].TelHab);
                     $('#tofi').val(ajaxResponse.Usuario.Direcciones[0].TelOfi);


                     $('#p_id_dir_f').val(ajaxResponse.Usuario.Direcciones[1].Id);
                     $('#p_id_dir_e').val(ajaxResponse.Usuario.Direcciones[0].Id);

                     $('#p_id_user').val(ajaxResponse.Usuario.IdUser);
                 }
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
                dfd.resolve(true);
            }
        })
        return dfd.promise();
    };


    $('form').submit(function (e) {
        if ($("#tcelf").val() == '(' && $("#thabf").val() == '(' && $("#tofif").val() == '(') {
            e.preventDefault();
            $("#telefonosf").html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        } else {
            $("#telefonosf").html("");
        }

        if($("input[name='envio']").val()=='direccion2'){
            if ($("#tcel").val() == '(' && $("#thab").val() == '(' && $("#tofi").val() == '(') {
                e.preventDefault();
                $("#telefonos").html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
            } else {
                $("#telefonos").html("");
            }
        }

    });

    $("input[name='envio']").on('ifChecked', function (event) {
        $('#direccion2').css('display', ($(this).val() === 'direccion2') ? 'block' : 'none');
        if($(this).val()=='direccion2'){
            $('#estado').attr('required','required');
            $('#codigoPostal').attr('required','required');
            $('#calle1').attr('required','required');
            $('#tipoVivienda').attr('required','required');
        }else{
            $('#estado').removeAttr('required');
            $('#codigoPostal').removeAttr('required');
            $('#calle1').removeAttr('required');
            $('#tipoVivienda').removeAttr('required');
        }

    });


    crearEventosTelefono("#tcelf","#thabf","#tofif","#telefonosf");
    crearEventosTelefono("#tcel","#thab","#tofi","#telefonos");
    crearEventosDireccion("#estadof","#ciudadf","#municipiof","#parroquiaf");
    crearEventosDireccion("#estado","#ciudad","#municipio","#parroquia");





});

crearEventosTelefono=function(tcel,thab,tofi,contenedor){
    $(tcel).on('keyup', function () {
        var value = $(this).val().length;
        if (value == 1) {
            $(contenedor).html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        } else {
            $(contenedor).html("");

        }
    });

    $(thab).on('keyup', function () {
        var value = $(this).val().length;
        if (value == 1) {
            $(contenedor).html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        } else {
            $(contenedor).html("");

        }
    });

    $(tofi).on('keyup', function () {
        var value = $(this).val().length;
        if (value == 1) {
            $(contenedor).html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        } else {
            $(contenedor).html("");

        }
    });




}

crearEventosDireccion=function(estado,ciudad,municipio,parroquia){

    $(estado).change(function () {
        //<![CDATA[
        $(ciudad).next("span").html("<img src='../images/ui-anim_basic_16x16.gif' title='' alt='' />");
        //]]>
        $(ciudad+"option:eq(0)").attr("selected", "selected");
        $(ciudad).attr("disabled", "disabled");
        $(municipio+"option:eq(0)").attr("selected", "selected");
        $(municipio).attr("disabled", "disabled");
        $(parroquia+" option:eq(0)").attr("selected", "selected");
        $(parroquia).attr("disabled", "disabled");
        cargarCiudades(estado,ciudad,null);



    });

    $(ciudad).change(function () {
        //<![CDATA[
        $(municipio).next("span").html("<img src='../images/ui-anim_basic_16x16.gif' title='' alt='' />");
        //]]>
        $(municipio+" option:eq(0)").attr("selected", "selected");
        $(municipio).attr("disabled", "disabled");
        $(parroquia+" option:eq(0)").attr("selected", "selected");
        $(parroquia).attr("disabled", "disabled");
        cargarMunicipios(estado,ciudad,municipio,null);


    });

    $(municipio).change(function () {
        //<![CDATA[
        $(parroquia).next("span").html("<img src='../images/ui-anim_basic_16x16.gif' title='' alt='' />");
        //]]>
        $(parroquia+" option:eq(0)").attr("selected", "selected");
        $(parroquia).attr("disabled", "disabled");
        cargarParroquias(estado,ciudad,municipio,parroquia,null);


    });
} ;

cargarCiudades = function (estado,ciudad,seleccion) {
    var dfd = jQuery.Deferred();
    $.getJSON("../ajax/cargarCiudades.php",
        {
            idestado: $(estado).val()
        },
        function (data) {
            if (data != '') {
                $("select"+ciudad).html(data);
                $(ciudad).next("span").html("");
                $(ciudad).append($('<option>', {
                    value: '',
                    text: '-Seleccione-'
                }));

                $.each(data, function (i, item) {
                    $(ciudad).prop('disabled', false);
                    $(ciudad).append($('<option>', {
                        value: item.CodCiudad,
                        text: item.DescCiudad
                    }));
                });
                if (seleccion != null)
                    $(ciudad).val(seleccion);


            } else {
                $(ciudad).removeAttr('required');
            }
            dfd.resolve(true);

        });
    return dfd.promise();
};
cargarMunicipios = function (estado,ciudad,municipio,seleccion) {
    var dfd = jQuery.Deferred();
    $.getJSON("../ajax/cargarMunicipios.php",
        {
            idestado: $(estado).val(),
            idciudad: $(ciudad).val()
        },
        function (data) {
            if (data != '') {
                $("select"+municipio).html(data);
                $(municipio).next("span").html("");
                $(municipio).append($('<option>', {
                    value: '',
                    text: '-Seleccione-'
                }));
                $.each(data, function (i, item) {
                    $(municipio).prop('disabled', false);
                    $(municipio).append($('<option>', {
                        value: item.CodMunicipio,
                        text: item.DescMunicipio
                    }));
                });
                if (seleccion != null)
                    $(municipio).val(seleccion);

            } else {
                $(municipio).removeAttr('required');
            }
            dfd.resolve(true);
        })
        return dfd.promise();
};
cargarParroquias = function (estado,ciudad,municipio,parroquia,seleccion) {
    var dfd = jQuery.Deferred();
    $.getJSON("../ajax/cargarParroquias.php",
        {
            idestado: $(estado).val(),
            idciudad: $(ciudad).val(),
            idmunicipio: $(municipio).val()
        },
        function (data) {
            if (data != '') {
                $("select"+parroquia).html(data);
                $("select"+parroquia).attr('data-validate', true);
                //$("#register").validator('update').validator('validate');
                $(parroquia).next("span").html("");
                $(parroquia).append($('<option>', {
                    value: '',
                    text: '-Seleccione-'
                }));

                $.each(data, function (i, item) {
                    $(parroquia).prop('disabled', false);
                    $(parroquia).append($('<option>', {
                        value: item.CodParroquia,
                        text: item.DescParroquia
                    }));
                });
                if (seleccion != null)
                    $(parroquia).val(seleccion);

            } else {
                $(parroquia).removeAttr('required');
            }
            dfd.resolve(true);
        })

    return dfd.promise();

};