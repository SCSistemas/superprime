$(document).ready(function() {
    var productos = [];
    devuelveInventario=function(codProducto,codColor,codTalla){
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerInventarioTallaColor&p_cod_producto="+codProducto+"&p_cod_color="+codColor+"&p_cod_talla="+codTalla,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $('#disponible-'+codProducto).val(ajaxResponse.total);
            },
            complete: function () {
                $("#myModal").modal({backdrop: false});
            }
        })

    } ;

    $(".borrar").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#talla-" + codProducto).val();
        var codColor = $('#color-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var codUsuario = $('#id_user').val();

        var dialog = new BootstrapDialog({
            message: function (dialogRef) {
                var $message = $('<div></div>');
                var $message2 = $('<div class="col-lg-12 text-center"> <h4>¿Está seguro de eliminar del carro?</h4></div>');
                var $si = $('<button class="btn btn-success btn-lg btn-block">Si</button>');
                var $no = $('<button class="btn btn-danger btn-lg btn-block">No</button>');
                $si.on('click', {dialogRef: dialogRef}, function (event) {
                    borrarEnCarro(codProducto, codColor,codTalla,cantidad,codUsuario);
                });
                $no.on('click', {dialogRef: dialogRef}, function (event) {
                    dialogRef.close();
                });

                $message.append($message2);
                $message.append($si);
                $message.append($no);

                return $message;
            },
            closable: true
        });
        dialog.realize();
        dialog.getModalHeader().hide();
        dialog.getModalFooter().hide();
        dialog.open();





    });

    borrarEnCarro = function (codProducto, codColor,codTalla,cantidad,codUsuario) {
        var parametros = {
            'p_cod_producto': codProducto,
            'p_cod_color': codColor,
            'p_cod_talla': codTalla,
            'p_cantidad': cantidad,
            'p_id_user': codUsuario
        };
        $.ajax({
                type: "POST",
                url: "../../admin/index.php?service=userservices&metodo=BorrarEnCarro",
                data: JSON.stringify(parametros),
                contentType: "application/json",
                success: function (respuesta) {
                    window.location.href="cart.php";

                },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
            }
        )

    }

    $(".buyNow").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#talla-" + codProducto).val();
        var codColor = $('#color-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var dialog = new BootstrapDialog({
                message: function (dialogRef) {
                   // var $message = $('<div>Producto:'+codProducto+' Talla:'+codTalla+' Color:'+codColor +' Cantidad:'+cantidad +' </div>');
                    var $message = $('<div></div>');
                    var $message2 = $('<div class="col-lg-12 text-center"> <h4>¿Está seguro de pagar este producto?</h4></div>');
                    var $si = $('<button class="btn btn-success btn-lg btn-block">Si</button>');
                    var $no = $('<button class="btn btn-danger btn-lg btn-block">No</button>');
                    $si.on('click', {dialogRef: dialogRef}, function (event) {
                        window.location.href="compraExpress.php?codProd="+CodProducto+"&codColor="+codColor+"&codTalla="+codTalla+"&cantidad="+cantidad;
                    });
                    $no.on('click', {dialogRef: dialogRef}, function (event) {
                        dialogRef.close();
                    });

                    $message.append($message2);
                    $message.append($si);
                    $message.append($no);

                    return $message;
                },
                closable: true
            });
            dialog.realize();
            dialog.getModalHeader().hide();
            dialog.getModalFooter().hide();
            dialog.open();
    });
    $(".acept").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#talla-" + codProducto).val();
        var codColor = $('#color-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var codUsuario = $('#id_user').val();
        var cantidadAnterior = $('#cantidadAnterior-'+codProducto).val();
        if(cantidad<=0){
            $('#cantidad-' + codProducto).val(cantidadAnterior);
            mostrarMensajeError('Error', 'La cantidad no puede ser menor o igual a 0', 'ERROR');
            return;
        }
        var dialog = new BootstrapDialog({
                message: function (dialogRef) {
                   // var $message = $('<div>Producto:'+codProducto+' Talla:'+codTalla+' Color:'+codColor +' Cantidad:'+cantidad +' </div>');
                    var $message = $('<div></div>');
                    var $message2 = $('<div class="col-lg-12 text-center"> <h4>¿Está seguro de modificar este producto?</h4></div>');
                    var $si = $('<button class="btn btn-success btn-lg btn-block">Si</button>');
                    var $no = $('<button class="btn btn-danger btn-lg btn-block">No</button>');
                    $si.on('click', {dialogRef: dialogRef}, function (event) {
                        editarEnCarro(codProducto,codColor,codTalla,cantidad,codUsuario,cantidadAnterior);
                        $('#divlcantidad-'+codProducto).empty();
                        $('#divlcantidad-'+codProducto).append(cantidad);
                        $("#botones-edit-" + codProducto).hide( "slow", function() {
                            $("#divcantidad-" + codProducto).hide("slow",function() {
                                $("#botones-princ-" + codProducto).show('slow');
                                $("#divlcantidad-" + codProducto).show('slow');
                            });

                        });
                        dialogRef.close();

                    });
                    $no.on('click', {dialogRef: dialogRef}, function (event) {
                        dialogRef.close();
                    });

                    $message.append($message2);
                    $message.append($si);
                    $message.append($no);

                    return $message;
                },
                closable: true
            });
            dialog.realize();
            dialog.getModalHeader().hide();
            dialog.getModalFooter().hide();
            dialog.open();
    });
    $(".edit").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        $("#botones-princ-" + codProducto).hide( "slow", function() {
            $("#divlcantidad-" + codProducto).hide("slow", function() {
                $("#botones-edit-" + codProducto).show('slow');
                $("#divcantidad-" + codProducto).show('slow');

            });

        });
    });
    $(".cancel").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        $('#cantidad-' + codProducto).val($('#cantidadAnterior-'+codProducto).val());
        $("#botones-edit-" + codProducto).hide( "slow", function() {
            $("#divcantidad-" + codProducto).hide("slow",function() {
                $("#botones-princ-" + codProducto).show('slow');
                $("#divlcantidad-" + codProducto).show('slow');
            });

        });
    });

    editarEnCarro = function (codProducto, codColor,codTalla,cantidad,codUsuario,cantidadAnterior) {
        var parametros = {
            'p_cod_producto': codProducto,
            'p_cod_color': codColor,
            'p_cod_talla': codTalla,
            'p_cantidad': cantidad,
            'p_id_user': codUsuario
        };
        $.ajax({
                type: "POST",
                url: "../../admin/index.php?service=userservices&metodo=EditarEnCarro",
                data: JSON.stringify(parametros),
                contentType: "application/json",
                success: function (respuesta) {
                    var ajaxResponse = $.parseJSON(respuesta);
                    if(ajaxResponse.success=='1'){
                        mostrarMensajeError('Éxito', ajaxResponse.msg, 'INFO');
                        productos.push(codProducto+'-'+codColor+'-'+codTalla+'-'+cantidad);
                        var pos = productos.indexOf(codProducto+'-'+codColor+'-'+codTalla+'-'+cantidadAnterior);
                        pos > -1 && productos.splice(pos, 1);

                    }else{
                        mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');
                    }

                },beforeSend: function () {
                    $.blockUI({
                        message: '<h4>Por favor Espere...</h4>'
                    });

                },
                complete: function () {
                    $.unblockUI();
                }
            }
        )

    }



    $('.seleccionar').on('ifChecked', function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#talla-" + codProducto).val();
        var codColor = $('#color-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        productos.push(codProducto+'-'+codColor+'-'+codTalla+'-'+cantidad);
        obtenerTotal();

        });
    $('.seleccionar').on('ifUnchecked', function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#talla-" + codProducto).val();
        var codColor = $('#color-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var pos = productos.indexOf(codProducto+'-'+codColor+'-'+codTalla+'-'+cantidad);
        pos > -1 && productos.splice(pos, 1)
        obtenerTotal();

        });

    $('#all')
        .on('ifChecked', function(event) {
             productos=[];
            $('.check').iCheck('check');
        })
        .on('ifUnchecked', function() {
             productos=[];
            $('.check').iCheck('uncheck');
        });

     $('#pagar-selec').off("click").on("click", function () {
         procesarCarro();
     });



    obtenerTotal = function () {
        var total=0;
        for (var i=0;i<productos.length;i++){
            producto = productos[i].split('-');
            CodProducto=producto[0];
            costo=parseFloat($('#costo-'+ CodProducto).val());
            total=total+costo;
        }
        $('#subtotal').hide( "slow", function() {
            $('#subtotal').empty();
            $('#subtotal').append('Bs. '+total);
            $('#subtotal').show('slow');
        });
    }

    procesarCarro = function () {
        var payload = {
            "productos": {"producto": []},
            "id_user": $('#id_user').val()};
        if(productos.length==0){
            mostrarMensajeError('Error', 'Debe seleccionar al menos un producto', 'ERROR');
        } else{
            for (var i=0;i<productos.length;i++){
                producto = productos[i].split('-');
                objRegistro = {};
                objRegistro.CodProducto = producto[0];
                objRegistro.CodColor=producto[1];
                objRegistro.CodTalla = producto[2];
                objRegistro.Cantidad = producto[3];
                payload.productos.producto[i]=objRegistro;
            }
            $.ajax({
                    type: "POST",
                    url: "../../admin/index.php?service=productoservices&metodo=ProcesarCarro",
                    data: JSON.stringify(payload),
                    contentType: "application/json",
                    success: function (respuesta) {
                        var ajaxResponse = $.parseJSON(respuesta);
                        if (ajaxResponse.success == '1')
                            window.location.href = "cart-continue.php?cod_pedido=" + ajaxResponse.cod_pedido+ "&id_user=" + $('#id_user').val();
                        else
                            mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');

                    },beforeSend: function () {
                        $.blockUI({
                            message: '<h4>Por favor Espere...</h4>'
                        });

                    },
                    complete: function () {
                        $.unblockUI();
                    }
                }
            )
        }
    }





});