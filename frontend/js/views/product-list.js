$(document).ready(function () {
    var templatecarusel = Handlebars.getTemplate('../views/templates/carusel');

    //devuelveInventario();

    $('ul li').click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codColor = $(this).attr('data-CodColor');
        var codTalla = $("#s-talla" + codProducto).val();
        devuelvePrecio(codProducto, codColor);
        devuelveTallas(codProducto, codColor);
        $('#colorselec-' + codProducto).val(codColor);
        devuelveInventario(codProducto, codColor, codTalla);
        devuelveImagenesColor(codProducto, codColor);

    });

    $('.select').off("change").on("change", function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codColor = $('#colorselec-' + codProducto).val();
        var codTalla = $(this).val();
        devuelveInventario(codProducto, codColor, codTalla);

    });


    devuelvePrecio = function (codProducto, codColor) {
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerPrecioProducto&p_cod_producto=" + codProducto + "&p_cod_color=" + codColor,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $('#id-precio-' + codProducto).empty();
                $('#id-precio-' + codProducto).append(ajaxResponse.precio);
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    };

    devuelveInventario = function (codProducto, codColor, codTalla) {
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerInventarioTallaColor&p_cod_producto=" + codProducto + "&p_cod_color=" + codColor + "&p_cod_talla=" + codTalla,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $('#disponible-' + codProducto).val(ajaxResponse.total);
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    };
    devuelveTallas = function (codProducto, codColor) {
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerTallaColor&p_cod_producto=" + codProducto + "&p_cod_color=" + codColor,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $.each(ajaxResponse, function (v, k) {
                    $("#s-talla" + codProducto).empty();
                    $('<option>').val(k.CodMarca).text(k.DescMarca).appendTo('#s-marca');
                });
            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    };
    devuelveImagenesColor = function (codProducto, codColor) {
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerImagenesColor&p_cod_producto=" + codProducto + "&p_cod_color=" + codColor,
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $("#div-carousel-" + codProducto).empty();
                $("#div-carousel-" + codProducto).append(templatecarusel(ajaxResponse));
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        })

    };

    marcaFavorito = function (codProducto, codColor, codUsuario, servicio) {
        var parametros = {
            'p_cod_producto': codProducto,
            'p_id_user': codUsuario
        };
        $.ajax({
                type: "POST",
                url: "../../admin/index.php?service=userservices&metodo=" + servicio,
                data: JSON.stringify(parametros),
                contentType: "application/json",
                success: function (respuesta) {
                    var ajaxResponse = $.parseJSON(respuesta);
                    if(ajaxResponse.success=='1'){
                        mostrarMensajeError('Éxito', ajaxResponse.msg, 'INFO');
                        devuelveCantidadCarrito(codUsuario);
                    }else{
                        mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');
                    }

                },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
            }
        )

    }


    $(".prime a.lprime").append("<span class='label'>Prime</span>");

    $('#s-subcategoria').off("change").on("change", function () {
        devuelveMarcas();
    });

    $(".favorito").off("change").on("change", function () {
        var servicio = $(this).is(":checked") ? "InsertarDeseo" : "EliminarDeseo";
        var codProducto = $(this).attr('data-CodProducto');
        var codColor = $('#colorselec-' + codProducto).val();
        var codUsuario = $('#id_user').val();//Hay que meter el usuario que este conectado;
        if(codUsuario!='')
        marcaFavorito(codProducto, codColor, codUsuario, servicio);
    });


    // Configuracion del slider de rango de precios en product list
    $("#s-precios").ionRangeSlider({
        type: "double",
        grid: false,
        min: $('#listaminimo').val(),
        max: $('#listamaximo').val(),
        from: $('#minimo').val(),
        to: $('#maximo').val(),
        prefix: "Bs."
    });

    devuelveMarcas = function () {
        $.ajax({
            type: "GET",
            url: "../../admin/index.php?service=productoservices&metodo=ObtenerMarcasListado&p_subcategorias=" + $('#s-subcategoria').val(),
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                $.each(ajaxResponse, function (v, k) {
                    $("#s-marca").empty();
                    $('<option>').val(k.CodMarca).text(k.DescMarca).appendTo('#s-marca');
                });


            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }

        })

    };




    $('.mg-space-init').mgSpace({
        // Breakpoints at which the accordian changes # of columns


        // Default selectors
        rowWrapper: ".mg-rows",
        row: ".mg-row",
        targetWrapper: ".mg-targets",
        target: ".mg-target",
        trigger: ".trigger",
        close: ".mg-close",

        // Default target padding top/bottom and row bottom margin
        rowMargin: 25, //25 Set to zero for gridless
        targetPadding: 20, // Padding top/bottom inside target gets divided by 2

        useHash: false, // Set to true for history
        useOnpageHash: false, // Set true for onpage history
        hashTitle: "#/item-", // Must include `#` hash symbol

        // MISC
        useIndicator: true

    });
    $(".pagination").bootpag({
        total: Math.ceil($('#total').val()),
        page: $('#desde').val(),
        maxVisible: $('#hasta').val()
    }).on("page", function (e, num) {
        e.preventDefault();
        var oferta = $('#chk-oferta').is(":checked") ? "S" : "";
        var prime = $('#chk-prime').is(":checked") ? "S" : "";
        $.blockUI({
            message: '<h4>Por favor Espere...</h4>'
        });
        window.location.href = "product-list.php?categoria=" + $('#categoria').val() + "&subcategoria=" + $('#s-subcategoria').val() + "&desde=" + num + "&hasta=" + $('#s-cantidad').val() + "&nombrecateg=" + $('#nombrecateg').val() + "&smarca=" + $('#s-marca').val() + "&rango=" + $('#s-precios').val() + "&oferta=" + oferta + "&prime=" + prime + "&descripcion=" + $('#descripcion').val() + "&nombre=" + $('#nombre').val();

    });

    $('#s-cantidad').off("change").on("change", function () {
        var oferta = $('#chk-oferta').is(":checked") ? "S" : "";
        var prime = $('#chk-prime').is(":checked") ? "S" : "";
        $.blockUI({
            message: '<h4>Por favor Espere...</h4>'
        });
        window.location.href = "product-list.php?categoria=" + $('#categoria').val() + "&subcategoria=" + $('#s-subcategoria').val() + "&hasta=" + $('#s-cantidad').val() + "&nombrecateg=" + $('#nombrecateg').val() + "&smarca=" + $('#s-marca').val() + "&rango=" + $('#s-precios').val() + "&oferta=" + oferta + "&prime=" + prime + "&descripcion=" + $('#descripcion').val() + "&nombre=" + $('#nombre').val();

    });

    $("#buscar").click(function () {
        var oferta = $('#chk-oferta').is(":checked") ? "S" : "";
        var prime = $('#chk-prime').is(":checked") ? "S" : "";
        $.blockUI({
            message: '<h4>Por favor Espere...</h4>'
        });
       window.location.href = "product-list.php?categoria=" + $('#categoria').val() + "&subcategoria=" + $('#s-subcategoria').val() + "&nombrecateg=" + $('#nombrecateg').val() + "&smarca=" + $('#s-marca').val() + "&rango=" + $('#s-precios').val() + "&oferta=" + oferta + "&prime=" + prime + "&descripcion=" + $('#descripcion').val() + "&nombre=" + $('#nombre').val();


    });

    $(".buyNow").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#s-talla-" + codProducto).val();
        var codColor = $('#colorselec-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var disponible = $('#disponible-'+codProducto).val();
        if (jQuery.isEmpty(cantidad)) {
            mostrarMensajeError('Error', 'Debe colocar una cantidad', 'ERROR');
            return;
        }
        if(cantidad > disponible){
            mostrarMensajeError('Error', 'La cantidad solicitada no está disponible', 'ERROR');

        }else{
            if($('#id_user').val()!=''){
                window.location.href = "process-CompraExpress.php?codProd=" + codProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad;
            }else {
                comprarAhora(codProducto, codTalla, codColor, cantidad);
            }
        }

    });
    $(".addCart").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var codTalla = $("#s-talla-" + codProducto).val();
        var codColor = $('#colorselec-' + codProducto).val();
        var cantidad = $('#cantidad-' + codProducto).val();
        var disponible = $('#disponible-'+codProducto).val();
        var codUsuario = $('#id_user').val();
        if (jQuery.isEmpty(cantidad)) {
            mostrarMensajeError('Error', 'Debe colocar una cantidad', 'ERROR');
            return;
        }
       // if(cantidad > disponible){
         //   mostrarMensajeError('Error', 'La cantidad solicitada no está disponible', 'ERROR');
        //}else{
            if(codUsuario!=''){
                insertarEnCarro(codProducto, codColor,codTalla,cantidad,codUsuario);
            }else {
                //Si no está Logueado no se que hacer aún
                //comprarAhora(codProducto, codTalla, codColor, cantidad);
            }
        //}

    });

    $("#buscar2").click(function () {
        $.blockUI({
            message: '<h4>Por favor Espere...</h4>'
        });
        window.location.href = "product-list.php?nombre=" + $('#descripcion2').val();
    });

    comprarAhora = function (CodProducto, codTalla, codColor, cantidad) {
        var dialog = new BootstrapDialog({
            message: function (dialogRef) {
                var $message = $('<div></div>');
                var $message2 = $('<div class="col-lg-6 col-lg-offset-5"> <h4>o haga una</h4></div>');
                var $ingresar = $('<button class="btn btn-danger btn-lg btn-block">Ingrese a su cuenta</button>');
                var $comprar = $('<button class="btn btn-primary btn-lg btn-block">Compra Express</button>');
                $ingresar.on('click', {dialogRef: dialogRef}, function (event) {
                    window.location.href = "login.php?codProd=" + CodProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad+'&redirect=process-CompraExpress';
                });
                $comprar.on('click', {dialogRef: dialogRef}, function (event) {
                    window.location.href = "compraExpress.php?codProd=" + CodProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad;
                });

                $message.append($ingresar);
                $message.append($message2);
                $message.append($comprar);

                return $message;
            },
            closable: true
        });
        dialog.realize();
        dialog.getModalHeader().hide();
        dialog.getModalFooter().hide();
        dialog.open();
    };





});