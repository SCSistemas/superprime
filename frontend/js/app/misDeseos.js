/**
 * Created by B591912 on 21/12/2016.
 */

// Datatables
$('table').DataTable({
    "language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "bFilter": false,
    "createdRow": function( row, data, dataIndex ) {
        $(row).addClass('row item');
    },
    "aoColumns": [
        {"sClass": "row item" }
    ]
});

$(document).ready(function() {
    $('#desde, #hasta').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    $('#devolucion').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            //No validado
        } else {
            $('[data-remodal-id = aceptarDevolucion]').remodal().open();
        }
    });
    obtenerDeseos();
});

// Handler para Eliminar Deseos
$(document).on('click', '.confirm-wish-delete', function () {
    var cod_producto = $("#wish_id_delete").val();
    var id_user="4";
    $.ajax({
        type: 'POST',
        url: 'http://localhost/superprime_new/superprime/admin/index.php?service=userservices&metodo=EliminarDeseo',
        data: JSON.stringify({p_id_user: id_user, p_cod_producto : cod_producto}),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (response) {
            $('#msj-wishlist-information').html('');
            $('#msj-wishlist-information').html(response.msg);
            $('[data-remodal-id=wishlist-information-message]').remodal().open();
        },
        complete: function(){
            $("#wish_id_delete").val("");
        },
        error: function(xhr, status, error) {
            var acc = []
            $.each(xhr, function(index, value) {
                acc.push(index + ': ' + value);
            });
            console.log("Error eliminando deseo"+JSON.stringify(acc));
        }
    });
});

//Obtener Lista de Deseos
function obtenerDeseos(){
    $.ajax({
        type: 'GET',
        url: 'http://localhost/superprime_new/superprime/admin/index.php?service=userservices&metodo=ObtenerListaDeseos&p_id_usuario=4',
        dataType: 'json',
        success: function (response) {
            var item_to_append;
            var img;
            $('#table_wishes').DataTable().clear();
            var t = $('#table_wishes').DataTable();
            $.each(response.mi_lista_de_deseo, function(index, value) {
                item_number= "item_"+index;
                $.each(value.imagenes, function(index, value) {
                    if (index==0){
                        img = value;
                    }
                });
                item_to_append=
                    "<td class=\"row item\">"+
                    "<input type=\"hidden\" name=\"codigo_producto\" value="+value.codigo_producto+">"+
                    "<input type=\"hidden\" name=\"codigo_categoria\" value="+value.codigo_categoria+">"+
                    "<input type=\"hidden\" name=\"codcolor\" value="+value.codcolor+">"+
                    "<div class=\"col-lg-2\">"+
                    "<img src=\"../images"+img+"\" class=\"img-responsive\">"+
                    "</div>"+
                    "<div class=\"col-lg-7\">"+
                    "<h3 class=\"producto\">"+value.descripcion+"</h3>"+
                    "<h3>Modelo "+value.modelo+"</h3>"+
                    "<h3>Precio: Bs "+value.precio+"</h3>"+
                    "<h3 class=\"total\">Total: Bs "+value.total+"</h3>"+
                    "</div>"+
                    "<div class=\"col-lg-3 text-center\">"+
                    "<a href=\"../views/cart-groupProducts.php\" class=\"btn btn-success btn-block btn-sm buyAgain\" > <span class=\"icon-cart\"></span> Comprar Ahora</a>"+
                    "<a onclick=\"agregarCarrito('"+"4"+"','"+value.codigo_producto+"')"+"\" class=\"btn btn-warning btn-block btn-sm buyAgain\"> <span class=\"icon-cart\"></span> Agregar al carrito</a>"+
                    "<a onclick=\"eliminarDeseo('"+"4"+"','"+value.codigo_producto+"')"+"\" class=\"btn btn-danger btn-block btn-sm\"  data-remodal-target=\"cancelar\"><i class=\"fa fa-heart\" aria-hidden=\"true\"></i> Eliminar de la lista</a>"+
                    "</div>"+
                    "</td>"
                t.row.add([item_to_append]).draw(false);
            });
        },
        error: function(xhr, status, error) {
            var acc = []
            $.each(xhr, function(index, value) {
                acc.push(index + ': ' + value);
            });
            console.log("Error obteniendo la lista de deseos"+JSON.stringify(acc));
        }
    });
}

function eliminarDeseo(id_user,cod_producto){
    $("#wish_id_delete").val(cod_producto);
    $('[data-remodal-id=wish-confirm-delete]').remodal().open();
}

function agregarCarrito(id_user,cod_producto){
    $.ajax({
        type: 'POST',
        url: 'http://localhost/superprime_new/superprime/admin/index.php?service=userservices&metodo=MoverACarrito',
        data: JSON.stringify({p_usuario: id_user, cod_producto : cod_producto}),
        dataType: 'json',
        success: function (response) {

        },
        error: function(xhr, status, error) {
            var acc = []
            $.each(xhr, function(index, value) {
                acc.push(index + ': ' + value);
            });
            console.log("Error moviendo deseo al carrito"+JSON.stringify(acc));
        }
    });
}