/**
 * Created by luis on 09/01/2017.
 */
String.prototype.initCap = function () {
    return this.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
String.prototype.toUpperCaseFirstChar = function() {
    return this.initCap();
}

String.prototype.toLowerCaseFirstChar = function() {
    return this.substr( 0, 1 ).toLowerCase() + this.substr( 1 );
}

String.prototype.toUpperCaseEachWord = function( delim ) {
    delim = delim ? delim : ' ';
    return this.split( delim ).map( function(v) { return v.toUpperCaseFirstChar() } ).join( delim );
}

String.prototype.toLowerCaseEachWord = function( delim ) {
    delim = delim ? delim : ' ';
    return this.split( delim ).map( function(v) { return v.toLowerCaseFirstChar() } ).join( delim );
}
String.prototype.toMoneda = function() {
    var numero = Number(this);
    return numeral(numero).format();
}

String.prototype.toNumber= function(  ) {
    return numeral().unformat(this);
}

String.prototype.unMask= function(  ) {
    var value = this;
    value = value.replace(/\-/g,'').replace(/\)/g,'').replace(/\(/g,'').replace(/\ /g,'');
    return value;
}
String.prototype.maskPhoneNumber= function() {
    var s = this;
    var s2 = (""+s).replace(/\D/g, '');
    var m = s2.match(/^(\d{4})(\d{3})(\d{4})$/);
    return (!m) ? this : "(" + m[1] + ") " + m[2] + "-" + m[3];
}

String.prototype.PhoneNumber= function() {
    var s = this;
    return s.substring(4);
}
String.prototype.ColorEscala= function() {
    var s = this;
    if ('ROJO' == s){ //automovil
        return 'danger';
    }
    if ('VERDE' == s){ //fianzas
        return 'success';
    }
    if ('AMARILLO' == s){ //fianzas
        return 'warning';
    }
    return 'success';
}
/**
 * Protopipo para conversión de número a moneda desde un dato Number
 */
Number.prototype.numberToMoneda = function() {
    return this.withPuntos();
};

/**
 * Protopipo para conversión de número a moneda desde un dato String
 */
String.prototype.numberToMoneda = function() {
    var monto = this.monedaToNumber();
    if (monto != 'NaN') {
        return monto.withPuntos();
    } else {
        return 'NaN';
    }
};

/**
 * Protopipo para conversión de moneda a número desde un dato Number
 */
Number.prototype.monedaToNumber = function() {
    return this.toString().monedaToNumber();
};

/**
 * Protopipo para conversión de moneda a número desde un dato String
 */
String.prototype.monedaToNumber = function() {
    var valor = this.trim();
    // entrada en formato criollo, más de dos decimales:
    var re0 = /(^([1-9]{1}\d{0,2}(\.{1}\d{3})*)|^(\d+)),{1}(\d{3,})$/g;
    // entrada en formato gringo, más de dos decimales:
    var re1 = /(^([1-9]{1}\d{0,2}(,{1}\d{3})*)|^(\d+))\.{1}(\d{3,})$/g;
    // entrada en formato criollo, con o sin decimales:
    var re2 = /(^([1-9]{1}\d{0,2}(\.{1}\d{3})*)|^(\d+))(,{1}\d+)?$/g;
    // entrada en formato gringo, con o sin decimales:
    var re3 = /(^([1-9]{1}\d{0,2}(,{1}\d{3})*)|^(\d+))(\.{1}\d+)?$/g;
    // entrada en formato gringo, con o sin decimales:
    var re4 = /(^[,\.]{1}\d+)$/g;
    var arr1, arr2, limpio;
    var re0Arr = re0.exec(valor);
    var re1Arr = re1.exec(valor);
    if (re0Arr) {
        arr1 = Number(parseFloat(re0Arr[1].replace(/\./g, '')));
        arr2 = Number(parseFloat('0.' + re0Arr[5]).toFixed(2));
        valor = Number(arr1 + arr2);
        valor = valor + '';
    }
    if (re1Arr) {
        arr1 = Number(parseFloat(re1Arr[1].replace(/,/g, '')));
        arr2 = Number(parseFloat('0.' + re1Arr[5]).toFixed(2));
        valor = Number(arr1 + arr2);
        valor = valor + '';
    }
    if (valor.match(re2)) {
        var preLimpio = valor.replace(/\./g, '');
        limpio = preLimpio.replace(/,/g, '.');
        return parseFloat(limpio);
    } else
    if (valor.match(re3)) {
        limpio = valor.replace(/,/g, '');
        return parseFloat(limpio);
    } else
    if (valor.match(re4)) {
        limpio = valor.replace(/,/g, '.');
        var valor4 = parseFloat('0' + limpio).toFixed(2);
        return valor4;
    } else {
        return 'NaC';
    }
};

/**
 * Protopipo auxiliar a numberToMoneda y a monedaToNumber
 */
String.prototype.reverse = function() {
    return this.split('').reverse().join('');
};

/**
 * Protopipo auxiliar a numberToMoneda y a monedaToNumber
 */
Number.prototype.withPuntos = function() {
    var x = 6, y = parseFloat(this).toFixed(2).toString().reverse();
    y = y.replace(/\./, ',');
    while (x < y.length) {
        y = y.substring(0, x) + '.' + y.substring(x);
        x += 4;
    }
    return y.reverse();
};

String.prototype.withPuntos = function() {
    var x = 6, y = parseFloat(this).toFixed(2).toString().reverse();
    y = y.replace(/\./, ',');
    while (x < y.length) {
        y = y.substring(0, x) + '.' + y.substring(x);
        x += 4;
    }
    return y.reverse();
};


Handlebars.getTemplate = function(name) {
    //Si el template fue cargado no lo vuelve a cargar durante la ejecucion
    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined)    {
        $.ajax({
            contentType :'text/plain; charset=UTF-8',
            url : name + '.handlebars',
            cache: false,
            success : function(data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                Handlebars.templates[name] = Handlebars.compile(data);
            },
            async : false
        });
    }
    return Handlebars.templates[name];
};
Handlebars.registerHelper('toLowerCase', function(value) {
    if(value) {
        return new Handlebars.SafeString(value.toLowerCase());
    } else {
        return '';
    }
});
Handlebars.registerHelper('toCamelCase', function(value) {
    if(value) {
        return new Handlebars.SafeString(value.toLowerCase().initCap());
    } else {
        return '';
    }
});

Handlebars.registerHelper('toMoneda', function(value) {
    var valor = Number(value);
    if ((valor) || (valor ==0))  {
        return new Handlebars.SafeString(numeral(valor).format());
    } else {
        return '';
    }
});

Handlebars.registerHelper('estaSeleccionado',  function(value) {
    if (value == 'S')  {
        return 'bg-primary'
    } else {
        return 'bg-success';
    }
});
Handlebars.registerHelper('estaAprobada',  function(value) {
    if (value == 'S')  {
        return 'bg-success';
    } else {
        return 'bg-warning';
    }
});

Handlebars.registerHelper('esCobroExitoso',  function(value) {
    if (value == 'S')  {
        return 'Cobro Exitoso';
    } else {
        return 'Operación Fallida';
    }
});


Handlebars.registerHelper('esNatural',  function(value) {
    if (value == 'N')  {
        return 'fa fa-user';
    } else {
        return 'fa fa-institution';
    }
});

Handlebars.registerHelper('esVisible',  function(value) {
    if (value == 'N')  {
        return 'disNone';
    }
});

Handlebars.registerHelper('esDctoVisible',  function(value) {
    if (value <= 0)  {
        return '';
    }else{
        return ' (-' + numeral(value).format() + ')';
    }
});

Handlebars.registerHelper('esMontoVisible',  function(value) {
    if (value == '')  {
        return '';
    } else{
        return numeral(value).format();
    }
});


Handlebars.registerHelper('isSelected', function(a, b) {
    if(a == b)
        return "selected";
    else
        return "";
});


Handlebars.registerHelper('isChecked', function(a, b) {
    if(a == b)
        return "checked";
    else
        return "";
});
Handlebars.registerHelper('iconoRamo', function(value) {
    if ('0002' == value || 'AUTOMOVIL' == value) { //automovil
        return 'fa-car';
    }
    if ('0003' == value || 'FIANZAS' == value) { //fianzas
        return 'fa-bank';
    }
    if ('0001' == value || 'PATRIMONIALES' == value) { //patrimonial
        return 'fa-building';
    }
    if ('0004' == value || 'PERSONAS' == value) { //personas
        return 'fa-user';
    }
});
Handlebars.registerHelper('decodeColorEscala', function(value) {
    if ('ROJO' == value){ //automovil
        return 'danger';
    }
    if ('VERDE' == value){ //fianzas
        return 'success';
    }
    if ('AMARILLO' == value){ //fianzas
        return 'warning';
    }
});

Handlebars.registerHelper('maskPhoneNumber', function(value) {
    if(value) {
        return new Handlebars.SafeString(value.maskPhoneNumber());
    } else {
        return '';
    }
});
Handlebars.registerHelper('esPrimero',  function(value) {
    if (value == 1)  {
        return 'active';
    } else {
        return '';
    }
});


Handlebars.registerHelper('isLugarAjuste',  function(value) {
    if (value == 'C')  {
        return 'Centro de Negocios';
    } else {
        return 'Otro';
    }
});
Handlebars.registerHelper('ifCondicional', function(v1, v2, options) {
    if(v1 === v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('esTelefono',  function(value) {
    if (value == '')
        return "";

    return value.maskPhoneNumber();
});

Handlebars.registerHelper('numTelefono',  function(value) {
    if (value == '')
        return "";
    return value.PhoneNumber();
});

Handlebars.registerHelper('indicadorSuma',  function(value, montosuma) {
    if (value == 'N'){
        return "No aplica";
    }else{
        var valor = Number(montosuma);
        if ((valor) || (valor ==0))  {
            return new Handlebars.SafeString(numeral(valor).format());
        } else {
            return '';
        }
    }

});


Handlebars.registerHelper('estaSeleccionadoPlanIcono',  function(value) {
    if (value == 'S')  {
        return 'fa fa-arrow-circle-o-right'
    } else {
        return 'fa fa-pencil';
    }
});


Handlebars.registerHelper('estaSeleccionadoPlanIconoResumen',  function(value) {
    if (value == 'S')  {
        return 'fa fa-arrow-circle-o-down'
    } else {
        return 'fa fa-pencil';
    }
});
Handlebars.registerHelper('estaSeleccionadoSelect',  function(value) {
    if (value == 'S')  {
        return 'selected'
    } else {
        return '';
    }
});
Handlebars.registerHelper('estaLleno',  function(value) {
    if (value.length == 0)  {
        return 'active'
    } else {
        return '';
    }
});


Handlebars.registerHelper('ifCond', function(v1, v2, options) {
    if(v1 === v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('json', function(context) {
    return JSON.stringify(context);
});

Handlebars.registerHelper('ifCondOperador', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});
Handlebars.registerHelper('isCurrency', function(value) {
    if (value != undefined){
        if (value.indexOf('.') > 0){
            var valor = Number(value);
            if ((valor) || (valor ==0))  {
                return new Handlebars.SafeString(numeral(valor).format());
            } else {
                return '';
            }
        }else{
            return value;
        }
    }else{
        return value;
    }
});
Handlebars.registerHelper('escribeRowJson', function() {
    return JSON.stringify(this);
});

Handlebars.registerHelper('checked', function(value) {
    if(value == 'S')
        return "checked";
    else
        return "";
});


Handlebars.registerHelper('disable', function(value) {
    if(value == 'S')
        return "disabled=disabled";
    else
        return "";
});
Handlebars.registerHelper('nvlNullNumeric', function(value) {
    if(value == '')
        return "0";
    else
        return value;
});


Handlebars.registerHelper('nullNumber', function(value) {

    var valor = null;
    if(value == '')
        valor =Number("0");
    else
        valor= Number(value);

    return new Handlebars.SafeString(numeral(valor).format());
});

Handlebars.getTemplatePartial = function(name) {
    $.ajax({
        contentType :'text/plain; charset=UTF-8',
        url : name + '.handlebars',
        cache: false,
        success : function(data) {
            Handlebars.registerPartial(name, Handlebars.compile(data));
        },
        async : false
    });

};

(function($) {

    jQuery.isEmpty = function(obj){
        var isEmpty = false;

        if (typeof obj == 'undefined' || obj === null || obj === ''){
            isEmpty = true;
        }

        if (typeof obj == 'number' && isNaN(obj)){
            isEmpty = true;
        }

        if (obj instanceof Date && isNaN(Number(obj))){
            isEmpty = true;
        }

        return isEmpty;
    }

})(jQuery);


function  mostrarMensajeError(titulo,mensaje,tipoMensaje) {
    var tipo=BootstrapDialog.TYPE_DEFAULT;
    if(tipoMensaje=='ERROR')
        tipo=BootstrapDialog.TYPE_DANGER;
    if(tipoMensaje=='MENSAJE')
        tipo=BootstrapDialog.TYPE_SUCCESS;
    if(tipoMensaje=='INFO')
        tipo=BootstrapDialog.TYPE_INFO;

    BootstrapDialog.show({
            type: tipo,
            title: titulo,
            message: mensaje,
            buttons: [{
                label: 'Ok',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });

}

devuelveCantidadCarrito = function (codUsuario) {
    $.ajax({
        type: "GET",
        url: "../../admin/index.php?service=userservices&metodo=ObtenerCantidadCarrito&p_id_usuario=" + codUsuario,
        success: function (respuesta) {
            var ajaxResponse = $.parseJSON(respuesta);
                $("#carrito").empty();
                $("#carrito").append(ajaxResponse.Total);
        },
        complete: function () {
            $("#myModal").modal({backdrop: false});
        }
    })

};

insertarEnCarro = function (codProducto, codColor,codTalla,cantidad,codUsuario) {
    var parametros = {
        'p_cod_producto': codProducto,
        'p_cod_color': codColor,
        'p_cod_talla': codTalla,
        'p_cantidad': cantidad,
        'p_id_user': codUsuario
    };
    $.ajax({
            type: "POST",
            url: "../../admin/index.php?service=userservices&metodo=InsertarEnCarro",
            data: JSON.stringify(parametros),
            contentType: "application/json",
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);

                if(ajaxResponse.success=='1'){
                    mostrarMensajeError('Éxito', 'El producto fue agregado a su carro', 'INFO');
                    devuelveCantidadCarrito(codUsuario);
                }else{
                    mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');
                }

            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        }
    )

}

comprarAhora=function(CodProducto,codTalla,codColor,cantidad){
    var dialog = new BootstrapDialog({
        message: function(dialogRef){
            var $message = $('<div></div>');
            var $message2 = $('<div class="col-lg-6 col-lg-offset-5"> <h4>o haga una</h4></div>');
            var $ingresar = $('<button class="btn btn-danger btn-lg btn-block">Ingrese a su cuenta</button>');
            var $comprar = $('<button class="btn btn-primary btn-lg btn-block">Compra Express</button>');
            $ingresar.on('click', {dialogRef: dialogRef}, function(event){
                window.location.href = "login.php?codProd=" + CodProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad+'&redirect=process-CompraExpress';
            });
            $comprar.on('click', {dialogRef: dialogRef}, function(event){
                window.location.href="compraExpress.php?codProd="+CodProducto+"&codColor="+codColor+"&codTalla="+codTalla+"&cantidad="+cantidad;
            });
            $message.append($ingresar);
            $message.append($message2);
            $message.append($comprar);

            return $message;
        },
        closable: true
    });
    dialog.realize();
    dialog.getModalHeader().hide();
    dialog.getModalFooter().hide();
    dialog.open();
} ;



