<?php 
	include '../config/definitions.php';

	$ch = curl_init();
	$url = $urlWS.'service=generalservices&metodo=ObtenerParroquias&p_cod_estado='.$_GET['idestado'].'&p_cod_ciudad='.$_GET['idciudad'].'&p_cod_municipio='.$_GET['idmunicipio'];

	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$parroquias = curl_exec($ch);

	curl_close($ch);
	echo $parroquias;
?>
