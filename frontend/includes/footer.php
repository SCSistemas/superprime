<footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-3 col-lg-3">
            <h1>SuperPrime C.A</h1>
            <ul>
              <li><a href="">Quiènes Somos</a></li>
              <li><a href="">Preguntas Frecuentes</a></li>
              <li><a href="">Ayuda</a></li>
              <li><a href="">Distribuidores</a></li>
              <li><a href="">Tiendas</a></li>
              <li><a href="">Contàctanos</a></li>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <h1>Busca en nuestras Categorías</h1>
            <ul>
              <li><a href="">Hogar</a></li>
              <li><a href="">Caballeros</a></li>
              <li><a href="">Damas</a></li>
              <li><a href="">Juvenil</a></li>
              <li><a href="">Juguetería</a></li>
              <li><a href="">Deportes</a></li>
              <li><a href="">Ferretería</a></li>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
            <h1>Contáctanos</h1>
            <p>Urbanización Nombre Largo, Zona Uno, Torre numero dos, Oficinas, Tres, Cuatro y Cinco
            Caracas, Venezuela.<br>
            <strong>Email:</strong> <a href="">info@primeshoes.com</a><br>
            <strong>Telf:</strong> 0212 3659685 0212 4569845<p>
          </div>
          <div class="col-sm-6 col-md-3 col-lg-3">
             <h1>Nuestras Redes Sociales</h1> 
            <p>Encuentranos en las redes socilaes y está atento a nuestras promociones</p>
            <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
          </div>  
        </div>
      </div>
      <div class="creditsSector">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <p>Copyright 2016 Superprime C.A RIF: j-5698616-5 Todos los Derechos Reservados </p>
            </div>
            <div class="col-lg-6">
              <p class="pull-right"><strong>Desarrollado por</strong> S&C Sistemas C.A</p>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/modernizr.custom.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/validator.js"></script>
    <script src="../js/remodal/remodal.js"></script>  
    <script src="../js/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="../js/datepicker/locales/bootstrap-datepicker.es.min.js"></script>
    <script src="../js/jquery.numeric.min.js"></script>
    <script src="../js/bootstrap-formhelpers.js"></script>
    <script src="../js/ion.rangeSlider.js"></script>
    <script src="../js/jquery.mg-space.js"></script>
   <!-- <script src="../js/app.js"></script>-->
    <script src="../js/jasny/jasny-bootstrap.js"></script>
    <script src="../js/iCheck/icheck.min.js"></script>
    <script src="../js/datatables/jquery.dataTables.min.js"></script>
    <script src="../js/datatables/dataTables.bootstrap.min.js"></script>
    <script src="../js/responsiveslider/responsiveslides.min.js"></script>
    <script src="../js/jquery-scrollto.js"></script>
    <script src="../js/lightslider/js/lightslider.min.js"></script>
    <script src="../js/grid.js"></script>
    <script src="../js/custom.js"></script>
    <script src="../js/validatePass.js"></script>
    <script src="../js/handlebars/handlebars-v4.0.2.min.js"></script>
    <script src="../js/util.js"></script>
    <script type="text/javascript" src="../js/jquery.bootpag.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap-dialog.js"></script>
    <script type="text/javascript" src="../js/BlockUi.js"></script>
  </body>
</html>