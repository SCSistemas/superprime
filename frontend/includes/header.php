<?php

if(!isset($_SESSION))
{
  session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SuperPrime</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../js/remodal/remodal.css" rel="stylesheet">
    <link href="../js/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../js/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">
    <link href="../css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="../css/ion.rangeSlider.css" rel="stylesheet">
    <link href="../css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="../css/mg-space.css" rel="stylesheet">
    <link href="../js/iCheck/skins/flat/red.css" rel="stylesheet">
    <link href="../js/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="../js/datatables/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../js/responsiveslider/responsiveslides.css" rel="stylesheet">
    <link href="../js/lightslider/css/lightslider.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,300,400italic,300italic,500,800,900,700,600' rel='stylesheet' type='text/css'>
    <link href='../css/pretty.min.css' rel='stylesheet' type='text/css'>
    <link href='../css/bootstrap-dialog.css' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <input type="hidden" id="id_user" value="<?php echo $_SESSION['userid']; ?>"/>
  <header class="miniHeader">
  <div class="container">
    <ul class="nav nav-pills pull-right">
<?php  if (!empty($_SESSION['userid'])) {  ?>
      <li><a href="logout.php"><span class="icon-user"></span> Salir</a></li>
      <li><a href="backend-MisCompras.php"><span class="icon-user"></span> Administrar</a></li>
<?php } else {?>
      <li><a href="login.php"><span class="icon-user"></span> Iniciar</a></li>
      <li><a href="register.php"><span class="icon-user"></span> Registrarse</a></li>
<?php }?>
      <li><a href="help.php"><span class="icon-help"></span> Ayuda</a></li>
    </ul>
  </div>
</header>

<header class="full">
  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-md-3 col-lg-3">
        <a href="../pages/index.php" class="brand"><img src="../images/logoB.png" class="img-responsive"></a>
      </div>
      <div class="col-sm-7 col-md-7 col-lg-7">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" class="  search-query form-control" id="descripcion2" name="descripcion2" placeholder="¿Buscas algo?" />
                <span class="input-group-btn">
                    <button class="btn btn-danger" id="buscar2" name="buscar2" type="button">
                        <span class=" glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </div>
      </div>
      <div class="col-sm-2 col-md-2 col-lg-2">
         <button type="button" class="btn btn-default btn-sm cartButton"><span class="icon-cart"></span> Carrito <span id="carrito"></span></button>
      </div>
    </div>
  </div>
</header>

