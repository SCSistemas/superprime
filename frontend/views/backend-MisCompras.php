 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
							  <h4><span id="nombre_u"></span></h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>
	    		<div class="col-lg-9" id="misCompras">

	    			<h1>Mis Compras</h1>



	    		 	<form>
	    		 		<fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="row">
                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" id="desde" name="desde" placeholder="Hasta" required="required">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                   <div class="input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" id="hasta" name="hasta" placeholder="Hasta" required="required">
                                  </div>
                                </div>
                                <div class="col-md-5" style="width:36.666667%">
                                  <div class="form-inline">
                                    <div class="form-group">
                                      	<label for="categorías" class="cols-sm-2 control-label">Filtrar por Categorías:</label>
                                      	<select class="form-control" name="categorías" id="categorías">
						                  <option></option>
						                  <option value="Zapatos">Zapatos</option>
						                  <option value="Cateras">Cateras</option>
						                  <option value="Juguetes">Juguetes</option>
						                  <option value="Accesorios">Accesorios</option>
						                </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-1">
                                	<button type="submit" class="btn btn-primary">Buscar</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </fieldset>	
	    		 	</form>


	    		 	<div class="container-fluid">
		    		 	<div class="results">

		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Orden Número: DC-26556 | Estatus: <span class="label label-info">Enviado</span><span class="label label-success">Facturado</span><span class="label label-success">Entregado</span><span class="label label-info">Por retirar en entienda</span><span class="label label-danger">Devuelto</span></h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a href="" class="btn btn-success btn-block  btn-sm buyAgain"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar de nuevo</a>
		    		 				<a href="" class="btn btn-danger btn-block  btn-sm buyAgain"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="" class="btn btn-warning btn-block  btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Hacer Reclamo</a>
									<a class="btn btn-warning btn-block  btn-sm" role="button" data-toggle="collapse" href="#item1" aria-expanded="false" aria-controls="collapseExample"><span class="glyphicon glyphicon-retweet"></span> Cambiar Producto</a>
		    		 			</div>


		    		 			<!-- Cambiar Producto -->
		    		 			<div class="col-lg-12" id="cambiarProducto">

		    		 				<div class="collapse" id="item1">

									  <div class="well">

									  <fieldset>
									  	<legend>Cambiar Producto: Orden DC-26556 <span class="pull-right">Fecha de cambio: 22/05/2016</span></legend>
											
											<form class="form-horizontal" id="devolucion" role="form" data-toggle="validator">


											<!-- Select Basic -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="motivoDevolucion">Motivo de la devolución</label>
											  <div class="col-md-4">
											    <select id="motivoDevolucion" name="selectbasic" class="form-control" required>
											      <option value="">Seleccione una</option>
											      <option value="1">Talla incorrecta</option>
											      <option value="2">Defectuoso</option>
											      <option value="2">Otro</option>
											    </select>
											  </div>
											</div>

											<!-- Textarea -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="textarea">Observaciones</label>
											  <div class="col-md-8">                     
											    <textarea class="form-control" id="textarea" name="textarea" placeholder="Indique la razón por la cual devuelve el producto" required></textarea>
											  </div>
											</div>

											<!-- Button -->
											<div class="form-group">
											  <label class="col-md-3 control-label"></label>
											  <div class="col-md-4">
											    <button type="submit" class="btn btn-primary">Aceptar</button>
											  </div>
											</div>

											<!-- Info -->
											<div class="confirmacion">
												<div class="row">
													<div class="col-lg-2 text-center">
														<span class="glyphicon glyphicon-warning-sign"></span>
													</div>
													<div class="col-lg-10">
														<h4>El Cambio ha sido registrado</h4>
														<p>El producto deberá ser enviado en un lapso de 7 días a la siguiente dirección a traves de Zoom:<br>
														Av. Intercomunal Guarenas Guatire, Galpón número 5, Oficina de devoluciones<br>
														Telf: 0212-2134567 / 0212-7894563<br>

														Una vez hayamos recibido el producto, comenzaremos a atender su petición.<br>
														Le daremoe respuesta entre 3 y 5 días despues, a travez de un correo electrónico en la sección Mis Compras.</p>
													</div>
													<div class="col-lg-12 text-center">
														<a href="" class="btn btn-primary">Cerrar</a>
													</div>
												</div>
												
											</div>

											</form>

							

									  </fieldset>
									  </div>
									</div>
		    		 			</div>
		    		 			<!-- /Cambiar Producto -->
		    		 		</div>
		    		 		<!-- /Item -->
		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Orden Número: DC-26556 | Estatus: <span class="label label-info">Enviado</span><span class="label label-success">Facturado</span><span class="label label-success">Entregado</span><span class="label label-info">Por retirar en entienda</span><span class="label label-danger">Devuelto</span></h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a href="" class="btn btn-success btn-block btn-sm buyAgain"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar de nuevo</a>
		    		 				<a href="" class="btn btn-danger btn-block btn-sm buyAgain"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="" class="btn btn-warning btn-block btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Hacer Reclamo</a>
									<a class="btn btn-warning btn-block btn-sm" role="button" data-toggle="collapse" href="#item2" aria-expanded="false" aria-controls="collapseExample"><span class="glyphicon glyphicon-retweet"></span> Cambiar Producto</a>
		    		 			</div>


		    		 			<!-- Cambiar Producto -->
		    		 			<div class="col-lg-12" id="cambiarProducto">

		    		 				<div class="collapse" id="item2">

									  <div class="well">

									  <fieldset>
									  	<legend>Cambiar Producto: Orden DC-26556 <span class="pull-right">Fecha de cambio: 22/05/2016</span></legend>
											
											<form class="form-horizontal" id="devolucion" role="form" data-toggle="validator">


											<!-- Select Basic -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="motivoDevolucion">Motivo de la devolución</label>
											  <div class="col-md-4">
											    <select id="motivoDevolucion" name="selectbasic" class="form-control" required>
											      <option value="">Seleccione una</option>
											      <option value="1">Talla incorrecta</option>
											      <option value="2">Defectuoso</option>
											      <option value="2">Otro</option>
											    </select>
											  </div>
											</div>

											<!-- Textarea -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="textarea">Observaciones</label>
											  <div class="col-md-8">                     
											    <textarea class="form-control" id="textarea" name="textarea" placeholder="Indique la razón por la cual devuelve el producto" required></textarea>
											  </div>
											</div>

											<!-- Button -->
											<div class="form-group">
											  <label class="col-md-3 control-label"></label>
											  <div class="col-md-4">
											    <button type="submit" class="btn btn-primary">Aceptar</button>
											  </div>
											</div>

											<!-- Info -->
											<div class="confirmacion">
												<div class="row">
													<div class="col-lg-2 text-center">
														<span class="glyphicon glyphicon-warning-sign"></span>
													</div>
													<div class="col-lg-10">
														<h4>El Cambio ha sido registrado</h4>
														<p>El producto deberá ser enviado en un lapso de 7 días a la siguiente dirección a traves de Zoom:<br>
														Av. Intercomunal Guarenas Guatire, Galpón número 5, Oficina de devoluciones<br>
														Telf: 0212-2134567 / 0212-7894563<br>

														Una vez hayamos recibido el producto, comenzaremos a atender su petición.<br>
														Le daremoe respuesta entre 3 y 5 días despues, a travez de un correo electrónico en la sección Mis Compras.</p>
													</div>
													<div class="col-lg-12 text-center">
														<a href="" class="btn btn-primary">Cerrar</a>
													</div>
												</div>
												
											</div>

											</form>

							

									  </fieldset>
									  </div>
									</div>
		    		 			</div>
		    		 			<!-- /Cambiar Producto -->
		    		 		</div>
		    		 		<!-- /Item -->
		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Orden Número: DC-26556 | Estatus: <span class="label label-info">Enviado</span><span class="label label-success">Facturado</span><span class="label label-success">Entregado</span><span class="label label-info">Por retirar en entienda</span><span class="label label-danger">Devuelto</span></h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a href="" class="btn btn-success btn-block  btn-sm buyAgain"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar de nuevo</a>
		    		 				<a href="" class="btn btn-danger btn-block  btn-sm buyAgain"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="" class="btn btn-warning btn-block  btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Hacer Reclamo</a>
									<a class="btn btn-warning btn-block  btn-sm" role="button" data-toggle="collapse" href="#item3" aria-expanded="false" aria-controls="collapseExample"><span class="glyphicon glyphicon-retweet"></span> Cambiar Producto</a>
		    		 			</div>


		    		 			<!-- Cambiar Producto -->
		    		 			<div class="col-lg-12" id="cambiarProducto">

		    		 				<div class="collapse" id="item3">

									  <div class="well">

									  <fieldset>
									  	<legend>Cambiar Producto: Orden DC-26556 <span class="pull-right">Fecha de cambio: 22/05/2016</span></legend>
											
											<form class="form-horizontal" id="devolucion" role="form" data-toggle="validator">


											<!-- Select Basic -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="motivoDevolucion">Motivo de la devolución</label>
											  <div class="col-md-4">
											    <select id="motivoDevolucion" name="selectbasic" class="form-control" required>
											      <option value="">Seleccione una</option>
											      <option value="1">Talla incorrecta</option>
											      <option value="2">Defectuoso</option>
											      <option value="2">Otro</option>
											    </select>
											  </div>
											</div>

											<!-- Textarea -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="textarea">Observaciones</label>
											  <div class="col-md-8">                     
											    <textarea class="form-control" id="textarea" name="textarea" placeholder="Indique la razón por la cual devuelve el producto" required></textarea>
											  </div>
											</div>

											<!-- Button -->
											<div class="form-group">
											  <label class="col-md-3 control-label"></label>
											  <div class="col-md-4">
											    <button type="submit" class="btn btn-primary">Aceptar</button>
											  </div>
											</div>

											<!-- Info -->
											<div class="confirmacion">
												<div class="row">
													<div class="col-lg-2 text-center">
														<span class="glyphicon glyphicon-warning-sign"></span>
													</div>
													<div class="col-lg-10">
														<h4>El Cambio ha sido registrado</h4>
														<p>El producto deberá ser enviado en un lapso de 7 días a la siguiente dirección a traves de Zoom:<br>
														Av. Intercomunal Guarenas Guatire, Galpón número 5, Oficina de devoluciones<br>
														Telf: 0212-2134567 / 0212-7894563<br>

														Una vez hayamos recibido el producto, comenzaremos a atender su petición.<br>
														Le daremoe respuesta entre 3 y 5 días despues, a travez de un correo electrónico en la sección Mis Compras.</p>
													</div>
													<div class="col-lg-12 text-center">
														<a href="" class="btn btn-primary">Cerrar</a>
													</div>
												</div>
												
											</div>

											</form>

							

									  </fieldset>
									  </div>
									</div>
		    		 			</div>
		    		 			<!-- /Cambiar Producto -->
		    		 		</div>
		    		 		<!-- /Item -->

		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Orden Número: DC-26556 | Estatus: <span class="label label-info">Enviado</span><span class="label label-success">Facturado</span><span class="label label-success">Entregado</span><span class="label label-info">Por retirar en entienda</span><span class="label label-danger">Devuelto</span></h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a href="" class="btn btn-success btn-block  btn-sm buyAgain"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Comprar de nuevo</a>
		    		 				<a href="" class="btn btn-danger btn-block  btn-sm buyAgain"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="" class="btn btn-warning  btn-block btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Hacer Reclamo</a>
									<a class="btn btn-warning btn-block  btn-sm" role="button" data-toggle="collapse" href="#item4" aria-expanded="false" aria-controls="collapseExample"><span class="glyphicon glyphicon-retweet"></span> Cambiar Producto</a>
		    		 			</div>


		    		 			<!-- Cambiar Producto -->
		    		 			<div class="col-lg-12" id="cambiarProducto">

		    		 				<div class="collapse" id="item4">

									  <div class="well">

									  <fieldset>
									  	<legend>Cambiar Producto: Orden DC-26556 <span class="pull-right">Fecha de cambio: 22/05/2016</span></legend>
											
											<form class="form-horizontal" id="devolucion" role="form" data-toggle="validator">


											<!-- Select Basic -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="motivoDevolucion">Motivo de la devolución</label>
											  <div class="col-md-4">
											    <select id="motivoDevolucion" name="selectbasic" class="form-control" required>
											      <option value="">Seleccione una</option>
											      <option value="1">Talla incorrecta</option>
											      <option value="2">Defectuoso</option>
											      <option value="2">Otro</option>
											    </select>
											  </div>
											</div>

											<!-- Textarea -->
											<div class="form-group">
											  <label class="col-md-3 control-label" for="textarea">Observaciones</label>
											  <div class="col-md-8">                     
											    <textarea class="form-control" id="textarea" name="textarea" placeholder="Indique la razón por la cual devuelve el producto" required></textarea>
											  </div>
											</div>

											<!-- Button -->
											<div class="form-group">
											  <label class="col-md-3 control-label"></label>
											  <div class="col-md-4">
											    <button type="submit" class="btn btn-primary">Aceptar</button>
											  </div>
											</div>

											<!-- Info -->
											<div class="confirmacion">
												<div class="row">
													<div class="col-lg-2 text-center">
														<span class="glyphicon glyphicon-warning-sign"></span>
													</div>
													<div class="col-lg-10">
														<h4>El Cambio ha sido registrado</h4>
														<p>El producto deberá ser enviado en un lapso de 7 días a la siguiente dirección a traves de Zoom:<br>
														Av. Intercomunal Guarenas Guatire, Galpón número 5, Oficina de devoluciones<br>
														Telf: 0212-2134567 / 0212-7894563<br>

														Una vez hayamos recibido el producto, comenzaremos a atender su petición.<br>
														Le daremoe respuesta entre 3 y 5 días despues, a travez de un correo electrónico en la sección Mis Compras.</p>
													</div>
													<div class="col-lg-12 text-center">
														<a href="" class="btn btn-primary">Cerrar</a>
													</div>
												</div>
												
											</div>

											</form>

							

									  </fieldset>
									  </div>
									</div>
		    		 			</div>
		    		 			<!-- /Cambiar Producto -->
		    		 		</div>
		    		 		<!-- /Item -->
		    		 	</div>
		    		 	<div class="navigation text-center">
		    		 		<nav aria-label="Page navigation">
							  <ul class="pagination">
							    <li>
							      <a href="#" aria-label="Previous">
							        <span aria-hidden="true">&laquo;</span>
							      </a>
							    </li>
							    <li><a href="#">1</a></li>
							    <li><a href="#">2</a></li>
							    <li><a href="#">3</a></li>
							    <li><a href="#">4</a></li>
							    <li><a href="#">5</a></li>
							    <li>
							      <a href="#" aria-label="Next">
							        <span aria-hidden="true">&raquo;</span>
							      </a>
							    </li>
							  </ul>
						</nav>
		    		 	</div>
		    		 	
	    		 	</div>

	    		 	<!-- Remodal -->
	    		 	<div class="remodal" data-remodal-id="aceptarDevolucion">
			          <button data-remodal-action="close" class="remodal-close"></button>
			          <h1>Aviso</h1>
			          <p>¿Está seguro que desea cambiar este Producto? Este proceso tomará varios días</p>
			          <br>
			          <button data-remodal-action="confirm" class="btn btn-success">Si</button>
			          <button data-remodal-action="confirm" class="btn btn-danger">No</button>
			        </div>
			        <!-- /Remodal -->


	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>

   <script>
      $(document).ready(function() {
		  $.ajax({
			  type: "GET",
			  url: "../../admin/index.php?service=userservices&metodo=ObtenerDatosPerfil&p_id_usuario=<?php echo  $_SESSION['userid']?>",
			  success: function (respuesta) {
				  var ajaxResponse = $.parseJSON(respuesta);
				  var name = ajaxResponse.Persona.Nombre+(ajaxResponse.Persona.Apellido != ''?(' '+ajaxResponse.Persona.Apellido):'');
				  $("#nombre_u").html(name);


			  }
		  });

        $('#desde, #hasta').datepicker({
		    format: "dd/mm/yyyy",
		    language: "es",
		    autoclose: true,
		    todayHighlight: true,
		});

	
		$('#devolucion').validator().on('submit', function (e) {
		  if (e.isDefaultPrevented()) {
		    //No validado
		  } else {
		    $('[data-remodal-id = aceptarDevolucion]').remodal().open();
		  }
		});


      });

    </script>
    <!-- /bootstrap-daterangepicker -->