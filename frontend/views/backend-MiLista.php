 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
	    				  	<h4><span id="nombre_u"></span></h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>
	    		<div class="col-lg-9 miLista" id="misCompras">
	    			<h1>Mi Lista de Deseos</h1>
	    		 	<div class="container-fluid">
		    		 	<div class="results">
                            <table id="table_wishes" class="table table-hover" style="border-collapse:separate; border-spacing: 0 5px;" width="100%">
                                <thead>
                                <tr>
                                    <th style="display:none" ></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
		    		 	</div>
	    		 	</div>
                    <!-- Remodal -->
                    <div class="remodal" data-remodal-id="wishlist-information-message">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Información</h1>
                        <p id="msj-wishlist-information"></p>
                        <br>
                        <button data-remodal-action="confirm" class="btn btn-success">OK</button>
                    </div>
                    <!-- /Remodal -->
                    <div class="remodal" data-remodal-id="wish-confirm-delete">
                        <div id="confirmation-box">
                            <p>¿Desea eliminar este deseo?</p>
                            <input type="hidden" name="wish_id_delete" id="wish_id_delete">
                            <a  data-remodal-action="confirm" class="remodal-confirm confirm-wish-delete"><span>Si</span></a>
                            <a  data-remodal-action="cancel" class="remodal-cancel">No</a>
                        </div>
                    </div>
	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>
 <script type="text/javascript" src="../js/app/misDeseos.js"></script>
