<?php include '../includes/header.php';?>
	
	<div class="backend-user">
		<div class="container ">
			    	<div class="row contentArea" id="cart">
			    		
			    		
			    		<div class="col-lg-3">
			    			<p class="alert alert-info">Aqui se visualizan todos los productos que han sido añadidos al carrito de compras, puede eliminarnos del carrito, editarlos para ajustar algun valor de compra, comprarlos de forma individual o agruparlos para una compra conjunta.</p> 
			    		</div>
			    		<div class="col-lg-9">
			    			<h1>Carrito de Compras</h1>

			    			<table class="table"> 
			    				 <thead> 
			    					<tr> 
			    						<th>
			    							<div class="checkbox" style="margin:0px">
												<label>
													<input type="checkbox" id="all" class="flat"> 
												</label>
											</div>
										</th> 
			    						<th>Producto</th> 
			    						<th class="text-center">Cantidad</th> 
			    						<th class="text-center">Costo unitario</th>
			    						<th></th> 
			    					</tr> 
			    				</thead> 
			    				<tbody>
								<?php foreach ($listaProductos['Productos'] as $producto) {
								$nombre_imagen="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][0]['color']."/".$producto['ImagenesSelec'][0]['nombre'];
								if (!file_exists($nombre_imagen)){
									$nombre_imagen="../images/noImageThumb.jpg";
								}
								?>
			    					<tr>
										<input type="hidden" id="color-<?php echo $producto['CodProducto'];?>" name="color-<?php echo $producto['CodProducto'];?> " value="<?php echo $producto['CodColor']; ?>">
										<input type="hidden" id="talla-<?php echo $producto['CodProducto'];?>" value="<?php echo $producto['CodTalla'];?>">
										<td class="checkOption">
				    						<div class="checkbox">
											<label>
												<input type="checkbox" class="flat check seleccionar" data-CodProducto="<?php echo $producto['CodProducto'];?>">
											</label>
											</div>
										</td> 
			    						<td>
				    						<img src="<?php echo $nombre_imagen;?>">
				    						<h3><?php echo substr($producto['Nombre'],0,25).'...'; ?></h3>
				    					</td>
			    						<td class="options" align="center">
											<div id="divcantidad-<?php echo $producto['CodProducto']; ?>" class="disNone">
											<input type="number" class="form-control" style="width: 60px;" id="cantidad-<?php echo $producto['CodProducto'];?>" value="<?php echo $producto['Cantidad'];?>">
											<input type="hidden" class="form-control" style="width: 60px;" id="cantidadAnterior-<?php echo $producto['CodProducto'];?>" value="<?php echo $producto['Cantidad'];?>">
											<input type="hidden" class="form-control"  id="costo-<?php echo $producto['CodProducto'];?>" value=" <?php echo $producto['PrecioUnitario'];?>">
											</div>
											<div id="divlcantidad-<?php echo $producto['CodProducto']; ?>">
											<?php echo $producto['Cantidad']; ?>
											</div>
										</td>
			    						<td class="options" align="center">Bs <?php echo $producto['PrecioUnitario']; ?></td>
			    						<td class="options">
											<div id="botones-princ-<?php echo $producto['CodProducto']; ?>">
			    							<a  class="btn btn-danger borrar" data-CodProducto="<?php echo $producto['CodProducto']; ?>"><span class="glyphicon glyphicon-remove"></span></a>
			    							<a  class="btn btn-info edit" data-CodProducto="<?php echo $producto['CodProducto']; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
			    							<a  class="btn btn-success buyNow" data-CodProducto="<?php echo $producto['CodProducto']; ?>"><span class="glyphicon glyphicon-credit-card"></span></a>
											</div>
											<div id="botones-edit-<?php echo $producto['CodProducto']; ?>" class="disNone">
											<a  class="btn btn-success acept" data-CodProducto="<?php echo $producto['CodProducto']; ?>"><span class="glyphicon glyphicon-ok"></span></a>
											<a  class="btn btn-danger  cancel" data-CodProducto="<?php echo $producto['CodProducto']; ?>"><span class="glyphicon glyphicon-remove"></span></a>
											</div>
										</td>
			    					</tr> 
			    					<?php }?>
			    				</tbody>
			    			</table>
							<table class="table">
								<tr>
									<td align="center" width="50%"><strong> SubTotal</strong></td>
									<td><strong><span id="subtotal">Bs. 0</span></strong></td>
								</tr>
							</table>



			    			<div class="text-center">
			    				<a id="pagar-selec" class="btn btn-primary">Pagar Seleccionados</a>
			    			</div>
			    		</div>

			    	</div>
		</div>	
	</div>

 <?php include '../includes/footer.php';?>
<script src="../js/views/cart.js"></script>
