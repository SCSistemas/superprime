<?php include '../includes/header.php';?>

<div class="register">



    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Dirección de Facturación</h3>

          <form id="register" method="post" action="../pages/process-register.php" role="form" data-toggle="validator">

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                  <label for="nombre" class="cols-sm-2 control-label">Estado</label>
                  <select class="form-control" name="estado" id="estado" required>
                      <option value="">-Seleccione-</option>
                      <?php foreach($estados as $estado){ ?>
                          <option value="<?php echo $estado["CodEstado"]?>" ><?php echo $estado["DesEstado"] ?></option>
                        <?php }?>
                      
                    </select>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                  <label for="nombre" class="cols-sm-2 control-label">Ciudad</label>
                  <select class="form-control" name="ciudad" id="ciudad"  disabled="true" required>
                      <option value="">-Seleccione-</option>
                    </select>
                    <span> </span>

              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
             <div class="form-group">
                  <label for="nombre" class="cols-sm-2 control-label">Municipio</label>
                  <select class="form-control" name="municipio" id="municipio"  disabled="true" required>
                     <option value="">-Seleccione-</option>
                  </select>
                  <span></span>
              </div> 
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                  <label for="nombre" class="cols-sm-2 control-label">Parroquia</label>
                  <select class="form-control" name="parroquia" id="parroquia"  disabled="true" required>
                      <option value="">-Seleccione-</option>
                  </select>
                  <span></span>
              </div>
            </div>
          </div>


          <div class="form-group">
              <label for="nombre" class="cols-sm-2 control-label">Código Postal</label>
              <input type="text" class="form-control" name="codigoPostal" id="codigoPostal"  placeholder="Código Postal" required/>
               <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
              <label for="nombre" class="cols-sm-2 control-label">Calle 1</label>
              <input type="text" class="form-control" name="calle1" id="calle1"  placeholder="Calle" required/>
               <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
              <label for="nombre" class="cols-sm-2 control-label">Calle 2 <span>(Opcional)</span></label>
              <input type="text" class="form-control" name="calle2" id="calle2"  placeholder="Calle"/>
               <div class="help-block with-errors"></div>
          </div>


          
              <div class="form-group">
                  <label for="nombre" class="cols-sm-2 control-label">Tipo de Vivienda</label>
                  <select class="form-control" name="tipoVivienda" id="tipoVivienda" required>
                      <option value="">-Seleccione-</option>
                      <option value="V">Casa</option>
                      <option value="E">Edificio</option>
                    </select>
              </div>
             <div class="form-group">
              <label for='nombre' class='cols-sm-2 control-label'>Nombre de Vivienda</label>
              <input type='text' class='form-control' name='nameC' id='nameC'  placeholder='Nombre' required="" />
              <div class="help-block with-errors"></div>
          </div>

        
              <div class="form-group" id="tipoV">
                <!-- se colocan input depediendo de la selección de tipo de vivienda-->
              </div>
            
          

         

          <div class="form-group">
            <div class="row">
              <div class="col-lg-4">
                  <label for="tcel" class="cols-sm-2 control-label">Telf Principal</label>
                  <input type="text"   id="tcel" name="tcel" class="form-control bfh-phone" minlength="14" data-format="(dddd)ddd-dddd"/>
              </div>
              <div class="col-lg-4">
                  <label for="thab"" class="cols-sm-2 control-label">Telf Secundario</label>
                  <input type="text" id="thab" name="thab" class="form-control bfh-phone"  minlength="14" data-format="(dddd)ddd-dddd"/>
              </div>
              <div class="col-lg-4">
                  <label for="tofi" class="cols-sm-2 control-label">Telf Otro</label>
                  <input type="text" id="tofi" name="tofi" class="form-control bfh-phone"  minlength="14" data-format="(dddd)ddd-dddd"/>
              </div>
              <div class="col-lg-12" id="telefonos">
              </div>
            </div>
            <div class="help-block with-errors"></div>
          </div>


          <div class="checkbox">
            <label>
              <input id='condiciones' name='condiciones'  data-validation-minchecked-message="Choose two" required type="checkbox" value="">
              Acepto los términos y condiciones
            </label>
            <div class="help-block with-errors"></div>
          </div>

          <div class="checkbox">
            <label>
              <input type="checkbox" value="" >
              Quiero recibir ofertas y noticias en el correo electrónico
            </label>
          </div>
           
            <div class="form-group ">
              <button type="submit" id="cargar" name="cargar" class="btn btn-primary btn-lg btn-block login-button" >Terminar</button>
            </div>

            </form>
        </div>

        <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Registro realizado</h1>
          <p>Gracias por registrarse como cliente Super Prime. Ya puedes comenzar a comprar</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>

    </div>
</div>
<?php include '../includes/footer.php';?>
<script language="javascript" type="text/javascript">

    $("#tcel").on('keyup', function(){
        var value = $(this).val().length;
        if(value==1){
            $("#telefonos").html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        }else{
            $("#telefonos").html("");

        }
    });

    $("#thab").on('keyup', function(){
        var value = $(this).val().length;
        if(value==1){
            $("#telefonos").html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        }else{
            $("#telefonos").html("");

        }
    });

    $("#tofi").on('keyup', function(){
        var value = $(this).val().length;
        if(value==1){
            $("#telefonos").html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        }else{
            $("#telefonos").html("");

        }
    });

    $('form').submit(function(e){
        // si la cantidad de checkboxes "chequeados" es cero,
        // entonces se evita que se envíe el formulario y se
        // muestra una alerta al usuario
        if ($("#tcel").val()=='(' && $("#thab").val()=='(' && $("#tofi").val()=='(' ) {
            e.preventDefault();
            $("#telefonos").html("<ul class='list-unstyled'><li><font color='#a94442'> Debe especificar al menos un número telefónico</font></li></ul>");
        }else{
            $("#telefonos").html("");
        }
    });
    
    $("#tipoVivienda").change(function(){
      if ($("#tipoVivienda").val() == 'E'){
        $("#tipoV").html("<label for='nombre' class='cols-sm-2 control-label'>Piso/Apartamento</label><div class='row'><div class='col-lg-6'><input type='text' class='form-control' name='piso' id='piso'  placeholder='Piso' /></div><div class='col-lg-6'><input type='text' class='form-control' name='apartamento' id='apartamento'  placeholder='Apto' /></div>");
      }else
          $("#tipoV").html("");

    })

    $("#estado").change(function(){
        //<![CDATA[
        $("#ciudad").next("span").html("<img src='../images/ui-anim_basic_16x16.gif' title='' alt='' />");
        //]]>
        $("#ciudad option:eq(0)").attr("selected", "selected");
        $("#ciudad").attr("disabled", "disabled");
        $("#municipio option:eq(0)").attr("selected", "selected");
        $("#municipio").attr("disabled", "disabled");
        $("#parroquia option:eq(0)").attr("selected", "selected");
        $("#parroquia").attr("disabled", "disabled");

        
        $.getJSON("../ajax/cargarCiudades.php",
        {
          idestado: $("#estado").val()
        }, 
        function(data) {
            if (data != ''){
            $("select#ciudad").html(data);
            $("#ciudad").next("span").html("");
            $('#ciudad').append($('<option>', {
                value: '',
                text: '-Seleccione-'
            }));

            $.each(data, function (i, item) {
                $('#ciudad').prop('disabled', false);
                $('#ciudad').append($('<option>', {
                    value: item.CodCiudad,
                    text: item.DescCiudad
                }));
            });
            }else{
                $('#ciudad').removeAttr('required');

            }
        })
    })
    
    $("#ciudad").change(function(){
        //<![CDATA[
        $("#municipio").next("span").html("<img src='../images/ui-anim_basic_16x16.gif' title='' alt='' />");
        //]]>
        $("#municipio option:eq(0)").attr("selected", "selected");
        $("#municipio").attr("disabled", "disabled");
        $("#parroquia option:eq(0)").attr("selected", "selected");
        $("#parroquia").attr("disabled", "disabled");

        $.getJSON("../ajax/cargarMunicipios.php",
        {
          idestado: $("#estado").val(),
          idciudad: $("#ciudad").val(),
        }, 
        function(data) {
          if (data != ''){
  		      $("select#municipio").html(data);
  		      $("#municipio").next("span").html("");
  		      $('#municipio').append($('<option>', { 
  		        value: '',
  		        text : '-Seleccione-'
  		      }));
            $.each(data, function(i, item) {
              $('#municipio').prop('disabled',false);
              $('#municipio').append($('<option>', { 
                value: item.CodMunicipio,
                text : item.DescMunicipio
              }));
            });

          }else{
              $('#municipio').removeAttr('required');
          }

         
        })
    })
    
    $("#municipio").change(function(){
        //<![CDATA[
        $("#parroquia").next("span").html("<img src='../images/ui-anim_basic_16x16.gif' title='' alt='' />");
        //]]>
        $("#parroquia option:eq(0)").attr("selected", "selected");
        $("#parroquia").attr("disabled", "disabled");
           
        $.getJSON("../ajax/cargarParroquias.php",
        {
          idestado: $("#estado").val(),
          idciudad: $("#ciudad").val(),
          idmunicipio: $("#municipio").val()
        }, 
        function(data) {
          if (data != ''){
             $("select#parroquia").html(data);
             $("select#parroquia").attr('data-validate', true);
             //$("#register").validator('update').validator('validate');
  		       $("#parroquia").next("span").html("");
      		   $('#parroquia').append($('<option>', { 
      		        value: '',
      		        text : '-Seleccione-'
      		    }));

            $.each(data, function(i, item) {
              $('#parroquia').prop('disabled',false);
              $('#parroquia').append($('<option>', { 
      		        value: item.CodParroquia,
      		        text : item.DescParroquia
      		    }));
            });
          }else{
              $('#parroquia').removeAttr('required');
          }
        })
    })


</script>