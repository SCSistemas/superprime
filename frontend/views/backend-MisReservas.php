 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
							  <h4><span id="nombre_u"></span></h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>


	    		<div class="col-lg-9" id="misCompras">

	    			<h1>Mis Reservas</h1>

	    		 	<form>
	    		 		<fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="row">
                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" id="desde" name="desde" placeholder="Hasta" required="required">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                   <div class="input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" id="hasta" name="hasta" placeholder="Hasta" required="required">
                                  </div>
                                </div>
                                <div class="col-md-5" style="width:36.666667%">
                                  <div class="form-inline">
                                    <div class="form-group">
                                      	<label for="categorías" class="cols-sm-2 control-label">Filtrar por Categorías:</label>
                                      	<select class="form-control" name="categorías" id="categorías">
						                  <option></option>
						                  <option value="Zapatos">Zapatos</option>
						                  <option value="Cateras">Cateras</option>
						                  <option value="Juguetes">Juguetes</option>
						                  <option value="Accesorios">Accesorios</option>
						                </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-1">
                                	<button type="submit" class="btn btn-primary">Buscar</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </fieldset>	
	    		 	</form>


	    		 	<div class="container-fluid">
		    		 	<div class="results">

		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Forma de pago: Depósito o Transferencia | Reserva válida hasta: 06/04/2016 1:00pm</h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a class="btn btn-danger btn-block btn-sm buyAgain" data-remodal-target="cancelar"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="../pages/cart-continue.php" class="btn btn-warning btn-block btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Cambiar forma de pago</a>
									<a href="../pages/backend-PagoBanca.php" class="btn btn-success btn-block btn-sm"  ><i class="fa fa-check" aria-hidden="true"></i>  Registrar Pago</a>
		    		 			</div>
		    		 			<div class="col-lg-12 timeline">
		    		 				<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Quedan menos de 8 horas para que completes el pago. No dejes perder tu reserva</div>
		    		 			</div>

		    		 		</div>
		    		 		<!-- /Item -->

		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Forma de pago: Depósito o Transferencia | Reserva válida hasta: 06/04/2016 1:00pm</h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a class="btn btn-danger btn-block btn-sm buyAgain" data-remodal-target="cancelar"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="../pages/cart-continue.php" class="btn btn-warning btn-block btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Cambiar forma de pago</a>
									<a href="../pages/backend-PagoBanca.php" class="btn btn-success btn-block btn-sm"  ><i class="fa fa-check" aria-hidden="true"></i>  Registrar Pago</a>
		    		 			</div>
		    		 			<div class="col-lg-12 timeline">
		    		 				<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Quedan menos de 8 horas para que completes el pago. No dejes perder tu reserva</div>
		    		 			</div>

		    		 		</div>
		    		 		<!-- /Item -->

		    		 		<!-- Item -->
		    		 		<div class="row item">
		    		 			<h2>Forma de pago: Depósito o Transferencia | Reserva válida hasta: 06/04/2016 1:00pm</h2>
		    		 			<div class="col-lg-2">
		    		 				<img src="../images/image.jpg" class="img-responsive">
		    		 			</div>
		    		 			<div class="col-lg-7">
		    		 				<h3 class="producto">Deportivo RS21 para Caballeros</h3>
		    		 				<h3>Modelo Paseo 1015</h3>
		    		 				<h3>Precio: Bs 19.500</h3>
		    		 				<h3 class="total">Total: Bs 19.500</h3>
		    		 			</div>
		    		 			<div class="col-lg-3 text-center">
		    		 				<a class="btn btn-danger btn-block btn-sm buyAgain" data-remodal-target="cancelar"> <span class="glyphicon glyphicon-ban-circle"></span> Cancelar Compra</a>
		    		 				<a href="../pages/cart-continue.php" class="btn btn-warning btn-block btn-sm buyAgain"> <span class="glyphicon glyphicon-exclamation-sign"></span> Cambiar forma de pago</a>
									<a href="../pages/backend-PagoBanca.php" class="btn btn-success btn-block btn-sm"  ><i class="fa fa-check" aria-hidden="true"></i>  Registrar Pago</a>
		    		 			</div>
		    		 			<div class="col-lg-12 timeline">
		    		 				<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Quedan menos de 8 horas para que completes el pago. No dejes perder tu reserva</div>
		    		 			</div>

		    		 		</div>
		    		 		<!-- /Item -->

		    		 	</div>
		    		 	<div class="navigation text-center">
		    		 		<nav aria-label="Page navigation">
							  <ul class="pagination">
							    <li>
							      <a href="#" aria-label="Previous">
							        <span aria-hidden="true">&laquo;</span>
							      </a>
							    </li>
							    <li><a href="#">1</a></li>
							    <li><a href="#">2</a></li>
							    <li><a href="#">3</a></li>
							    <li><a href="#">4</a></li>
							    <li><a href="#">5</a></li>
							    <li>
							      <a href="#" aria-label="Next">
							        <span aria-hidden="true">&raquo;</span>
							      </a>
							    </li>
							  </ul>
						</nav>
		    		 	</div>
		    		 	
	    		 	</div>

	    		 	<!-- Remodal -->
	    		 	<div class="remodal" data-remodal-id="cancelar">
			          <button data-remodal-action="close" class="remodal-close"></button>
			          <h1>Aviso</h1>
			          <p>¿Está seguro que desea cancelar este Producto?</p>
			          <br>
			          <button data-remodal-action="confirm" class="btn btn-success">Si</button>
			          <button data-remodal-action="confirm" class="btn btn-danger">No</button>
			        </div>
			        <!-- /Remodal -->


	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>

   <script>
      $(document).ready(function() {
		  $.ajax({
			  type: "GET",
			  url: "../../admin/index.php?service=userservices&metodo=ObtenerDatosPerfil&p_id_usuario=<?php echo  $_SESSION['userid']?>",
			  success: function (respuesta) {
				  var ajaxResponse = $.parseJSON(respuesta);
				  var name = ajaxResponse.Persona.Nombre+(ajaxResponse.Persona.Apellido != ''?(' '+ajaxResponse.Persona.Apellido):'');
				  $("#nombre_u").html(name);


			  }
		  });

        $('#desde, #hasta').datepicker({
		    format: "dd/mm/yyyy",
		    language: "es",
		    autoclose: true,
		    todayHighlight: true,
		});

	
		$('#devolucion').validator().on('submit', function (e) {
		  if (e.isDefaultPrevented()) {
		    //No validado
		  } else {
		    $('[data-remodal-id = aceptarDevolucion]').remodal().open();
		  }
		});


      });

    </script>
    <!-- /bootstrap-daterangepicker -->