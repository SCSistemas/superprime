 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
	    				  	<h4>Carlos Rojas</h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>


	    		<div class="col-lg-9" id="bankPaiment">

	    			<h1>Carrito de compras  | Orden número: DC-6645321-23</h1>

	    		 	<div class="container-fluid">
	    		 		<h6 class="green"><i class="fa fa-check" aria-hidden="true"></i> Compra exitosa</h6>

	    		 		<div class="alert alert-info" role="alert">Gracias por tu compra, te enviaremos un correo electrónico para confirmar el éxito de la operación realizada.</div>
	    		 		
	    		 		<p>Recuerda retirar tu producto en la siguiente tienda.</p>

	    		 		<div class="panel panel-default">
						  <div class="panel-body">
						    <p>
							    <strong>Tienda</strong> Prime Shoes Sambil Caracas Av Libertador <br> 
			    		 		<strong>Dirección</strong> C.C Sambil Caracas Nivel Feria Local Fc-18 Chacao <br>	
			    		 		<strong>Telf:</strong> 0212 3659565 | 0212 9056584 <br>
			    		 		<strong>Horario:</strong> Lunes a Domingo de 12:00m a 7:00pm
		    		 		</p>
						  </div>
						</div>

						<div class="col-lg-6 resumen">
							<h1>Comprobante de Pago</h1>
							<dl class="dl-horizontal">
							  <dt>Referencia</dt>
							  <dd>MC-1654321</dd>
							  <dt>Tarjeta de crédito</dt>
							  <dd>Visa - 1212 ●●●● ●●●● ●●89</dd>
							  <dt>Titular</dt>
							  <dd>Carlos Rojas</dd>
							  <dt>Fecha de Pago</dt>
							  <dd>15/08/2016 - 8:00am</dd>
							  <dt>Monto</dt>
							  <dd>Bs 158.652</dd>
							</dl>
							<div class="col-lg-8 text-muted">Este no es un comprobante fiscal</div>
							<div class="col-lg-4 text-right"><a href="" class="print" data-toggle="tooltip" data-placement="top" title="Imprimir"><i class="fa fa-print" aria-hidden="true"></i></a></div>
						</div>

						<div class="col-lg-6 detalleCompra">
							<h1>Detalle</h1>
							<!-- item -->
							<div class="row">
	                            <div class="col-lg-3">
	                                <img src="../images/4.png" class="img-responsive">
	                            </div>
	                            <div class="col-lg-9 details">
	                                <small class="nombre">Deportivo RS21 - Bota Basketball</small>
	                                <small class="talla">Talla: 45 - Color: <span style="color:#e91e63">●</span></small>
	                                <small class="precio">Precio Bs: 12.500</small>
	                            </div>
	                        </div>
	                        <!-- item -->
	                        <!-- item -->
							<div class="row">
	                            <div class="col-lg-3">
	                                <img src="../images/3.png" class="img-responsive">
	                            </div>
	                            <div class="col-lg-9 details">
	                                <small class="nombre">Deportivo RS21 - Bota Basketball</small>
	                                <small class="talla">Talla: 45 - Color: <span style="color:#cddc39">●</span></small>
	                                <small class="precio">Precio Bs: 12.500</small>
	                            </div>
	                        </div>
	                        <!-- item -->
	                        <!-- item -->
							<div class="row">
	                            <div class="col-lg-3">
	                                <img src="../images/6.png" class="img-responsive">
	                            </div>
	                            <div class="col-lg-9 details">
	                                <small class="nombre">Deportivo RS21 - Bota Basketball</small>
	                                <small class="talla">Talla: 45 - Color: <span style="color:#03a9f4">●</span></small>
	                                <small class="precio">Precio Bs: 12.500</small>
	                            </div>
	                        </div>
	                        <!-- item -->
						</div>
	    		 		


		    		 	
	    		 	</div>


			        


	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>

   <script>
      $(document).ready(function() {



      });

    </script>
