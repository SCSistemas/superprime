<?php include '../includes/header.php';?>

<div class="backend-user">
	<div class="container">
    	<div class="row contentArea" id="cart">
    		
    		
    		<div class="col-lg-3">
    			<p class="alert alert-info">Cambiar por un mensaje proporcionando ayuda sobre el proceso de pago.</p> 
    		</div>
    		<div class="col-lg-9">
                
                <h1>Carrito de Compras - Envío de Productos</h1>
        		
                <div class="container-fluid">
                    <div class="row groupProducts">
                    <h1>Sucursal Prime Shoes - C.C Sambil Caracas</h1>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/1.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="color">Color: <i class="fa fa-circle blue" aria-hidden="true"></i></small>
                                <small class="talla">Talla: 45</small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/3.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="color">Color: <i class="fa fa-circle green" aria-hidden="true"></i></small>
                                <small class="talla">Talla: 45</small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/6.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- Para retirar -->
                    <div class="clearfix"></div>
                    <div class="row retirement">
                        <div class="col-lg-6">
                            <div class="radio">
                              <label>
                                <input type="radio" name="retirement1" class="flat" value="direction1">
                                Enviar a una dirección registrada
                                </label>
                            </div>

                            <div id="direction1" class="message" style='display:none'>
                                <a href="../backend-MisDatos.php" class="pull-right" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> Urb, Las Islas, Edificio Domusqui, Piso 3 Apt 07, Guarenas, Edo Miranda  <br>
                                <span><span class="icon-delivery"></span> Costo de envío: Bs. 12.500</span>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="radio">
                              <label>
                                <input type="radio" name="retirement1" class="flat" value="personal1">
                                Ahorrate el costo de envío y retiralo en una tienda
                                </label>
                            </div>

                            <div id="personal1"  class="message" style='display:none'>
                
                                <div class="msg2">
                                    <i class="fa fa-exclamation-triangle pull-left" aria-hidden="true"></i> Los productos están en diferentes tiendas ubicados en varias ciudades
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row groupProducts">
                    <h1>Sucursal RR21 Experience - La Urbina Caracas</h1>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/4.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="talla">Talla: 45</small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/1.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="color">Color: <i class="fa fa-circle gray" aria-hidden="true"></i></small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/7.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="talla">Talla: 45</small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- item -->
                    <div class="col-lg-6 item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="../images/8.png" class="img-responsive">
                            </div>
                            <div class="col-lg-8 details">
                                <small class="nombre">Deportivo RS21</small>
                                <small class="detalle">Bota Basketball</small>
                                <small class="color">Color: <i class="fa fa-circle red" aria-hidden="true"></i></small>
                                <small class="precio">Precio Bs: 12.500</small>
                            </div>
                        </div>
                    </div>

                    <!-- Para retirar -->
                    <div class="clearfix"></div>
                    <div class="row retirement">
                        <div class="col-lg-6">
                            <div class="radio">
                              <label>
                                <input type="radio" name="retirement2" class="flat" value="direction2">
                                Enviar a una dirección registrada
                                </label>
                            </div>

                            <div id="direction2" class="message" style='display:none'>
                                <a href="../backend-MisDatos.php" class="pull-right" data-toggle="tooltip" data-placement="top" title="Modificar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> Urb, Las Islas, Edificio Domusqui, Piso 3 Apt 07, Guarenas, Edo Miranda<br>
                                <span><span class="icon-delivery"></span> Costo de envío: Bs. 10.500</span> 
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="radio">
                              <label>
                                <input type="radio" name="retirement2" class="flat" value="personal2">
                                Ahorrate el costo de envío y retiralo en una tienda
                                </label>
                            </div>

                            <div id="personal2"  class="message" style='display:none'>
                                <div class="msg1">
                                    Tu pedido está en <strong>Prime Shoes Sambil Caracas</strong><br>
                                   Av Libertador, C.C Sambil Caracas Nivel Feria Local Fc-18 Chacao<br>
                                   Telf: 0212 3659565 | 0212 9056584    
                                </div>
                    

                            </div>

                        </div>
                    </div>
                </div>

                <h2 class="total">Total a pagar Bs 245.000</h2>

                <div class="text-center">
                    <br>
                    <a href="cart-continue.php" class="btn btn-warning">Ir atras</a>
                    <a href="" class="btn btn-success" >Continuar</a>
                </div>
                </div>
                
                 


    		</div>

</div>
</div>
</div>

 <?php include '../includes/footer.php';?>

<script type="text/javascript">
	$(document).ready(function() {	

		// Activar tooltip
		$('[data-toggle="tooltip"]').tooltip()

		// Mostrar direcciones de envio con radiobutton
		$("input[name='retirement1']").on('ifChecked', function(event) {
		    $('#direction1').css('display', ($(this).val() === 'direction1') ? 'block':'none');
		});

		$("input[name='retirement1']").on('ifChecked', function(event) {
		    $('#personal1').css('display', ($(this).val() === 'personal1') ? 'block':'none');
		});



		$("input[name='retirement2']").on('ifChecked', function(event) {
		    $('#direction2').css('display', ($(this).val() === 'direction2') ? 'block':'none');
		});

		$("input[name='retirement2']").on('ifChecked', function(event) {
		    $('#personal2').css('display', ($(this).val() === 'personal2') ? 'block':'none');
		});
	});
</script>

