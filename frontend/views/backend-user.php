 <?php include 'includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<ul class="userMenu">
	    				<li><a href="">Mis Compras</a></li>
	    				<li><a href="">Mis Reservas</a></li>
	    				<li><a href="">Mi Lista de Deseos</a></li>
	    				<li><a href="">Mis Datos</a></li>
	    			</ul>
	    		</div>
	    		<div class="col-lg-9">

	    			<h1>Mis Compras</h1>
	    		 	<fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="row">
                                <div class="col-md-3">
                                  <div class="input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" id="fecha_desde" name="fecha_desde" placeholder="Desde" >
                                  </div>
                                </div>
                                <div class="col-md-3">
                                   <div class="input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" id="fecha_hasta" name="fecha_hasta" placeholder="Hasta" required="required">
                                  </div>
                                </div>
                                <div class="col-md-5">
                                  <div class="form-inline">
                                    <div class="form-group">
                                      <input type="text" id="cod_prod" name="cod_prod" style="width:80px" class="form-control" placeholder="Código" required="required">
                                      <input type="text" id="desc_sap" name="desc_sap" class="form-control" placeholder="Descripción" required="required">
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>
                        </fieldset>

	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include 'includes/footer.php';?>