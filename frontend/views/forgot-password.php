<?php include '../includes/header.php';?>

  <div class="register">



    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Recuperar contraseña</h3>

          <form id="register" action="../pages/forgot-password.php" role="form" data-toggle="validator">

            <div class="form-group">
              <label for="email" class="cols-sm-2 control-label">E-mail</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="email" id="email"  placeholder="Email" required/>
                </div>
              </div>
            </div>

            <div class="form-group ">
              <!--<button type="submit" class="btn btn-primary btn-lg btn-block login-button" data-remodal-target="reporte" >Enviar</button>-->
              <button type="submit" class="btn btn-primary btn-lg btn-block login-button" >Enviar</button>
            </div>




            </form>
        </div>

        <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p id='msj'>Para resguardar su información, le enviamos un correo electrónico con un enlace para continuar con el proceso de registro.</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>

    </div>
    </div>
   <?php include '../includes/footer.php';?>

   <script type="text/javascript"> 
    $( document ).ready(function() {

       
        var val = <?php echo $result['success']?>;
        //var msj = <?php echo $user['msg']?>;

        console.log(val);
        $('#msj').html('');

        if (val == 1){
          $('#msj').html('Se he enviado un correo para continuar el proceso...');
          $('[data-remodal-id=reporte]').remodal().open();
        }

        if (val == 0){
          $('#msj').html('Disculpe, por favor intente nuevamente');
          $('[data-remodal-id=reporte]').remodal().open();
        }
        
    });
</script>