<?php include '../includes/header.php';?>

  <div class="register">



    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Restauración de contraseña</h3>

          <form id="register" role="form" method="post" action="../pages/new-password.php" data-toggle="validator">


            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Ingrese su nueva contraseña</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" data-minlength="8" class="form-control" name="password" id="inputPassword"  placeholder="Elije una contraseña" required/>
                  
                </div>
                <span class="help-block">Debe tener una extensión de
al menos ocho caracteres, letras y números, al menos una letra en mayúscula</span>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Confirmar Contraseña</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" class="form-control" name="password" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Las contraseñas no coindicen"  placeholder="Repite la contraseña" required/>
                </div>
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div hidden="true">
            <input type="text" id='tok' name='tok' hidden="true" value="<?php echo $_GET['token']?>"/>
            <input type="text" id='idu' name='idu' hidden="true" value="<?php echo $_GET['idusuario']?>" />
            </div>

            <div class="form-group ">
              <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Guardar</button>
            </div>

            </form>

            <div id="pswd_info">
                <h4>La contraseña debe cumplir los siguientes requisitos:</h4>
                <ul>
                  <li id="letter" class="invalid">Mínimo <strong>una letra</strong></li>
                  <li id="capital" class="invalid">Mínimo <strong>una mayúscula</strong></li>
                  <li id="number" class="invalid">Mínimo <strong>un número</strong></li>
                  <li id="length" class="invalid">Mínimo <strong>8 caracteres</strong></li>
                </ul>
            </div>

        </div>

        <div class="remodal" data-remodal-id="modal">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p id='msj'>Su nueva contraseña ha sido guardada, ya puede iniciar sesión nuevamente</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>

    </div>
    </div>
   <?php include '../includes/footer.php';?>
    <script type="text/javascript"> 
      $( document ).ready(function() {

          var val = "<?php echo $resultData['success']?>";  
          //var msj = <?php echo $user['msg']?>;

          var val2 = <?php echo $result['success']?>;

          console.log('Valor 1=   '+val);
          console.log('Valor 2=   '+val2);
          $('#msj').html('');

          if (val == 1){
            $('#msj').html('Cambio de Contraseña satisfactorio...');
            $('[data-remodal-id=modal]').remodal().open();
          }

          if (val == 0){
            $('#msj').html('Disculpe, por favor intente nuevamente');
            $('[data-remodal-id=modal]').remodal().open();
          }

          if (val2 == 0){
            $('#msj').html('Token Invalido');
            $('[data-remodal-id=modal]').remodal().open();
          } 
          
      });
    </script>