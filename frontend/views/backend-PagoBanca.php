 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
	    				  	<h4>Carlos Rojas</h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>


	    		<div class="col-lg-9" id="bankPaiment">

	    			<h1>Reportar Pago | Orden número: DC-6645321-23</h1>

	    		 	<div class="container-fluid">
	    		 		<h6><span class="icon-bank"></span> Deposito o Transferencia</h6>
	    		 		<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Registre los datos de su deposito ó transferencia bancaria</div>
	    		 		

	    		 		<div class="panel panel-default">
						  <div class="panel-body">
						  	<span class="total">Total Bs: 158.265</span>
						    <button class="btn btn-info btn-sm pull-right" type="button" data-remodal-target="resume">
							  <span class="icon-cart"></span> Ver detalles
							</button>
						  </div>
						</div>

	    		 		

	    		 		<form class="form-horizontal" data-toggle="validator" role="form" >


							<!-- Select Basic -->
							<div class="form-group">
							  <label class="col-md-4 control-label" for="selectbasic">Seleccione el número de cuenta</label>
							  <div class="col-md-6">
							    <select id="selectbasic" name="selectbasic" class="form-control" required>
							      <option value=""></option>
							      <option value="1">Option one</option>
							      <option value="2">Option two</option>
							    </select>
							  </div>
							</div>

							<!-- Text input-->
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Fecha de deposito o transferencia</label>  
							  <div class="col-md-3">
							  	<div class="input-group">
							  		<input name="textinput" type="text" id="fecha"  class="form-control input-md" required="">
							    	<div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
							  	</div>
							  </div>
							</div>

							<!-- Text input-->
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Número de movimiento ó transferencia</label>  
							  <div class="col-md-3">
							  	<div class="input-group">
							  		<input name="textinput" type="text" class="form-control input-md numeric" required="">
							  		<div class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"  type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="6 últimos digitos de referencia o comprobante del depósito ó transferencia"></i></div>
							    </div>
							  </div>
							</div>

							<!-- Text input-->
							<div class="form-group">
							  <label class="col-md-4 control-label" for="textinput">Monto del movimiento ó transferencia</label>  
							  <div class="col-md-3">
							  <input  name="textinput" type="text" class="form-control input-md numeric" required="">
							    
							  </div>
							</div>

							<!-- Multiple Radios -->
							<div class="form-group">
							  <label class="col-md-4 control-label" for="radios">Indique el tipo de operación realizada</label>
							  <div class="col-md-6">
							  <div class="radio">
							    <label for="radios-0">
							      <input type="radio" class="flat" name="radios" id="radios-0" value="1"  required>
							      Deposito o transferencia del mismo banco
							    </label>
								</div>
							  <div class="radio">
							    <label for="radios-1">
							      <input type="radio" class="flat" name="radios" id="radios-1" value="2" required>
							      Transferencia desde otro banco
							    </label>
								</div>
							  </div>
							</div>

							<div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> El monto del depósito ó transferencia indicada no coincide con el monto total de los artículos seleccionados</div>

							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-4 control-label" for="singlebutton"></label>
							  <div class="col-md-4">
							    <button id="singlebutton" type="submit" name="singlebutton" class="btn btn-success">Registrar pago</button>
							  </div>
							</div>

						</form>

		    		 	
	    		 	</div>

	    		 	<!-- Remodal -->
	    		 	<div class="remodal" data-remodal-id="cancelar">
			          <button data-remodal-action="close" class="remodal-close"></button>
			          <h1>Aviso</h1>
			          <p>¿Está seguro que desea cancelar este Producto?</p>
			          <br>
			          <button data-remodal-action="confirm" class="btn btn-success">Si</button>
			          <button data-remodal-action="confirm" class="btn btn-danger">No</button>
			        </div>
			        <!-- /Remodal -->

			        <!-- Remodal -->
	    		 	<div class="remodal remodal-sm" data-remodal-id="resume">
			          <button data-remodal-action="close" class="remodal-close"></button>
			          <h1>Resumen</h1>
			          <div class="resumen">
				            <span class="help-block">A continuación se muestra el detalle del pago a realizar</span>

				            <table class="table table-striped">
				            	<tr>
				            		<td>Zapatos Nike 23343</td>
				            		<td class="text-right">Bs 965.32</td>
				            	</tr>
				            	<tr>
				            		<td>Zapatos Kriza night</td>
				            		<td class="text-right">Bs 1.265.32</td>
				            	</tr>
				            	<tr>
				            		<td>Cartera fem 089</td>
				            		<td class="text-right">Bs 1.665.32</td>
				            	</tr>
				            	<tr>
				            		<td>Juguete para niño</td>
				            		<td class="text-right">Bs 265.32</td>
				            	</tr>
				            	<tr class="subTotal">
				            		<td class="text-right">Sub-Total</td>
				            		<th class="text-right">Bs 4161,28</th>
				            	</tr>
				            	<tr>
				            		<td class="text-right">I.V.A</td>
				            		<th class="text-right">Bs 499,35</th>
				            	</tr>
				            	<tr class="total">
				            		<td class="text-right">Total</td>
				            		<th class="text-right">Bs 158.265</th>
				            	</tr>
				            </table>
			    		</div>
			          <br>
			          <button data-remodal-action="confirm" class="btn btn-success">Cerrar</button>
			        </div>
			        <!-- /Remodal -->


	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>

   <script>
      $(document).ready(function() {
        $('#fecha').datepicker({
		    format: "dd/mm/yyyy",
		    language: "es",
		    autoclose: true,
		    todayHighlight: true,
		});



      });

    </script>
    <!-- /bootstrap-daterangepicker -->