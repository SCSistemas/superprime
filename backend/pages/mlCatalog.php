<?php
	include '../config/definitions.php';
	//echo('<pre>');
	//print_r('rutaFotos:' . $rutaFotos);
	//print_r($_SERVER);
	$codPro = $_GET['codPro'];

	$urlRevision = $urlWS . 'service=productoservices&metodo=consultarPublicacionProducto&p_codProducto=' . $codPro;
	//echo($urlRevision);
	//$respRevision = file_get_contents($urlRevision);
	$respRevision = consumoServicioGet($urlRevision);
	//print_r($respRevision);

	$disabled = 0;

	//$respRevision = json_decode($respRevision, true);
	foreach ($respRevision as $value) {
		//print_r($value);
		if(isset($value['codProducto'])){
			$success = 1;
			$catSelect = $value['categorias'];
			$plan = $value['plan'];
			if($value['idPubML'] != null){
				$disabled = 1;
			}
		}else{
			$success = -1;
			$plan = -1;
			$catSelect = null;
			$hoja = 0;
			$disabled = 0;
		}
		$publicarML = $value['PublicadoMl'];
	}
	//print_r('success: ' . $success . ', categoria: ' . $catSelect . ', plan: ' . $plan . ', disabled: ' . $disabled);
	//$subCategorias = explode(";", $respRevision['categorias']); 

	$urlPlanes = 'https://api.mercadolibre.com/sites/MLV/listing_types';
	//echo('</br>'.$urlPlanes);
	//$respPlanes = file_get_contents($urlPlanes);
	//$respPlanes = json_decode($respPlanes, true);
	$respPlanes = consumoServicioGet($urlPlanes);
	//echo($respPlanes);

	$urlCats = 'https://api.mercadolibre.com/sites/MLV/categories';
	//$respCats = file_get_contents($urlCats);
	//$respCats = json_decode($respCats, true);
	$respCats = consumoServicioGet($urlCats);
	//ini_set('display_errors', '1');

	//if($respRevision['success'] == 1){
	if($success == 1){

		//$catBD = explode(';', $respRevision['categorias']);
		$catBD = explode(';', $catSelect);
		$hoja = 1;

		if($disabled == 1){
			$deshabilitar = "disabled";
		}else{
			$deshabilitar = "";
		}
		
		//print_r('disabled: ' . $disabled . ', deshabilitar:' . $deshabilitar);

		$divCategorias  = '<div id="div_cs1" class="col-lg-4">';
		$divCategorias .= ' <select id="cs1" class="form-control" name="category" size="5" onchange="buscarValor(1)" ' . $deshabilitar . '>';
	    foreach ($respCats as $valor){
	    	if($catBD[0] == $valor['id']){
	    		$divCategorias .= ' 	<option value="' . $valor['id'] . '" selected>' . $valor['name'] . '</option>';
	    	}else{
	    		$divCategorias .= ' 	<option value="' . $valor['id'] . '">' . $valor['name'] . '</option>';
	    	}
	    }
	    $divCategorias .= ' </select>';
		$divCategorias .= '</div>';

		for ($i = 1; $i < count($catBD); $i++) {
			$urlcategorias = 'https://api.mercadolibre.com/categories/' . $catBD[($i-1)];
			//$respCategorias = file_get_contents($urlcategorias);
			//$respCategorias = json_decode($respCategorias, true);
			$respCategorias = consumoServicioGet($urlcategorias);

			$divCategorias .= '<div id="div_cs' . ($i+1) . '" class="col-lg-4" style="float:left">';
			$divCategorias .= ' <select id="cs' . ($i+1) . '" class="form-control" name="category" size="5" onchange="buscarValor(' . ($i+1) . ')" ' . $deshabilitar . '>';

			foreach ($respCategorias['children_categories'] as $valor) {
				if($catBD[$i] == $valor['id']){
		    		$divCategorias .= ' 	<option value="' . $valor['id'] . '" selected>' . $valor['name'] . '</option>';
		    	}else{
		    		$divCategorias .= ' 	<option value="' . $valor['id'] . '">' . $valor['name'] . '</option>';
		    	}
			}
			$divCategorias .= ' </select>';
			$divCategorias .= '</div>';
		}
	}else{
		$hoja = 0;
		$divCategorias  = '<div id="div_cs1" class="col-lg-4" style="float:left">';
		$divCategorias .= ' <select id="cs1" class="form-control" name="category" size="5" onchange="buscarValor(1)">';
	    foreach ($respCats as $valor){
	      $divCategorias .= ' 	<option value="' . $valor['id'] . '">' . $valor['name'] . '</option>';
	    }
	    $divCategorias .= ' </select>';
		$divCategorias .= '</div>';
	}

	
	//print_r($respRevision);
	//echo('</pre>');

 	include '../views/mlCatalog.php';
?>

  