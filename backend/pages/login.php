<?php

include '../config/definitions.php';
session_start();
unset($_SESSION['userid']);

if ((isset($_POST['email'])) && isset($_POST['password'])){
    $data = array();
    $data["p_usuario"] = $_POST['email'];
    $data["p_password"] = $_POST['password'];

    $ch = curl_init();
    $url = $urlWS.'service=userservices&metodo=LoginBackend';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    $resultData = curl_exec($ch);
    $user = json_decode($resultData, true);

    if ($user['success'] == 1){
        $_SESSION['userid'] = $user['IdUser'];
    }
    curl_close($ch);

}

include '../views/login.php';
?>

  