<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Crear nueva Categoría</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form id="newCategory">
                      <div class="form-inline">
                        <div class="form-group">
                          <label for="categoryName">Nombre</label>
                          <input type="text" class="form-control" id="categoryName">
                        </div>
                        <div class="form-group">
                          <label for="onHomePage">Página principal</label>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="onHomePage">Destacado</label>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_desde" name="destacado_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_hasta" name="destacado_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="discount">Descuento (%)</label>
                            <input type="text" class="form-control numeric" id="discount" style="width:70px">                     
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_desde" name="descuento_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_hasta" name="descuento_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>

                    </form>



                  </div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

    <script>
      $(document).ready(function() {





        $('#destacado_desde, #destacado_hasta, #descuento_desde, #descuento_hasta').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            todayHighlight: true
        });






      });

    </script>
  </body>
</html>