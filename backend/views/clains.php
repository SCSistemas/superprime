<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Administrar Usuarios Registrados</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-inline">
                        <div class="form-group">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="desde" name="desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="hasta" name="hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                             <input type="text" class="form-control" id="search" placeholder="Introduce un término">
                          </div>
                        </div>
                        <div class="form-group" style="    margin-top: -10px;">
                              <label for="" style="    padding-top: 0px;">Tipo de Reclamo</label>
                              <select id="tipo-reclamo" name="" class="form-control">
                                <option value="">Cambio</option>
                                <option value="">Retrazo en entrega</option>
                                <option>Sin confirmar existencia</option>
                              </select>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                             <button type="submit" class="btn btn-primary">Buscar</button>
                          </div>
                        </div>
                      </div>

                   
                      <!-- Tabla de casos -->

                      <table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>Nombre</th> 
                              <th>Correo</th>
                              <th>Fecha</th> 
                              <th>Estatus</th>
                              <th>Reclamo</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>Marbelys Alamo</td> 
                              <td>marbelys@gmail.com</td>
                              <td>15-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#details">En proceso</a>
                              </td> 
                              <td>
                                Retrazo en entrega
                              </td>
                            </tr>
                            <tr>
                              <td>Daniel Yanez</td> 
                              <td>d.yanez@gmail.com</td>
                              <td>12-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-success" data-toggle="modal" data-target="#details">Nuevo</a>
                              </td> 
                              <td>
                                Cambio
                              </td>
                            </tr>
                            <tr>
                              <td>Imelda Rivas</td> 
                              <td>irivas@gmail.com</td>
                              <td>25-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-info" data-toggle="modal" data-target="#details">Por confirmación del cliente</a>
                              </td> 
                              <td>
                                Sin confirmar existencia
                              </td>
                            </tr>
                            <tr>
                              <td>Imelda Rivas</td> 
                              <td>irivas@gmail.com</td>
                              <td>25-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#details">Finalizado</a>
                              </td> 
                              <td>
                                Cambio
                              </td>
                            </tr>
                            <tr>
                              <td>Karla Rivas</td> 
                              <td>irivas@gmail.com</td>
                              <td>25-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#details">Aceptado</a>
                              </td> 
                              <td>
                                Cambio
                              </td>
                            </tr>
                            <tr>
                              <td>Imelda Rivas</td> 
                              <td>irivas@gmail.com</td>
                              <td>25-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#details">Negado</a>
                              </td> 
                              <td>
                                Sin confirmar existencia
                              </td  


                        </tbody>
                    </table>

                    <!-- //Tabla de casos -->


                      
                    <!-- Modal para atender caso-->

                      <div class="modal fade bs-example-modal-lg" id="details"  tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Editar usuario</h4>
                            </div>
                            <div class="modal-body">
                          <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <tbody>
                               <tr>
                              <td>Imelda Rivas</td> 
                              <td>irivas@gmail.com</td>
                              <td>25-03-2016 08-50</td>
                              <td>
                                <a href="" class="btn btn-xs btn-info" data-toggle="modal" data-target="#details">Por confirmación del cliente</a>
                              </td> 
                              <td>
                                Sin confirmar existencia
                              </td>
                            </tr>
                            </tbody>
                          </table>

                          <form class="form-horizontal">
                            <div class="form-group">
                              <textarea class="form-control" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword" class="col-sm-2 control-label" style="width: 13.666667%;">Cambiar estatus</label>
                              <div class="col-sm-4">
                                <select class="form-control">
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                </select>
                              </div>
                            </div>
                          </form>

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->



                  </div>
                </div>
              </div>
            <!-- /Opcion  -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>
    <script src="../js/remodal/remodal.js"></script>

      <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/datatables.scroller.min.js"></script>


    <script>
      $(document).ready(function() {


        $('#desde, #hasta').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            todayHighlight: true
        });



         $('#users').DataTable({
          "bFilter": false,
          "sDom": 'Rfrtlip'
         });


      });

    </script>  
  </body>
</html>