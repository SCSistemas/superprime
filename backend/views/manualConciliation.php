<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

            <!-- Opcion Sincronizar -->
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Proceso de conciliación manual</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <form class="form-horizontal">

                      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="conciliations" width="100%">
                        <thead>
                          <tr>
                            <th>Fecha</th>
                            <th>Concepto</th>
                            <th>Referencia</th>
                            <th>Cargo</th>
                            <th>Conciliar</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>20/03/2016</td>
                            <td>Trans otros bancos</td>
                            <td>2986</td>
                            <td>30.256 Bs</td>
                            <td class="text-center"><a href="" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Conciliar</a></td>
                          </tr>
                          <tr class="highlight">
                            <td>15/03/2016</td>
                            <td>Cheque</td>
                            <td>98563</td>
                            <td>15.369 Bs</td>
                            <td class="text-center"><a href="" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Conciliar</a></td>
                          </tr>
                          <tr>
                            <td>10/03/2016</td>
                            <td>Transf mismo bancos</td>
                            <td>8596</td>
                            <td>13.302 Bs</td>
                            <td class="text-center"><a href="" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Conciliar</a></td>
                          </tr>
                          <tr>
                            <td>05/03/2016</td>
                            <td>Master XX02</td>
                            <td>78596</td>
                            <td>150.258 Bs</td>
                            <td class="text-center"><a href="" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Conciliar</a></td>
                          </tr>
                        </tbody>
                      </table>

                      <div class="clearfix"></div>

                      <!-- Notificación de exito -->
                      <div class="alert alert-info alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        La compra se ha realizado con éxito
                      </div>
                      <!-- Notificación de exito -->

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-success">Aceptar</button>
                        </div>
                      </div>


                      <!-- Modal -->
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Referencia para conciliación</h4>
                            </div>
                            <div class="modal-body">
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="numberReference">Número de referencia</label>  
                                <div class="col-md-4">
                                <input id="numberReference" name="numberReference" type="text" placeholder="" class="form-control input-md">
                                  
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                              <button type="button" class="btn btn-primary">Aceptar</button>
                            </div>
                          </div>
                        </div>
                      </div>

                    </form>

                  </div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

<script>
  $(document).ready(function() {

    // Date range
    $('.input-daterange').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
    todayHighlight: true
    });

    // Inicializar tabla de conciliaciones
    $('#conciliations').DataTable({
      "bFilter": false,
      "sDom": 'Rfrtlip',
      "language":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    });

});
</script>

  </body>
</html>