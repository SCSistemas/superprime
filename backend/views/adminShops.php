<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

          <div class="col-lg-4">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Razones Sociales</h2>
                  <div class="clearfix"></div>
               </div> 

               <div class="x_content">
                  <ul id="razonS" class="listSelection">
                    <li><a href="#">Prime Shoes</a></li>
                    <li><a href="#">Desigual Centro</a></li>
                    <li><a href="#">Desigual Occidente</a></li>
                    <li><a href="#">Super Prime</a></li>
                  </ul>
               </div>   
            </div>
          </div>

            <div class="col-lg-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ciudades</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul id="ciudades" class="listSelection">
                      <li><a href="#">Distrito Capital</a>
                          <ul>
                            <li><a href="#">Caracas</a></li>
                          </ul>
                      </li>
                      <li><a href="#">Miranda</a></li>
                      <li><a href="#">Aragua</a></li>
                      <li><a href="#">Carabobo</a></li>
                    </ul>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tiendas</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <!-- item -->
                      <div class="row itemShop">
                        <div class="col-lg-2">
                          <a href="#"  data-toggle="modal" data-target="#editShop">001</a>
                        </div>
                        <div class="col-lg-10">
                            <label>
                              
                              Página web <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                            <label>
                              
                              Mercado Libre <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                        </div>  
                      </div>
                      <!-- item -->

                      <!-- item -->
                      <div class="row itemShop">
                        <div class="col-lg-2">
                          <a href="#"  data-toggle="modal" data-target="#editShop">002</a>
                        </div>
                        <div class="col-lg-10">
                            <label>
                              
                              Página web <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                            <label>
                              
                              Mercado Libre <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                        </div>  
                      </div>
                      <!-- item -->

                      <!-- item -->
                      <div class="row itemShop">
                        <div class="col-lg-2">
                          <a href="#"  data-toggle="modal" data-target="#editShop">003</a>
                        </div>
                        <div class="col-lg-10">
                            <label>
                              
                              Página web <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                            <label>
                              
                              Mercado Libre <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                        </div>  
                      </div>
                      <!-- item -->

                      <!-- item -->
                      <div class="row itemShop">
                        <div class="col-lg-2">
                          <a href="#"  data-toggle="modal" data-target="#editShop">004</a>
                        </div>
                        <div class="col-lg-10">
                            <label>
                              
                              Página web <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                            <label>
                              
                              Mercado Libre <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                        </div>  
                      </div>
                      <!-- item -->

                      <!-- item -->
                      <div class="row itemShop">
                        <div class="col-lg-2">
                          <a href="#"  data-toggle="modal" data-target="#editShop">005</a>
                        </div>
                        <div class="col-lg-10 ">
                            <label>
                              
                              Página web <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                            <label>
                              
                              Mercado Libre <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </label>
                        </div>  
                      </div>
                      <!-- item -->

                      <div class="text-center">
                      <br>
                        <a href="" class="btn btn-success" data-toggle="modal" data-target="#myModal">Agregar Nueva</a>
                      </div>


                        <!-- Modal para agregar un nueva tienda -->
                        <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                          <div  class="modal-dialog modal-lg" role="document">
                            <form class="newShop" data-toggle="validator" role="form">

                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Nueva Tienda</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-4 col-md-offset-2">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="code">Código</label>
                                          <input type="text" class="form-control numeric" id="code"  required>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="nombreTienda">Nombre</label>
                                          <input type="text" class="form-control" id="nombreTienda" required>
                                        </div>
                                      </div>
                                    </div> 
                                    <div class="col-lg-6">

                                      <h1>Dirección</h1>

                                      <div class="form-horizontal">
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="estado">Estado</label>
                                          <div class="col-md-8">
                                            <select id="estado" name="estado" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="ciudad">Ciudad</label>
                                          <div class="col-md-8">
                                            <select id="ciudad" name="ciudad" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="municipio">Municipio</label>
                                          <div class="col-md-8">
                                            <select id="municipio" name="municipio" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="parroquia">Parroquia</label>
                                          <div class="col-md-8">
                                            <select id="parroquia" name="parroquia" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="postal">Código Postal</label>
                                          <div class="col-md-8">
                                            <input id="telefono1" name="telefono1" type="text" class="form-control input-md" required>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <h1>Contácto</h1>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="telefono1" style="padding: 7px 2px;">Teléfono 1</label>  
                                              <div class="col-md-8">
                                              <input id="telefono1" name="telefono1" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="telefono2" style="padding: 7px 2px;">Teléfono 2</label>  
                                              <div class="col-md-8">
                                              <input id="telefono2" name="telefono2" type="text" class="form-control input-md">
                                                
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-lg-6">
                                            <h1>Encargado</h1>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="nombre">Nombre</label>  
                                              <div class="col-md-8">
                                              <input id="nombre" name="nombre" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="apellido">Apellido</label>  
                                              <div class="col-md-8">
                                              <input id="apellido" name="apellido" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-lg-6">

                                      <div class="form-horizontal">
                                      <br><br>
                                         <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="calle1">Calle 1</label>  
                                        <div class="col-md-8">
                                        <textarea class="form-control" id="calle1" name="calle1" required></textarea>
                                          
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="calle2">Calle 2</label>  
                                        <div class="col-md-8">
                                        <input id="calle2" name="calle2" type="text" placeholder="Opcional" class="form-control input-md">
                                          
                                        </div>
                                      </div>

                                      <!-- Select Basic -->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="tipo">Tipo de Edificación</label>
                                        <div class="col-md-8">
                                          <select id="tipo" name="tipo" class="form-control" required>
                                              <option value=""></option>
                                            <option value="1">Edificio</option>
                                            <option value="2">Casa</option>
                                          </select>
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="nombreCasa">Nombre o Número</label>  
                                        <div class="col-md-8">
                                        <input id="nombreCasa" name="nombreCasa" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-lg-6">
                                          <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-8 control-label" for="piso" style="    padding-right: 4px;">Piso</label>  
                                        <div class="col-md-4" style="    padding-left: 17px;">
                                        <input id="piso" name="piso" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                        </div>
                                        <div class="col-lg-6">
                                          <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-5 control-label" for="local">Local Nro</label>  
                                        <div class="col-md-5">
                                        <input id="local" name="local" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Email</label>  
                                        <div class="col-md-8">
                                        <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="celular">Celular</label>  
                                        <div class="col-md-8">
                                        <input id="celular" name="celular" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>
                                      </div>
                                      
                                    </div>  
                                  </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>

                              </form>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->





                        <!-- Modal para editar una tienda -->
                        <div class="modal fade" id="editShop"  tabindex="-1" role="dialog">
                          <div  class="modal-dialog modal-lg" role="document">
                            <form class="newShop" data-toggle="validator" role="form">

                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Editar Tienda</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-4 col-md-offset-2">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="code">Código</label>
                                          <input type="text" class="form-control numeric" id="code"  required disabled>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="nombreTienda">Nombre</label>
                                          <input type="text" class="form-control" id="nombreTienda" required>
                                        </div>
                                      </div>
                                    </div> 
                                    <div class="col-lg-6">

                                      <h1>Dirección</h1>

                                      <div class="form-horizontal">
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="estado">Estado</label>
                                          <div class="col-md-8">
                                            <select id="estado" name="estado" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="ciudad">Ciudad</label>
                                          <div class="col-md-8">
                                            <select id="ciudad" name="ciudad" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="municipio">Municipio</label>
                                          <div class="col-md-8">
                                            <select id="municipio" name="municipio" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="parroquia">Parroquia</label>
                                          <div class="col-md-8">
                                            <select id="parroquia" name="parroquia" class="form-control" required>
                                              <option value=""></option>
                                              <option value="1">Option one</option>
                                              <option value="2">Option two</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="postal">Código Postal</label>
                                          <div class="col-md-8">
                                            <input id="telefono1" name="telefono1" type="text" class="form-control input-md" required>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <h1>Contácto</h1>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="telefono1" style="padding: 7px 2px;">Teléfono 1</label>  
                                              <div class="col-md-8">
                                              <input id="telefono1" name="telefono1" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="telefono2" style="padding: 7px 2px;">Teléfono 2</label>  
                                              <div class="col-md-8">
                                              <input id="telefono2" name="telefono2" type="text" class="form-control input-md">
                                                
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-lg-6">
                                            <h1>Encargado</h1>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="nombre">Nombre</label>  
                                              <div class="col-md-8">
                                              <input id="nombre" name="nombre" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="apellido">Apellido</label>  
                                              <div class="col-md-8">
                                              <input id="apellido" name="apellido" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-lg-6">

                                      <div class="form-horizontal">
                                      <br><br>
                                         <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="calle1">Calle 1</label>  
                                        <div class="col-md-8">
                                        <textarea class="form-control" id="calle1" name="calle1" required></textarea>
                                          
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="calle2">Calle 2</label>  
                                        <div class="col-md-8">
                                        <input id="calle2" name="calle2" type="text" placeholder="Opcional" class="form-control input-md">
                                          
                                        </div>
                                      </div>

                                      <!-- Select Basic -->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="tipo">Tipo de Edificación</label>
                                        <div class="col-md-8">
                                          <select id="tipo" name="tipo" class="form-control" required>
                                              <option value=""></option>
                                            <option value="1">Edificio</option>
                                            <option value="2">Casa</option>
                                          </select>
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="nombreCasa">Nombre o Número</label>  
                                        <div class="col-md-8">
                                        <input id="nombreCasa" name="nombreCasa" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-lg-6">
                                          <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-8 control-label" for="piso" style="    padding-right: 4px;">Piso</label>  
                                        <div class="col-md-4" style="    padding-left: 17px;">
                                        <input id="piso" name="piso" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                        </div>
                                        <div class="col-lg-6">
                                          <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-5 control-label" for="local">Local Nro</label>  
                                        <div class="col-md-5">
                                        <input id="local" name="local" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Email</label>  
                                        <div class="col-md-8">
                                        <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="celular">Celular</label>  
                                        <div class="col-md-8">
                                        <input id="celular" name="celular" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>
                                      </div>
                                      
                                    </div>  
                                  </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>

                              </form>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                  </div>
                </div>
              </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

<script src="../js/switchery/switchery.min.js"></script>



    <script>
      $(document).ready(function() {

        // Mostrar clic en lista de razon social
        $("#razonS").on('click','li a',function(){
            // remove classname 'active' from all li who already has classname 'active'
            $("#razonS li a.active").removeClass("active"); 
            // adding classname 'active' to current click li 
            $(this).addClass("active"); 
        });

        // Mostrar clic en lista de ciudades
        $("#ciudades").on('click','li a',function(){
            // remove classname 'active' from all li who already has classname 'active'
            $("#ciudades li a.active").removeClass("active"); 
            // adding classname 'active' to current click li 
            $(this).addClass("active"); 
        });

        // Aplicar odd y even en la lista de tiendas
        $ ('.itemShop:even').addClass ('even');
        $ ('.itemShop:odd').addClass ('odd');

        $('.modal').on('shown.bs.modal', function () {
            $(this).find('form').validator('destroy').validator()
        });

        $(".numeric").numeric()

        $('#celular').mask('(0000) 0000000');

      });

    </script>
  </body>
</html>