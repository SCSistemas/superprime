<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

          <div class="col-lg-4">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Arbol de Categorías</h2>
                  <div class="clearfix"></div>
               </div> 

               <div class="x_content">
                 <div id="container">

                    <ol id='auto-checkboxes' data-name='foo'>
                      <li class='expanded' data-value='0'>Todas
                        <ol>
                          <li data-value='1'>Hogar
                          <ol>
                              <li data-name='baz' data-value='3'>Articulos Eléctricos</li>
                              <li data-value='5'>Vajillas</li>
                          </ol>
                          </li>
                          <li data-value='2' data-id='2'>Caballeros
                            <ol>
                              <li data-name='baz' data-value='3'>Zapatos</li>
                          </ol>
                          </li>
                          <li data-value='2' data-id='2'>Damas
                            <ol>
                              <li data-name='baz' data-value='3'>Textil elegante</li>
                              <li data-name='baz' data-value='3'>Textil deportivo</li>
                              <li data-name='baz' data-value='3'>Zapatos</li>

                          </ol>
                          </li>
                        </ol>
                      </li>
                    </ol>
                  </div>

               </div>   


            </div>
            
          </div>
            <!-- Opcion Sincronizar -->
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-8">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Crear nueva SubCategoría</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form id="newCategory">
                      <div class="form-inline">
                        <div class="form-group">
                          <label for="categoryName">Nombre</label>
                          <input type="text" class="form-control" id="categoryName">
                        </div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="onHomePage">Destacado</label>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_desde" name="destacado_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_hasta" name="destacado_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="discount">Descuento (%)</label>
                            <input type="text" class="form-control numeric" id="discount" style="width:70px">                     
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_desde" name="descuento_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_hasta" name="descuento_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>
                      
                      <br>
                      <br>
                      <br>  
                      <table class="table table-bordered"> 
                        <thead> 
                          <tr> 
                            <th>Nombre del Filtro</th> 
                            <th>Tipo de Campo</th> 
                            <th></th> 
                          </tr> 
                        </thead> 
                        <tbody> 
                          <tr> 
                            <td>Filtro 1</td> 
                            <td>Radio Buttons</td> 
                            <td><a href="" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a><a href="" class="btn  btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a><a href="" class="btn  btn-xs btn-info"><span class="glyphicon glyphicon-info-sign"></span></a></td> 
                          </tr> 
                          <tr> 
                            <td>Filtro 2</td> 
                            <td>Lista de selección</td> 
                            <td><a href="" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a><a href="" class="btn  btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a><a href="" class="btn  btn-xs btn-info"><span class="glyphicon glyphicon-info-sign"></span></a></td> 
                          </tr> 
                          <tr> 
                            <td>Filtro 3</td> 
                            <td>Check</td> 
                            <td><a href="" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a><a href="" class="btn  btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a><a href="" class="btn  btn-xs btn-info"><span class="glyphicon glyphicon-info-sign"></span></a></td> 
                          </tr> 
                        </tbody> 
                      </table>

                      <div class="col-md-12 text-center">
                          <a href="" class="btn btn-success" data-toggle="modal" data-target="#myModal">Agregar Nueva</a>
                        </div>

                    </form>



                    <!-- Modal para agregar un nuevo filtro a la tabla -->
                    <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Nuevo filtro</h4>
                          </div>
                          <div class="modal-body">
                            <form>
                              <div class="form-group">
                                <label for="nameFilter">Nombre del Filtro</label>
                                <input type="text" class="form-control" id="nameFilter" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="typeElement">Tipo de elemento</label>
                                <select class="form-control">
                                  <option>Radio</option>
                                  <option>Checkbox</option>
                                  <option>Lista de opciones</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="numberOptions">Número de opciones</label>
                                <select class="form-control">
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="option1">Opción 1</label>
                                <input type="text" class="form-control" id="option1" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="option2">Opción 2</label>
                                <input type="text" class="form-control" id="option2" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="option3">Opción 3</label>
                                <input type="text" class="form-control" id="option3" placeholder="">
                              </div>


                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Guardar</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->



                  </div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

    <!-- Proton tree checkboxes -->
    <script src="../js/bonsai/jquery.bonsai.js"></script>
    <script src="../js/bonsai/jquery.qubit.js"></script>
    <script>
      $(document).ready(function() {


        $('#destacado_desde, #destacado_hasta, #descuento_desde, #descuento_hasta').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            todayHighlight: true
        });



        //Inicializar arbol de categorias
        $('#auto-checkboxes').bonsai({
          expandAll: true,
          checkboxes: true, // depends on jquery.qubit plugin
          createInputs: 'checkbox' // takes values from data-name and data-value, and data-name is inherited
        });





      });

    </script>
  </body>
</html>