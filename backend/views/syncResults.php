<?php include '../includes/header.php';?>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

              <!-- Resultados de Sincronizacion -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Resultados de la sincronización</h2>
                    <ul class="navbar-right">
                      <li><a href="../pages/sync.php" class="btn btn-info btn-sm">Volver a sincronización</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">Se han sincronizado los siguientes productos satisfactoriamente</p>

                      <table id="datatable" class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>Código</th>
                          <th>Descripción SAP</th>
                          <th>Precio</th>
                        </tr>
                      </thead>


                      <tbody>
                        <tr>
                          <td>457646878</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                        </tr>
                        <tr>
                          <td>45745745</td>
                          <td>Accountant</td>
                          <td>Tokyo</td>
                        </tr>
                        <tr>
                          <td>435745457</td>
                          <td>Junior Technical Author</td>
                          <td>$86,000</td>
                        </tr>
                        <tr>
                          <td>4357457745</td>
                          <td>Senior Javascript Developer</td>
                          <td>$433,060</td>
                        </tr>
                        <tr>
                          <td>45754747</td>
                          <td>Accountant</td>
                          <td>$162,700</td>
                        </tr>
                        <tr>
                          <td>457457457</td>
                          <td>Integration Specialist</td>
                          <td>$372,000</td>
                        </tr>
                        <tr>
                          <td>4747457</td>
                          <td>Sales Assistant</td>
                          <td>$137,500</td>
                        </tr>
                        <tr>
                          <td>475457457</td>
                          <td>Integration Specialist</td>
                          <td>$327,900</td>
                        </tr>
                        <tr>
                          <td>4374575474</td>
                          <td>Javascript Developer</td>
                          <td>$205,500</td>
                        </tr>
                        <tr>
                          <td>3474574574</td>
                          <td>Software Engineer</td>
                          <td>$103,600</td>
                        </tr>
                        <tr>
                          <td>4357457457</td>
                          <td>Office Manager</td>
                          <td>$90,560</td>
                        </tr>
                        <tr>
                          <td>437457457</td>
                          <td>Support Lead</td>
                          <td>$342,000</td>
                        </tr>
                        <tr>
                          <td>4745745745</td>
                          <td>Regional Director</td>
                          <td>$470,600</td>
                        </tr>
                        <tr>
                          <td>47457457</td>
                          <td>Senior Marketing Designer</td>
                          <td>$313,500</td>
                        </tr>
                        <tr>
                          <td>47457457457</td>
                          <td>Regional Director</td>
                          <td>$385,750</td>
                        </tr>
                        <tr>
                          <td>437457457</td>
                          <td>Marketing Designer</td>
                          <td>$198,500</td>
                        </tr>
                        <tr>
                          <td>435754745</td>
                          <td>Chief Financial Officer (CFO)</td>
                          <td>$725,000</td>
                        </tr>
                        <tr>
                          <td>6787687</td>
                          <td>Systems Administrator</td>
                          <td>$237,500</td>
                        </tr>
                        <tr>
                          <td>1234144</td>
                          <td>Software Engineer</td>
                          <td>$132,000</td>
                        </tr>
                        <tr>
                          <td>5638543</td>
                          <td>Personnel Lead</td>
                          <td>$217,500</td>
                        </tr>
                        <tr>
                          <td>3568568658</td>
                          <td>Development Lead</td>
                          <td>$345,000</td>
                        </tr>
                        <tr>
                          <td>8568435678</td>
                          <td>Chief Marketing Officer (CMO)</td>
                          <td>$675,000</td>
                        </tr>
                        <tr>
                          <td>65344563</td>
                          <td>Pre-Sales Support</td>
                          <td>$106,450</td>
                        </tr>
                        <tr>
                          <td>363345645465</td>
                          <td>Sales Assistant</td>
                          <td>$85,600</td>
                        </tr>
                        <tr>
                          <td>Angelica Ramos</td>
                          <td>Chief Executive Officer (CEO)</td>
                          <td>$1,200,000</td>
                        </tr>
                        <tr>
                          <td>Gavin Joyce</td>
                          <td>Developer</td>
                          <td>$92,575</td>
                        </tr>
                        <tr>
                          <td>Jennifer Chang</td>
                          <td>Regional Director</td>
                          <td>$357,650</td>
                        </tr>
                        <tr>
                          <td>Brenden Wagner</td>
                          <td>Software Engineer</td>
                          <td>$206,850</td>
                        </tr>
                        <tr>
                          <td>Fiona Green</td>
                          <td>Chief Operating Officer (COO)</td>
                          <td>$850,000</td>
                        </tr>
                        <tr>
                          <td>Shou Itou</td>
                          <td>Regional Marketing</td>
                          <td>$163,000</td>
                        </tr>
                        <tr>
                          <td>Michelle House</td>
                          <td>Integration Specialist</td>
                          <td>$95,400</td>
                        </tr>
                        <tr>
                          <td>Suki Burks</td>
                          <td>Developer</td>
                          <td>$114,500</td>
                        </tr>
                        <tr>
                          <td>Prescott Bartlett</td>
                          <td>Technical Author</td>
                          <td>$145,000</td>
                        </tr>
                        <tr>
                          <td>Gavin Cortez</td>
                          <td>Team Leader</td>
                          <td>$235,500</td>
                        </tr>
                        <tr>
                          <td>Martena Mccray</td>
                          <td>Post-Sales support</td>
                          <td>$324,050</td>
                        </tr>
                        <tr>
                          <td>Unity Butler</td>
                          <td>Marketing Designer</td>
                          <td>$85,675</td>
                        </tr>
                        <tr>
                          <td>Howard Hatfield</td>
                          <td>Office Manager</td>
                          <td>$164,500</td>
                        </tr>
                        <tr>
                          <td>Hope Fuentes</td>
                          <td>Secretary</td>
                          <td>$109,850</td>
                        </tr>
                        <tr>
                          <td>Vivian Harrell</td>
                          <td>Financial Controller</td>
                          <td>$452,500</td>
                        </tr>
                        <tr>
                          <td>Timothy Mooney</td>
                          <td>Office Manager</td>
                          <td>$136,200</td>
                        </tr>
                        <tr>
                          <td>Jackson Bradshaw</td>
                          <td>Director</td>
                          <td>$645,750</td>
                        </tr>
                        <tr>
                          <td>Olivia Liang</td>
                          <td>Support Engineer</td>
                          <td>$234,500</td>
                        </tr>
                        <tr>
                          <td>Bruno Nash</td>
                          <td>Software Engineer</td>
                          <td>$163,500</td>
                        </tr>
                        <tr>
                          <td>Sakura Yamamoto</td>
                          <td>Support Engineer</td>
                          <td>$139,575</td>
                        </tr>
                        <tr>
                          <td>Thor Walton</td>
                          <td>Developer</td>
                          <td>$98,540</td>
                        </tr>
                        <tr>
                          <td>Finn Camacho</td>
                          <td>Support Engineer</td>
                          <td>$87,500</td>
                        </tr>
                        <tr>
                          <td>Serge Baldwin</td>
                          <td>Data Coordinator</td>
                          <td>$138,575</td>
                        </tr>
                        <tr>
                          <td>Zenaida Frank</td>
                          <td>Software Engineer</td>
                          <td>$125,250</td>
                        </tr>
                        <tr>
                          <td>Zorita Serrano</td>
                          <td>Software Engineer</td>
                          <td>$115,000</td>
                        </tr>
                        <tr>
                          <td>Jennifer Acosta</td>
                          <td>Junior Javascript Developer</td>
                          <td>$75,650</td>
                        </tr>
                        <tr>
                          <td>Cara Stevens</td>
                          <td>Sales Assistant</td>
                          <td>$145,600</td>
                        </tr>
                        <tr>
                          <td>Hermione Butler</td>
                          <td>Regional Director</td>
                          <td>$356,250</td>
                        </tr>
                        <tr>
                          <td>Lael Greer</td>
                          <td>Systems Administrator</td>
                          <td>$103,500</td>
                        </tr>
                        <tr>
                          <td>Jonas Alexander</td>
                          <td>Developer</td>
                          <td>$86,500</td>
                        </tr>
                        <tr>
                          <td>Shad Decker</td>
                          <td>Regional Director</td>
                          <td>$183,000</td>
                        </tr>
                        <tr>
                          <td>Michael Bruce</td>
                          <td>Javascript Developer</td>
                          <td>$183,000</td>
                        </tr>
                        <tr>
                          <td>Donna Snider</td>
                          <td>Customer Support</td>
                          <td>$112,000</td>
                        </tr>
                      </tbody>
                    </table>
                    <div class="text-center">
                      <a href="catalogAdmin.php" class="btn btn-primary">Aceptar</a>
                    </div>
                    </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Resultados de Sincronizacion -->

          </div>

     

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>

    <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../js/jszip/dist/jszip.min.js"></script>
    <script src="../js/pdfmake/build/pdfmake.min.js"></script>
    <script src="../js/pdfmake/build/vfs_fonts.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {

        

        $('#datatable').dataTable( {
            "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
        });


      });
    </script>

  </body>
</html>