<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Administrar Usuarios Administradores</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="form-inline">
                        <div class="form-group">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="desde" name="desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="hasta" name="hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                             <input type="text" class="form-control" id="search" placeholder="Introduce un término">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                             <button type="submit" class="btn btn-primary">Buscar</button>
                          </div>
                        </div>
                      </div>

                   

                      <table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>Nombre</th> 
                              <th>Correo</th>
                              <th>Teléfono</th> 
                              <th>Nivel de acceso</th>
                              <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>Marbelys Alamo</td> 
                              <td>marbelys@gmail.com</td>
                              <td>0412 3626585</td>
                              <td>Administrador</td>
                              <td class="text-center">
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a>
                              <a href="" class="btn btn-xs btn-danger" data-remodal-target="deleteUser" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr>
                            <tr>
                              <td>Daniel Yanez</td> 
                              <td>d.yanez@gmail.com</td>
                              <td>0426 6548975</td>
                              <td>Catálogo</td>
                              <td class="text-center">
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a>
                              <a href="" class="btn btn-xs btn-danger" data-remodal-target="deleteUser" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr>
                            <tr>
                              <td>Imelda Rivas</td> 
                              <td>irivas@gmail.com</td>
                              <td>0416 5689878</td>
                              <td>Conciliaciones</td> 
                              <td class="text-center">
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a>
                              <a href="" class="btn btn-xs btn-danger" data-remodal-target="deleteUser" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr> 


                        </tbody>
                    </table>
                    
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <a href="" class="btn btn-success" data-toggle="modal" data-target="#addUser">Agregar Usuario</a>
                        </div>
                      </div>

                    <!-- //Tabla de usuarios -->


                      

                      <!-- Remodal -->
                      <div class="remodal" data-remodal-id="blockUser">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Confirme</h1>
                        <p>​¿Desea bloquear al usuario?</p>
                        <br>
                        <button data-remodal-action="confirm" class="btn btn-success">Si</button>
                        <button data-remodal-action="cancel" class="btn btn-danger">No</button>
                      </div>
                      <!-- /Remodal  -->

                      <!-- Remodal -->
                      <div class="remodal" data-remodal-id="deleteUser">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Confirme</h1>
                        <p>​¿Desea eliminar al usuario?</p>
                        <br>
                        <button data-remodal-action="confirm" class="btn btn-success">Si</button>
                        <button data-remodal-action="cancel" class="btn btn-danger">No</button>
                      </div>
                      <!-- /Remodal  -->

                      <!-- Modal para editar usuario -->
                      <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Editar usuario</h4>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Nombre</label>
                                  <input type="text" class="form-control" id="nameUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Correo</label>
                                  <input type="text" class="form-control" id="emailUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Teléfono</label>
                                  <input type="text" class="form-control" id="cityUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Nivel de Acceso</label>
                                  <select class="form-control">
                                  <option>Administrador</option>
                                  <option>Conciliaciones</option>
                                  <option>Catálogo</option>
                                </select>
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->

                      <!-- Modal para añadir usuario -->
                      <div class="modal fade" id="addUser"  tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Añadir usuario</h4>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Nombre</label>
                                  <input type="text" class="form-control" id="nameUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Correo</label>
                                  <input type="text" class="form-control" id="emailUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Teléfono</label>
                                  <input type="text" class="form-control" id="cityUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Nivel de Acceso</label>
                                  <select class="form-control">
                                  <option>Administrador</option>
                                  <option>Conciliaciones</option>
                                  <option>Catálogo</option>
                                </select>
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->

                  </div>
                </div>
              </div>
            <!-- /Opcion  -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>
    <script src="../js/remodal/remodal.js"></script>
    <!-- Daterange Picker -->
    <script src="../js/moment/moment-with-locales.js"></script>
    <script src="../js/datepicker/daterangepicker.js"></script>

      <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/datatables.scroller.min.js"></script>





    <script>
      $(document).ready(function() {

        moment.locale('es', {
            weekdaysMin : ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            monthsShort : [
                           "Ene", "Feb", "Mar", "Abr", "May", "Jun",
                           "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"
                          ]
        });

        $('#desde').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#hasta').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

         $('#users').DataTable({
          "bFilter": false,
          "sDom": 'Rfrtlip',
         });


      });

    </script>  
  </body>
</html>