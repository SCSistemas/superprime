<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-12 col-sm-12 col-xs-8 col-lg-8 col-md-offset-2">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Administrar Categorías</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-bordered"> 
                        <thead> 
                          <tr>
                            <th>#</th> 
                            <th>Categorias</th>
                            <th>Eliminar</th> 
                            <th>Editar</th> 
                            <th>Superior</th>
                            <th>Página principal</th> 
                          </tr> 
                        </thead> 
                        <tbody> 
                          <tr>
                            <th>1</th> 
                            <td>Filtro 1</td> 
                            <td class="text-center"><a href="" class="btn btn-xs btn-danger" data-remodal-target="correo"><span class="glyphicon glyphicon-remove"></span></a></td>
                            <td class="text-center"><a href="" class="btn  btn-xs btn-primary"  data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a></td>
                            <td>
                              <select class="form-control">
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                </select>
                            </td> 
                            <td class="text-center">
                              <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </td> 
                          </tr> 
                          <tr>
                            <th>2</th> 
                            <td>Filtro 2</td> 
                            <td class="text-center"><a href="" class="btn btn-xs btn-danger" data-remodal-target="correo"><span class="glyphicon glyphicon-remove"></span></a></td>
                            <td class="text-center"><a href="" class="btn  btn-xs btn-primary"  data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a></td>
                            <td>
                            <select class="form-control">
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                </select>
                            </td> 
                            <td class="text-center">
                              <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </td> 
                          </tr> 
                          <tr>
                            <th>3</th> 
                            <td>Filtro 3</td> 
                            <td class="text-center"><a href="" class="btn btn-xs btn-danger" data-remodal-target="correo"><span class="glyphicon glyphicon-remove"></span></a></td> 
                            <td class="text-center"><a href="" class="btn  btn-xs btn-primary"  data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-pencil"></span></a></td>
                            <td>
                              <select class="form-control">
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                </select>
                            </td>
                            <td class="text-center">
                              <input id="publicado" name="publicado" type="checkbox" class="js-switch" />
                            </td> 
                          </tr> 
                        </tbody> 
                      </table>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>

                      <!-- Remodal -->
                      <div class="remodal" data-remodal-id="correo">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Confirme</h1>
                        <p>​Esta categoria tiene productos asociados a ella, si la elimina, serán retirados de publicación estos productos, desea continuar</p>
                        <br>
                        <button data-remodal-action="confirm" class="btn btn-success">Si</button>
                        <button data-remodal-action="cancel" class="btn btn-danger">No</button>
                      </div>
                      <!-- /Remodal  -->

                      <!-- Modal para editar nombre de la categoria -->
                      <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Editar nombre del filtro</h4>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Nombre del Filtro</label>
                                  <input type="text" class="form-control" id="nameFilter" placeholder="">
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary">Guardar</button>
                            </div>
                          </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                      </div><!-- /.modal -->

                  </div>
                </div>
              </div>
            <!-- /Opcion  -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>
    <script src="../js/remodal/remodal.js"></script>
    <!-- Switchery -->
    <script src="../js/switchery/switchery.min.js"></script>
   
  </body>
</html>