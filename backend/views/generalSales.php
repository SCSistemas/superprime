<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Ventas Generales </h2>

                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content generalSales">

                    <div class="row">
                      <div class="col-lg-4 col-md-offset-1">
                        <h1>Diciembre 2016</h1>
                        <h2>Bs 865.000</h2>
                      </div>

                      <div class="col-lg-2">
                        <a href="" class="btn btn-sm btn-info"><i class="fa fa-print" aria-hidden="true"></i> Imprimir reporte</a>
                        <br>
                        <a href="" class="btn btn-sm btn-info"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar PDF</a>
                      </div>

                      <div class="col-lg-4">
                        <table class="" style="width:100%">
                          <tbody>
                          <tr>
                            <td><canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas></td>
                            <td>
                              <table class="tile_info">
                                <tbody><tr>
                                  <td>
                                    <p><i class="fa fa-square blue"></i>IOS </p>
                                  </td>
                                  <td>30%</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square green"></i>Android </p>
                                  </td>
                                  <td>10%</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square purple"></i>Blackberry </p>
                                  </td>
                                  <td>20%</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square aero"></i>Symbian </p>
                                  </td>
                                  <td>15%</td>
                                </tr>
                                <tr>
                                  <td>
                                    <p><i class="fa fa-square red"></i>Others </p>
                                  </td>
                                  <td>30%</td>
                                </tr>
                              </tbody></table>
                            </td>
                          </tr>
                        </tbody>
                        </table>
                      </div>

                    </div>

                      <br>

                     <div class="row">
                        <div class="col-lg-12">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="preview" width="100%">
                        <thead>
                          <tr>
                            <th>Razón social</th>
                            <th>Unicades Vendidas</th>
                            <th>Monto</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><a href="../pages/generalSales-detail1.php">Razón social n°1</a></td>
                            <td>2986</td>
                            <td>30.256 Bs</td>
                          </tr>
                          <tr>
                            <td><a href="../pages/generalSales-detail1.php">Razón social n°2</a></td>
                            <td>98563</td>
                            <td>15.369 Bs</td>
                          </tr>
                          <tr>
                            <td><a href="../pages/generalSales-detail1.php">Razón social n°3</a></td>
                            <td>8596</td>
                            <td>13.302 Bs</td>
                          </tr>
                          <tr>
                            <td><a href="../pages/generalSales-detail1.php">Razón social n°4</a></td>
                            <td>78596</td>
                            <td>150.258 Bs</td>
                          </tr>
                        </tbody>
                      </table>

                      </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>

            


          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

<!-- Switchery -->
    <script src="../js/remodal/remodal.js"></script>
<script>
  $(document).ready(function() {
 
    // Inicializar tabla de vista previa
    $('#preview').DataTable({
      "bFilter": false,
      "sDom": 'Rfrtlip'
    });
  });
</script>

<script>
      $(document).ready(function(){
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "Symbian",
              "Blackberry",
              "Other",
              "Android",
              "IOS"
            ],
            datasets: [{
              data: [15, 20, 30, 10, 30],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
      });
    </script>
  </body>
</html>