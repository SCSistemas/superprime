﻿<?php include '../includes/header.php';?>
<?php session_start();?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

              <!-- Resultados de Sincronizacion -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta de Productos</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- Opciones de consulta -->
                    <div class="well">



                      <form id="formFilter" data-toggle="validator" onsubmit="return false" role="form" class="form-horizontal" _lpchecked="1">


                        <!-- Filtros -->
                        <fieldset style="font-size:12px" >
                          <div class="form-inline" style="min-height:45px">
                            <div class="form-group">
                              <label for="ex3" style="margin-left: 0px;margin-right: 3px;">Tipo de Producto</label>
                              <select id="tipoprod" name="tipo_prod" class="select2_single form-control" tabindex="-1" required>
                                <option value="">Tipo Prod</option>
                                <?php 
                                foreach ($tProducto['tipoproducto'] as $tp){ 
                                ?>
                                  <option value="<?php echo $tp['CodTipoProd']?>"><?php echo $tp['DescTipoProd']?></option>

                                <?php
                                }
                                ?>  
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="ex4">Marca</label>
                              <select id="marca" name="marca"  class="select2_single form-control" tabindex="-1" required>
                                  <option value="">Marcas</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="ex4">Genero</label>
                              <select id="genero" name="genero" disabled="true" class="select2_single form-control" tabindex="-1" required>
                                  <option value="">Generos</option>
                              </select>
                            </div>



                              <div class="form-group">
                                <label for="ex4">Fotos</label>
                                <select id="foto" name="foto" class="select2_single form-control" tabindex="-1" >
                                  <option value="">Todos</option>
                                  <option value="S">Con fotos</option>
                                  <option value="N">Sin fotos</option>
                                </select>
                              </div>

                            <div class="form-group hover">

                              <label class="control-label hover">
                                  <a href="#" data-toggle="tooltip" title="Publicado Web">Web</a> <input id="publicado" name="publicado" type="checkbox" class="js-switch" /></label>
                            </div>
                            <div class="form-group hover">
                              <label class="control-label hover" style="margin-left: 0px;">
                                  <a href="#" data-toggle="tooltip" title="Publicado MercadoLibre">ML</a> <input id="publicado_ml" name="publicado_ml" type="checkbox" class="js-switch" /></label>
                            </div>
                          </div>
                        </fieldset>
                          <br>
                          <fieldset>
                              <div class="control-group">
                                  <div class="controls">
                                      <div class="row">
                                          <div class="col-md-3">
                                              <div class="input-group">
                                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                  <input type="text" class="form-control" id="fecha_desde" name="fecha_desde" placeholder="Desde" >
                                              </div>
                                          </div>
                                          <div class="col-md-3">
                                              <div class="input-group">
                                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                  <input type="text" class="form-control" id="fecha_hasta" name="fecha_hasta" placeholder="Hasta" >
                                              </div>
                                          </div>
                                          <div class="col-md-5">
                                              <div class="form-inline">
                                                  <div class="form-group">
                                                      <input type="text" id="cod_prod" name="cod_prod" style="width:80px" class="form-control" placeholder="Código">
                                                      <input type="text" id="desc_sap" name="desc_sap" class="form-control" placeholder="Descripción">
                                                      <label class="control-label hover" style="margin-left: 0px;">
                                                      <a href="#" data-toggle="tooltip" title="Productos Ignorados">Ignorados</a> <input id="ignorados" name="ignorados" type="checkbox" class="js-switch" /></label>
                                                  </div>
                                              </div>

                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </fieldset>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-center" style="padding-top:10px">
                                <button id="search" class="btn btn-success">Buscar</button>
                            </div>
                        </div>
                      </form>
                    </div>
                    <!-- /Opciones de consulta -->


                    <p class="text-muted font-13 m-b-30"></p>

                      <table id="consulta" class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>Código</th>
                          <th>Marca</th>
                          <th>Descripción SAP</th>
                          <th>Tipo de Producto</th>
                          <th>Género</th>
                          <th>Fecha de Sincronización</th>
                          <th>​Publicado en Web</th>
                          <th>​Publicado en ML</th>
                          <th>Colores Sin Imagenes</th>
                          <th>Ignorado</th>
                          <th>Opciones</th>
                        </tr>
                      </thead>

                    </table>

                    </div>
                    <div class="text-center">
                      <button id="generarPDF" disabled="true" class="btn btn-primary"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp; Generar Reporte</button>
                      <button id="correoE" disabled="true"  data-remodal-target="correo" class="btn btn-primary"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; Enviar por Correo</button>
                      <!--<a href="#" id="generarPDF"class="btn btn-primary" data-remodal-target="reporte"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp; Generar Reporte</a>-->
                     <!-- <a href="#" class="btn btn-primary" id="correoE" data-remodal-target="correo"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; Enviar por Correo</a>-->
                    </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Resultados de Sincronizacion -->
              
              <!-- Remodal para generar reporte 
              <div class="remodal" data-remodal-id="reporte">
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1>Reporte</h1>
                <p>Se esta generando el reporte, apenas esté listo se descargará automaticamente.</p>
                <br>
                <button data-remodal-action="confirm" class="btn btn-success">OK</button>
              </div>-->
              <!-- /Remodal para generar reporte -->

              <!-- Remodal para enviar por correo -->
              <div class="remodal" data-remodal-id="correo">
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1>Enviando por correo</h1>
                <p>​Se está enviando el reporte por correo electronico.</p>
                <br>
                <button data-remodal-action="confirm" class="btn btn-success">OK</button>
              </div>
              <!-- /Remodal para enviar por correo -->

          </div>



        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
    <?php include('../includes/scripts.php') ?>

    <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../js/jszip/dist/jszip.min.js"></script>
    <script src="../js/pdfmake/build/pdfmake.min.js"></script>
    <script src="../js/pdfmake/build/vfs_fonts.js"></script>

    
    <!-- datepicker -->
    <script src="../js/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="../js/datepicker/locales/bootstrap-datepicker.es.min.js"></script>

    <!-- Switchery -->
    <script src="../js/switchery/switchery.min.js"></script>

    <!-- Switchery -->
    <script src="../js/remodal/remodal.js"></script>

    <!-- Datatables -->
    <script>

      $(document).ready(function() {



          $('[data-toggle="tooltip"]').tooltip();

          Chkpublicado = document.querySelector('#publicado');
          Chkpublicadoml = document.querySelector('#publicado_ml');
          Chkignorados = document.querySelector('#ignorados');


          actualizaIgnorar=function(codigoProducto,ignorar) {
              var parametros={
                  'p_cod_prod':codigoProducto,
                  'p_ignorado':ignorar
              };
              $.ajax({
                      type: "POST",
                      url: "../../admin/index.php?service=productoservices&metodo=ActualizaIgnorado",
                      data:JSON.stringify(parametros),
                      contentType : "application/json",
                      success: function (respuesta) {
                          var ajaxResponse = $.parseJSON(respuesta);
                          Tabla.ajax.reload(null,false);

                      }
                  }
              )

          } ;


          devuelveMarcas=function(){
              $.ajax({
                  type: "GET",
                  url: "../../admin/index.php?service=productoservices&metodo=ObtenerMarcasProducto&p_cod_tipo_prod="+$('#tipoprod').val(),
                  success: function (respuesta) {
                      var ajaxResponse = $.parseJSON(respuesta);
                      $('#marca').prop('disabled',true);
                      $('#marca').html("").append("<option value=''>Marcas</option>");
                      $.each(ajaxResponse,function(v,k){
                          $('#marca').prop('disabled',false);
                          $('<option>').val(k.CodMarca).text(k.DescMarca).appendTo('#marca');
                      });


                  },
                  complete: function () {
                      $("#myModal").modal({backdrop: false});
                  }
              })

          } ;
          devuelveGeneros=function(){
              $.ajax({
                  type: "GET",
                  url: "../../admin/index.php?service=productoservices&metodo=ObtenerGenerosProducto&p_cod_tipo_prod="+$('#tipoprod').val(),
                  success: function (respuesta) {
                      var ajaxResponse = $.parseJSON(respuesta);
                      $('#genero').prop('disabled',true);

                      $('#genero').html("").append("<option value=''>Generos</option>");
                      $.each(ajaxResponse,function(v,k){
                          $('#genero').prop('disabled',false);
                          $('#genero').blur();
                          $('<option>').val(k.CodGenero).text(k.DescGenero).appendTo('#genero');
                      });


                  },
                  complete: function () {
                      $("#myModal").modal({backdrop: false});
                  }
              })

          } ;



          setEventosConsulta = function(){
              $('#tipoprod').off("change").on("change", function() {
                  devuelveMarcas();
                  devuelveGeneros();
              });

          };
          setEventosConsulta();

          Tabla= $('#consulta').DataTable( {
              "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningún dato disponible para su búsqueda",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  //"sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "Último",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
              columnDefs: [{ targets: [9], visible: false }],
              "processing": true,
              "serverSide": true,
              "deferLoading": 0,
              "sort": false,
              "searching": false,    //deshabilitar el filtro de búsqueda
              "bFilter" : false,    //deshabilitar el length menu
              "bLengthChange": false, //deshabilitar el length menu
              "pageLength": 5, //definir número de registros por páginas
              "ajax": {
                  'url': '../ajax/ConsultarProductos.php',
                  'type': 'POST',
                  data: function ( d ) {
                      
                      $.each($('#formFilter').serializeArray(), function(_, kv) {
                          d[kv.name] = (kv.value == 'on')?'S':((kv.value == 'off')?'N':kv.value);
                      });
                      d['publicado']=Chkpublicado.checked==true?'S':'N';
                      d['publicado_ml']=Chkpublicadoml.checked==true?'S':'N';
                      d['ignorados']=Chkignorados.checked==true?'S':'N';
                      return d;
                  },



                   "dataSrc": function ( json ) {
                        //alert(json);
                        //console.log('url: '+json.data);
                        if (json.data != ""){


                          //console.log(json);
                          $('#generarPDF').prop('disabled',false);
                          $('#correoE').prop('disabled',false);

                        }
                        else{
                          $('#generarPDF').prop('disabled',true);
                          $('#correoE').prop('disabled',true);
                        }
                  
                        return json.data;
                    }
              },
              "drawCallback": function(settings, json) {
                  for ( var i=0;i<settings.aoData.length; i++){
                      var producto=settings.aoData[i]._aData[0];
                      $('#btig-'+producto).click(function() {
                          var cod_producto=this.getAttribute('data-producto');
                          var ignorar=this.getAttribute('data-ignorar');
                          actualizaIgnorar(cod_producto,ignorar);
                      });
                  };
              }
          } );

      });

      $("#cod_prod").change(function(){
          if($(this).val()!= '' ){
              $('.form-group').removeClass("has-error");
              $('#tipoprod').removeAttr('required');
              $('#marca').removeAttr('required');
              $('#genero').removeAttr('required');
          }
      });
      $("#desc_sap").change(function(){
          if($(this).val()!= '' ){
              $('.form-group').removeClass("has-error");

          }
      });

      $("#search").click(function() {

         if($('#cod_prod').val()!= '' || $('#desc_sap').val()!= '' || Chkignorados.checked==true ) {
             $('#tipoprod').removeAttr('required');
             $('#marca').removeAttr('required');
             $('#genero').removeAttr('required');
             $('.form-group').removeClass("has-error");
             Tabla.ajax.reload(null,false);
         } else  if (($('#tipoprod').val()!= '') && ($('#marca').val()!= '')&& (($('#genero').attr('disabled')=='disabled') ? true : $('#genero').val() != '')){
             Tabla.ajax.reload(null,false);
         }



         
      });

      $('#generarPDF').click( function() {
        
        window.location.href='../pdf/generarPDF.php';

      });
      $('#correoE').click( function() {
          $.ajax({
              type: "POST",
              url: '../PHPMailer/enviarCorreo.php',
              success: function (respuesta) {


              }
          })
          
      });

        $('#fecha_desde, #fecha_hasta').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            todayHighlight: true
        });







    </script>



  </body>
</html>