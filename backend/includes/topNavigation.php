<div class="top_nav">

          <div class="nav_menu row">
              <div class="col-md-1">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                  <a href=""></a>
                </div>
              </div>
              <div class="col-md-11">
                <ul class="nav navbar-nav navbar-right">
                  <li class="pull-left"><h1>Sistema de Administración de Productos</h1></li>
                  <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="nameTop">
                      <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                      <li><a href="javascript:;">  Perfil</a>
                      </li>
                      <li>
                        <a href="javascript:;">
                          <span>Configuración</span>
                        </a>
                      </li>

                      <li><a href="../"><i class="fa fa-sign-out pull-right"></i> Salir</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
          </div>
        </div>

<script>
    $.getJSON( "<?php echo  $urlWS?>service=userservices&metodo=ObtenerDatosPerfil&p_id_usuario=<?php echo  $_SESSION['userid']?>")
        .done(function( json ) {
            var name = json.Persona.Nombre+(json.Persona.Apellido != ''?(' '+json.Persona.Apellido):'');
            $("#nameTop").html(name);
            $("#nameMenu").html(name);
        })
        .fail(function( jqxhr, textStatus, error ) {
            var err = textStatus + ", " + error;
            console.log( "Request Failed: " + err );
        });

</script>