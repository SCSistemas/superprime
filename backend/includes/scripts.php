
    <!-- jQuery -->
    <script src="../js/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../js/bootstrap/js/bootstrap.min.js"></script>
    <!-- NProgress -->
    <script src="../js/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../js/iCheck/icheck.min.js"></script>

    <!-- Validator -->
    <script src="../js/validator.js"></script>

    <!-- Datepicker y locales -->
    <script src="../js/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="../js/datepicker/locales/bootstrap-datepicker.es.min.js"></script>


    <!-- Mascara de campos -->
    <script src="../js/mask/jquery.mask.min.js"></script>

    <!-- Numeric -->
    <script src="../js/numeric/jquery.numeric.min.js"></script>
    

    <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../js/jszip/dist/jszip.min.js"></script>
    <script src="../js/pdfmake/build/pdfmake.min.js"></script>
    <script src="../js/pdfmake/build/vfs_fonts.js"></script>

    <script src="../js/Chart.js/dist/Chart.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../js/custom.js"></script>
    <script src="../js/remodal/remodal.js"></script>
