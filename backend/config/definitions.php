<?php
	//$urlWS = 'http://qa.superprimeweb.com/admin/index.php?';
    $urlWS = 'http://localhost/superprime/admin/index.php?';
	$rutaFotos = '../images/fotos/';
	//$rutaFotos = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/backend/images/fotos/';
	$variable = array('ml_id','ml_secretKey', 'ml_authCode', 'ml_token', 'ml_fecToken', 'ml_expires', 'ml_redirectUri');
	//ini_set('display_errors', '1');
	foreach ($variable as $key ) {
		$dataML[$key] = obtenerClaveValor($urlWS, $key);
	}

    function comprobarRenovarToken($dataML,$urlWS){
    	$fecExpira = $dataML['ml_fecToken'] + $dataML['ml_expires'];
		//print_r('fecha expira: ' . $fecExpira . ', fecha actual: ' . time() . ', fecha(300s): ' . ($fecExpira - time()) . '</br>');
		if(time() > ($fecExpira) ||	($fecExpira - time()) < 300){//si el token expiro o su tiempo de vida es menor de 5 min(300 Seg) se renueva
			$tokenPre = $dataML['ml_token'];
			//print_r('TokenPre: '.$tokenPre.'</br>');
			$dataML = refreshToken($dataML, $urlWS);
			$tokenPost = $dataML['ml_token'];
			//print_r('TokenPost: '.$tokenPost.'</br>');

			if($tokenPre == $tokenPost){//hubo una falla en la renovacion del token
				$dataML['error'] = true;
			}else{
				$dataML['error'] = false;
			}
		}else{//no hace falta la renovacion ya que el token aun esta activo
			$dataML['error'] = false;
		}

		return $dataML;
    }

    function actulizarClaveValor($urlWS, $clave, $valor){
    	$hashParam = array('p_clave' => $clave, 'p_valor' => $valor);
		$hashParam = json_encode($hashParam);
		$hashUrl = $urlWS . 'service=generalservices&metodo=actualizarClaveValor';
		$curl = curl_init($hashUrl);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $hashParam);
		$responseHash = curl_exec($curl);
		curl_close($curl);
    }

	function obtenerClaveValor($urlWS, $clave){
    	$urlMLData = $urlWS . 'service=generalservices&metodo=consultarClaveValor&p_clave=' . $clave;
    	//$respMLData = file_get_contents($urlMLData);
    	//$respMLData = substr($respMLData, 3);
		//$respMLData = json_decode($respMLData, true);
		$respMLData = consumoServicioGet($urlMLData);

		return $respMLData['valor'];
    }

    function obtenerCode($dataML){
    	$service_url = 'http://auth.mercadolibre.com.ve/authorization?response_type=code&client_id=' . $dataML['ml_id'] . '&redirect_uri=' . $dataML['ml_redirectUri'];
    	//print_r('urlML: ' . $service_url);
		$redir = "Location: " . $service_url;
		//print_r('</br>service: ' . $redir);
		header($redir);
    }

    function obtenerToken($dataML, $urlWS){
    	$tokenParam = array('grant_type' => 'authorization_code',
					    	'client_id' => $dataML['ml_id'],
							'client_secret' => $dataML['ml_secretKey'],
							'code' => $dataML['ml_authCode'],
							'redirect_uri' => $dataML['ml_redirectUri']);
		//print_r($tokenParam);
		//print_r('tokenParam: ' . json_encode($tokenParam) . '</br>');

		$service_url = 'https://api.mercadolibre.com/oauth/token';
		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $tokenParam);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$responseToken = curl_exec($curl);
		curl_close($curl);
		//print_r('respToken: ' . $responseToken . '</br>');

		//se actualizan los valores en la variable de sesion
		if($responseToken != null){
			$responseToken = json_decode($responseToken,true);
			//print_r($responseToken);

			if(!isset($responseToken['message'])){
				$dataML['ml_fecToken'] = (time() - 30);
				$dataML['ml_expires'] = $responseToken['expires_in'];
				$dataML['ml_token'] = $responseToken['access_token'];
				$dataML['ml_authCode'] = $responseToken['refresh_token'];

				actulizarClaveValor($urlWS, 'ml_fecToken', $dataML['ml_fecToken']);
				actulizarClaveValor($urlWS, 'ml_expires', $dataML['ml_expires']);
				actulizarClaveValor($urlWS, 'ml_token', $dataML['ml_token']);
				actulizarClaveValor($urlWS, 'ml_authCode', $dataML['ml_authCode']);
			}
		}

		return $dataML;
    }

    function refreshToken($dataML, $urlWS){
    	$tokenParam = array('grant_type' => 'refresh_token',
					    	'client_id' => $dataML['ml_id'],
							'client_secret' => $dataML['ml_secretKey'],
							'refresh_token' => $dataML['ml_authCode']);
		//print_r('tokenParam: ' . json_encode($tokenParam) . '</br>');
		//print_r('tokenParam:</br>');
		//print_r($tokenParam);

		$service_url = 'https://api.mercadolibre.com/oauth/token';
		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $tokenParam);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$responseToken = curl_exec($curl);
		curl_close($curl);

		//print_r('respToken: ' . $responseToken . '</br>');
		//print_r(json_decode($responseToken,true));

		if($responseToken != null){
			$responseToken = json_decode($responseToken,true);
			//print_r('responseToken:</br>');
			//print_r($responseToken);

			if(!isset($responseToken['message'])){
				$dataML['ml_authCode'] = $responseToken['refresh_token'];
				$dataML['ml_fecToken'] = (time() - 30);
				$dataML['ml_expires'] = $responseToken['expires_in'];
				$dataML['ml_token'] = $responseToken['access_token'];

				actulizarClaveValor($urlWS, 'ml_authCode', $dataML['ml_authCode']);
				actulizarClaveValor($urlWS, 'ml_fecToken', $dataML['ml_fecToken']);
				actulizarClaveValor($urlWS, 'ml_expires', $dataML['ml_expires']);
				actulizarClaveValor($urlWS, 'ml_token', $dataML['ml_token']);
			}
		}

		return $dataML;
    }

    function consumoServicioGet($urlWS){

    	try {
			//print_r('</br>WS get: WS: ' . $urlWS . '</br>');
			$curl = curl_init($urlWS);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$resp = curl_exec($curl);
			curl_close($curl);
			//print_r('WS get: resp: ' . $resp . '</br>');
			//$resp = substr($resp, 3);
			$resp = json_decode($resp, true);

			return $resp;
		} catch (Exception $e) {
			$error = $e->getMessage();
            $resp = array('success' => '0', 'error' => $error);
            return $resp;
		}
	}

	function consumoServicioPost($urlWS, $params){
		try {
			//print_r('</br>WS Post: WS: ' . $urlWS . '</br>');
			//print_r('params: ' . $params . '</br>');
			$curl = curl_init($urlWS);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			$resp = curl_exec($curl);
			//print_r('WS Post: resp: ' . $resp . '</br>');
			//$resp = substr($resp, 3);
			$resp = json_decode($resp, true);
			curl_close($curl);

			return $resp;
		} catch (Exception $e) {
			$error = $e->getMessage();
            $resp = array('success' => '0', 'error' => $error);
            return $resp;
		}
	}

	function consumoServicioPostSSL($urlWS, $params){

		try {
			//print_r('</br>WS PostSSL: WS: ' . $urlWS . '</br>');
			//print_r('params: ' . $params . '</br>');
			$curl = curl_init($urlWS);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$resp = curl_exec($curl);
			curl_close($curl);
			//print_r('WS PostSSL: resp: ' . $resp . '</br>');
			$resp = json_decode($resp, true);

			return $resp;
		} catch (Exception $e) {
			$error = $e->getMessage();
            $resp = array('success' => '0', 'error' => $error);
            return $resp;
		}
	}

	function consumoServicioPut($urlWS, $params){

		try {
			//print_r('</br>WS Put: WS: ' . $urlWS . '</br>');
			//print_r('params: ' . $params . '</br>');
			$curl = curl_init($urlWS);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$resp = curl_exec($curl);
			curl_close($curl);
			//print_r('WS Put: resp: ' . $resp . '</br>');
			$resp = json_decode($resp, true);

			return $resp;
		} catch (Exception $e) {
			$error = $e->getMessage();
            $resp = array('success' => '0', 'error' => $error);
            return $resp;
		}
	}

	function consumoServicioPutSSL($urlWS, $params){

		try {
			//print_r('</br>WS Put: WS: ' . $urlWS . '</br>');
			//print_r('params: ' . $params . '</br>');
			$curl = curl_init($urlWS);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$resp = curl_exec($curl);
			curl_close($curl);
			//print_r('WS Put: resp: ' . $resp . '</br>');
			$resp = json_decode($resp, true);

			return $resp;
		} catch (Exception $e) {
			$error = $e->getMessage();
            $resp = array('success' => '0', 'error' => $error);
            return $resp;
		}
	}

?>
