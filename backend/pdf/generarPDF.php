<?php
	include '../config/definitions.php';
	$ch = curl_init();

	session_start();
session_start();
$url = $urlWS.'service=userservices&metodo=ObtenerDatosPerfil&p_id_usuario='.$_SESSION['userid'];
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$tPersona = json_decode($resultData, true);

	
    $fecha_desde = $_SESSION['data']['fecha_desde'];
    $fecha_hasta = $_SESSION['data']['fecha_hasta'];
    $tipo_prod = $_SESSION['data']['tipo_prod'];
    $marca = $_SESSION['data']['marca'];
    $genero = $_SESSION['data']['genero'];
    $publicado = $_SESSION['data']['publicado'];
    $p_publicado_ml = $_SESSION['data']['publicado_ml'];
    $p_cod_prod = $_SESSION['data']['cod_prod'];
    $p_desc_sap = $_SESSION['data']['desc_sap'];
    $p_foto = $_SESSION['data']['foto'];
    $p_ignorados = $_SESSION['ignorados'];
    $recordsTotal = $_SESSION['data']['recordsTotal'];


$url = $urlWS.'service=productoservices&metodo=ObtenerProductos&p_fecha_desde='.$fecha_desde.'&p_fecha_hasta='.$fecha_hasta.'&p_tipo_prod='.$tipo_prod.'&p_marca='.$marca.'&p_genero='.$genero.'&p_publicado='.$publicado.'&p_cod_prod='.$p_cod_prod.'&p_desc_sap='.$p_desc_sap.'&p_foto='.$p_foto.'&p_publicado_ml='.$p_publicado_ml.'&ignorados='.$p_ignorados.'&start=0&length='.$recordsTotal.'&draw=1';

	
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$resultData = json_decode(curl_exec($ch),true);
	if ($publicado)
		$pw = 'SI';
	else
		$pw = 'NO';

	if ($p_publicado_ml)
		$pml = 'SI';
	else
		$pml = 'NO';

	if ($genero == 1)
		$g = 'Masculino';
	else
		{
			if ($genero == 2)
				$g = 'Femenino';
		}

	if ($p_foto == 'S')
		$f = 'SI';
	if ($p_foto == 'N')
		$f = 'NO';
	if ($p_foto == '')
		$f= 'todos';

	/*$seleccion = 'Fecha: desde '. $fecha_desde.' hasta '. $fecha_hasta . ' ->'.'Tipo de Producto: '.$tipo_prod.
				 ' ->'.'Marca: '. $marca .' ->'.' Genero: '.$g. ' ->'.' Publicado en Web: '. $pw . 
				 ' ->'. 'Publicado en ML: ' . $pml. ' ->'. 'Foto: '.$f;
		echo ($seleccion);
				 
	   exit;*/

	curl_close($ch);


	ob_start();
	include("../templates/listConsultProduct.php");
	$page = ob_get_contents();  // almacenar el resultado de la salida en una variable
	ob_end_clean();				// limpiar buffer de salida hasta este punto .. 
	
	require_once("../dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($page);
	$dompdf->set_paper("letter","landscape");
	$dompdf->render();
	$dompdf->stream("consulta_productos.pdf");

?>
