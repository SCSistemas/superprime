<?php
	include '../config/definitions.php';

	//echo('<pre>');
	//ini_set('display_errors', '1');

	try {
		$id = $_POST['id'];
		$categorias = $_POST['categorias'];
		$plan = $_POST['plan'];
		$publicacion = $_POST['pub'];
		$idsML = 'vacio';

		$urlRevision = $urlWS . 'service=productoservices&metodo=consultarPublicacionProducto&p_codProducto=' . $id;
		//print_r("urlRevision: " . $urlRevision . '</br>');
		//$respRevision = file_get_contents($urlRevision);
		//$respRevision = json_decode($respRevision, true);
		$respRevision = consumoServicioGet($urlRevision);
		//print_r($respRevision);

		$catBD = explode(';', $categorias);
		$categoria = $catBD[(count($catBD) - 1)];

		$urlDetaCat = 'https://api.mercadolibre.com/categories/' . $categoria;
		//print_r('urlDetaCat: ' . $urlDetaCat . '</br>');
		$respDetaCat = consumoServicioGet($urlDetaCat);
		//print_r($respDetaCat);

		$WS = $urlWS . 'service=productoservices&metodo=ObtenerDetalleProducto&p_cod_prod=' . $id;
		//print_r('urldetaProd: ' . $WS . '</br>');
		$detaProd = consumoServicioGet($WS);
		//print_r($detaProd);

		$nomProd = $detaProd['producto']['Nombre'];
		if(!isset($detaProd['producto']['ColoresProducto'][0]['DescWeb'])){
			$nomProd .= ' ' . $detaProd['producto']['ColoresProducto'][0]['DescWeb'];
		}else{
			$nomProd .= ' ' . $detaProd['producto']['ColoresProducto'][0]['DescSap'];
		}

		//$descProd = $detaProd['producto']['Nombre'];
		//print_r('nomProd: ' . $nomProd);
		$descProd = $nomProd;
		//print_r('descProd: ' . $descProd);
		$precio = 0;

		foreach ($detaProd['producto']['ColoresProducto'] as $value) {
			$WS = $urlWS . 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod=' . $id . '&p_color=' . $value['CodColor'];
			//print_r('urlDetaColorProd: ' . $WS . '</br>');
			$detaColorProd = consumoServicioGet($WS);
			//print_r($detaColorProd);

			if($precio == 0 ){
				if(!is_null(($detaColorProd['colorProd']['PreciosWeb']['Total']))){
					$precio = $detaColorProd['colorProd']['PreciosWeb']['Total'];
				}else{
					$precio = $detaColorProd['colorProd']['PreciosSap']['Total'];
				}
				//print_r('precio: '.$precio);
			}
		}

		if(isset($respRevision[0]['codProducto'])){
			//print_r('posee un registro en la BD');
			//posee un registro en la BD
			if($publicacion == 'S'){
				//se publicara en ML
				if($respRevision[0]['PublicadoMl'] == 'N'){
					//no se habia publicado en la ultima administracion, se procede a publicar ML y modificar en BD
					$dataML = comprobarRenovarToken($dataML, $urlWS);
					$error = $dataML['error'];

					if(!$error){
						$resultado = guardarBDPublicarML($urlWS, $dataML, 2, $nomProd, $categorias, $precio, $plan, $descProd, $id, $rutaFotos, $publicacion);
					}else{
						$resultado = generarRespuesta('2', null);
					}
				}else{
					$resultado = generarRespuesta('4', null);
				}
			}else{
				//no se publicara en ML
				if($respRevision[0]['PublicadoMl'] == 'S'){
					//si anteriormente se habia publicado en ML, se borran las publicaciones
					$dataML = comprobarRenovarToken($dataML, $urlWS);
					$error = $dataML['error'];

					//print_r($dataML);
					if(!$error){
						$parametros = array('status' => 'paused');
						$WS = 'https://api.mercadolibre.com/items/' . $respRevision[0]['idPubML'] . '?access_token=' . $dataML['ml_token'];
						$parametros = json_encode($parametros);
						//print_r('paramDelete: ' . $parametros . '</br>');
						$responseML = consumoServicioPutSSL($WS, $parametros);
						//print_r($responseML);
						if(!isset($responseML['error'])){;
							$parametros = array('deleted' => 'true');
							$WS = 'https://api.mercadolibre.com/items/' . $respRevision[0]['idPubML'] . '?access_token=' . $dataML['ml_token'];
							$parametros = json_encode($parametros);
							//print_r('paramDelete: ' . $parametros . '</br>');
							$responseML = consumoServicioPutSSL($WS, $parametros);

							$resultado = guardarBD($urlWS, 2, $id, $plan, $categorias, $publicacion);	
						}else{
							$resultado = generarRespuesta('0', null);
						}
					}else{
						$resultado = generarRespuesta('2', null);
					}
				}else{
					$resultado = guardarBD($urlWS, 2, $id, $plan, $categorias, $publicacion);
				}
			}
		}else{
			//no posee un registro en la BD, se inserta la data, se publica en ML de ser el caso.
			//print_r('no posee un registro en la BD');
			if($publicacion == 'S'){
				//si el check de publicar se encuentra activo
				$dataML = comprobarRenovarToken($dataML, $urlWS);
				$error = $dataML['error'];

				if(!$error){
					$resultado = guardarBDPublicarML($urlWS, $dataML, 1, $nomProd, $categorias, $precio, $plan, $descProd, $id, $rutaFotos, $publicacion);
				}else{
					$resultado = generarRespuesta('2', null);
				}
			}else{
				$resultado = guardarBD($urlWS, 1, $id, $plan, $categorias, $publicacion);
			}
		}

		//print_r($resultado);
	} catch (Exception $e) {
		$resultado = generarRespuesta('0', null);
	}

	echo($resultado);
	
	//echo('</pre>');

	function generarRespuesta($valor, $ids){
		if($valor == '0') {
			$mensaje = 'Error guardando los datos de tu publicaci&oacute;n por favor intenta m&aacute;s tarde';
		} elseif($valor == '1'){ 
			$mensaje = 'Tu Publicaci&oacute;n fue guardada Correctamente...';
			if($ids != null){
				$mensaje .= '</br>&emsp;Publicaci&oacute;n en Mercado Libre:</br>' . $ids; 
			}
		} elseif($valor == '2'){
			$mensaje = 'Se ha producido un problema con la autencaci&oacute;n con Mercado Libre, por favor prueba m&aacute;s tarde';
		} elseif($valor == '3'){
			$mensaje = $ids;
		} elseif($valor == '4'){
			$mensaje = 'Tu Publicaci&oacute;n no ha sufrido cambios';
		}
		
		$resp = array('success' => $valor, 'message' => $mensaje);
		$resp = json_encode($resp);
		
		return $resp;
	}

	function guardarBD($urlWS, $opcion, $codProducto, $plan, $categorias, $publicado){
		$parametros = array('p_codProducto' => $codProducto, 
							'p_codColor' => null, 
							'p_plan' => $plan, 
							'p_categorias' => $categorias, 
							'p_idPubML' => null, 
							'p_publicado' => $publicado);
		//print_r($parametros);
		$parametros = json_encode($parametros);
		//print_r('paramSave: '.$parametros.'</br>');
		if($opcion == 1){
			//se inserta en BD
			$WS = $urlWS . 'service=productoservices&metodo=insertarPublicacion';
		}else{
			//se actualiza en BD
			$WS = $urlWS . 'service=productoservices&metodo=actualizarPublicacion';
		}
		
		$resp = consumoServicioPost($WS, $parametros);
		$resultado = generarRespuesta($resp['success'], null);
		
		return $resultado;
	}

	function guardarBDPublicarML($urlWS, $dataML, $opcion, $nomProd, $categorias, $precio, $plan, $descProd, $codProd, $rutaFotos, $publicacion){

		$comoComprarML  = '<div>';
		$comoComprarML .= '	<table>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<img src="' . $rutaFotos . 'ComprarML/membrete.png"/>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td>';
		$comoComprarML .= '				<img src="' . $rutaFotos . 'ComprarML/pasos1-4.png"/>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '			<td>';
		$comoComprarML .= '				<a href="http://www.grupozoom.com/" target="_blank">';
		$comoComprarML .= '					<img src="' . $rutaFotos . 'ComprarML/importante.png"/>';
		$comoComprarML .= '				</a>';
		$comoComprarML .= '				<a href="http://www.grupozoom.com/atencion/atencion.php?cat=20" target="_blank">';
		$comoComprarML .= '					<img src="' . $rutaFotos . 'ComprarML/reglas.png"/>';
		$comoComprarML .= '				</a>';
		$comoComprarML .= '				<img src="' . $rutaFotos . 'ComprarML/direccion.png"/>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<img src="' . $rutaFotos . 'ComprarML/cuerpo1.png"/>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<a href="http://www.grupozoom.com/" target="_blank">';
		$comoComprarML .= '					<img src="' . $rutaFotos . 'ComprarML/costo-envio.png"/>';
		$comoComprarML .= '				</a>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<a href="http://www.grupozoom.com/atencion/atencion.php?cat=20" target="_blank">';
		$comoComprarML .= '					<img src="' . $rutaFotos . 'ComprarML/mensajeros.png"/>';
		$comoComprarML .= '				</a>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<img src="' . $rutaFotos . 'ComprarML/cuerpo2.png"/>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<a href="http://www.grupozoom.com/" target="_blank">';
		$comoComprarML .= '					<img src="' . $rutaFotos . 'ComprarML/producto-asegurado.png"/>';
		$comoComprarML .= '				</a>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '		<tr>';
		$comoComprarML .= '			<td colspan="2">';
		$comoComprarML .= '				<img src="' . $rutaFotos . 'ComprarML/cuerpo3.png"/>';
		$comoComprarML .= '			</td>';
		$comoComprarML .= '		</tr>';
		$comoComprarML .= '	</table>';
		$comoComprarML .= '</div>';
		
		$catBD = explode(';', $categorias);
		$categoria = $catBD[(count($catBD) - 1)];

		$publicParam = array('title' => $nomProd,
							 'category_id' => $categoria,
							 'price' => $precio,
							 'currency_id' => 'VEF',
							 'buying_mode' => 'buy_it_now',
							 'listing_type_id' => $plan,
							 'condition' => 'new',
							 //'description' => $descProd,
							 'description' => $comoComprarML,
						     'tags' => array("immediate_payment")
							);

		$publicParam['variations'] = array();
		$publicParam['variations'] = generarVariaciones($urlWS, $precio, $codProd, $categoria, $rutaFotos);

		$WS = 'https://api.mercadolibre.com/items?access_token=' . $dataML['ml_token'];
		//print_r('urlPublicML: ' . $publicParam . '</br>');
		//print_r($publicParam);
		$publicParam = json_encode($publicParam);
		//print_r('paramPublicML: '.$publicParam.'</br>');
		$responseML = consumoServicioPostSSL($WS, $publicParam);										
		//print_r($responseML);

		if(!isset($responseML['error'])){
			$idsML = '&emsp;&emsp;<a href="' . $responseML['permalink'] . '" target="blank">' . $responseML['permalink'] . '</a>';
			//print_r($idsML . '</br>');

			$parametros = array('p_codProducto' => $codProd, 
								'p_codColor' => null, 
								'p_plan' => $plan, 
								'p_categorias' => $categorias, 
								'p_idPubML' => $responseML['id'], 
								'p_publicado' => $publicacion);
			//print_r($parametros);
			$parametros = json_encode($parametros);
			//print_r('paramSave: '.$parametros.'</br>');
			if($opcion == 1){
				//se inserta en BD
				$WS = $urlWS . 'service=productoservices&metodo=insertarPublicacion';
			}else{
				//se actualiza en BD
				$WS = $urlWS . 'service=productoservices&metodo=actualizarPublicacion';
			}
			$resp = consumoServicioPost($WS, $parametros);
			$resultado = generarRespuesta($resp['success'], ($idsML == 'vacio') ? null : $idsML);
		}else{
			$idsML = erroresML($responseML);
			$resultado = generarRespuesta('3', $idsML);
		}

		return $resultado;
	}

	function generarVariaciones($WS, $precio, $codProd,$categoria,$rutaFotos){

		$encontroTalla = false;

		$urlDetaCat = 'https://api.mercadolibre.com/categories/' . $categoria;
		//$respDetaCat = file_get_contents($urlDetaCat);
		//$respDetaCat = json_decode($respDetaCat, true);
		$respDetaCat = consumoServicioGet($urlDetaCat);
		//print_r($respDetaCat);

		if($respDetaCat['attribute_types'] == 'variations'){
			//la categoria tiene variaciones

			//se buscan las variaciones de la cateogoria 
			$urlAttr = 'https://api.mercadolibre.com/categories/' . $categoria . '/attributes';
			//$respAttr = file_get_contents($urlAttr);
			//$respAttr = json_decode($respAttr, true);
			$respAttr = consumoServicioGet($urlAttr);
			//print_r($respAttr);

			$colorPrimario = array();
			$colorSecundario = array();
			$tallas = array();

			foreach ($respAttr as $atributo) {
				if($atributo['id'] == 11000){
					foreach ($atributo['values'] as $value) {
						$color['id_cat'] = '11000';
						$color['id'] = $value['id'];
						$color['desc'] = $value['name'];
						array_push($colorPrimario, $color);
					}
				}else if($atributo['id'] == 103000){
					foreach ($atributo['values'] as $value) {
						$talla['id_cat'] = '103000';
						$talla['id'] = $value['id'];
						$talla['desc'] = $value['name'];
						array_push($tallas, $talla);
					}
				}
			}

			if(count($talla) > 0){
				//existen variaciones de talla para la categoria
				$urlWS = $WS . 'service=productoservices&metodo=ObtenerDetalleProducto&p_cod_prod=' . $codProd;
				$detaProd = consumoServicioGet($urlWS);
				$variaciones = array();

				$cantColores = 0;
				foreach ($detaProd['producto']['ColoresProducto'] as $colores) {
					$cantColores++;
				}

				if($cantColores == 0){
					$cantColores = 1;
				}

				$cantFotos = floor(10/$cantColores);
				foreach ($detaProd['producto']['ColoresProducto'] as $colores) {
					$variacion['price'] = $precio;
					$variacion['attribute_combinations'] = array();

					foreach ($colorPrimario as $color) {

						//if(trim(strtoupper($color['desc'])) == trim(strtoupper($colores['DescColor']))){
						if(trim(strtoupper($color['desc'])) == trim(strtoupper($colores['DescWeb']))){
							$urlWS = $WS . 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod=' . $codProd . '&p_color=' . $colores['CodColor'];
							$detaColor = consumoServicioGet($urlWS);

							$variacion['picture_ids'] = array();

							if(count($detaColor['colorProd']['ImagenesSelec']) > 0){
								$vueltas = 0;
								foreach ($detaColor['colorProd']['ImagenesSelec'] as $imagen) {
									if($vueltas > $cantFotos){
										break;
									}
									//$imagen = $detaColor['colorProd']['ImagenesSelec'][0];
									//$foto = $_SERVER['HTTP_HOST'] . $_SERVER['CONTEXT_PREFIX'] . str_replace('..', '', $rutaFotos) . $codProd . '/' . $colores['codColor'] . '/' . $imagen;
									$foto = $rutaFotos . $codProd . '/' . $colores['CodColor'] . '/' . $imagen;
									$dataFoto = array('source' => $foto);
									array_push($variacion['picture_ids'], $foto);
									//array_push($variacion['picture_ids'], $dataFoto);
									$vueltas++;
								}
							}
							//array_push($variacion['picture_ids'], "21150-MLA20204290696_112014");

							$variacionColor['id'] = $color['id_cat'];
							$variacionColor['value_id'] = $color['id'];
							array_push($variacion['attribute_combinations'], $variacionColor);

							//print_r($detaColor['colorProd']['ImagenesSelec']);
							//print_r($detaColor['colorProd']['tallas']);
							foreach ($detaColor['colorProd']['tallas'] as $tallas) {
								$encontroTalla = false;
								if($tallas['MostrarWeb'] == 'S'){
									if(trim(strtoupper($talla['desc'])) == trim(strtoupper($tallas['DescTalla']))){
										$encontroTalla = true;
										$urlWS = $WS . 'service=productoservices&metodo=ObtenerProductosPorColor&p_cod_prod=' . $codProd . '&p_cod_color=' . $colores['CodColor'] . '&p_cod_talla=' . $tallas['CodTalla'];
										$cantProd = consumoServicioGet($urlWS);
										//print_r('urlCant: ' . $urlWS);
										$variacion['available_quantity'] = $cantProd['total'];

										$variacionTalla['id'] = $talla['id_cat'];
										$variacionTalla['value_id'] = $talla['id'];
										array_push($variacion['attribute_combinations'], $variacionTalla);
										array_push($variaciones, $variacion);
									}
								}
							}

							if(!$encontroTalla){
								foreach ($detaColor['colorProd']['tallas'] as $tallas) {
									if($tallas['MostrarWeb'] == 'S'){
										$urlWS = $WS . 'service=productoservices&metodo=ObtenerProductosPorColor&p_cod_prod=' . $codProd . '&p_cod_color=' . $colores['CodColor'] . '&p_cod_talla=' . $tallas['CodTalla'];
										//print_r('urlCant: ' . $urlWS);
										$cantProd = consumoServicioGet($urlWS);

										$variacion['available_quantity'] = $cantProd['total'];

										$variacionTalla['id'] = $talla['id_cat'];
										$variacionTalla['value_name'] = $tallas['DescTalla'];
										array_push($variacion['attribute_combinations'], $variacionTalla);
										array_push($variaciones, $variacion);
									}
								}										
							}
						}
					}
				}

			}else{
				//no existen variaciones de talla para la categoria
				$urlWS = $WS . 'service=productoservices&metodo=ObtenerDetalleProducto&p_cod_prod=' . $codProd;
				$detaProd = consumoServicioGet($urlWS);
				$variaciones = array();

				$cantColores = 0;
				foreach ($detaProd['producto']['ColoresProducto'] as $colores) {
					$cantColores++;
				}

				if($cantColores == 0){
					$cantColores = 1;
				}

				$cantFotos = floor(10/$cantColores);

				foreach ($detaProd['producto']['ColoresProducto'] as $colores) {
					$variacion['price'] = $precio;
					$variacion['attribute_combinations'] = array();

					foreach ($colorPrimario as $color) {
						//if(trim(strtoupper($color['desc'])) == trim(strtoupper($colores['DescColor']))){
						if(trim(strtoupper($color['desc'])) == trim(strtoupper($colores['DescWeb']))){
							$urlWS = $WS . 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod=' . $codProd . '&p_color=' . $colores['CodColor'];
							$detaColor = consumoServicioGet($urlWS);

							$variacion['picture_ids'] = array();

							if(count($detaColor['colorProd']['ImagenesSelec']) > 0){
								$vueltas = 0;
								foreach ($detaColor['colorProd']['ImagenesSelec'] as $imagen) {
									if($vueltas > $cantFotos){
										break;
									}
									//$imagen = $detaColor['colorProd']['ImagenesSelec'][0];
									//$foto = $_SERVER['HTTP_HOST'] . $_SERVER['CONTEXT_PREFIX'] . str_replace('..', '', $rutaFotos) . $codProd . '/' . $colores['codColor'] . '/' . $imagen;
									$foto = $rutaFotos . $codProd . '/' . $colores['CodColor'] . '/' . $imagen;
									$dataFoto = array('source' => $foto);
									array_push($variacion['picture_ids'], $foto);
									//array_push($variacion['picture_ids'], $dataFoto);
									$vueltas++;
								}
							}
							//array_push($variacion['picture_ids'], "21150-MLA20204290696_112014");

							$variacionColor['id'] = $color['id_cat'];
							$variacionColor['value_id'] = $color['id'];
							array_push($variacion['attribute_combinations'], $variacionColor);

							foreach ($detaColor['colorProd']['tallas'] as $tallas) {
								if($tallas['MostrarWeb'] == 'S'){
									$urlWS = $WS . 'service=productoservices&metodo=ObtenerProductosPorColor&p_cod_prod=' . $codProd . '&p_cod_color=' . $colores['CodColor'] . '&p_cod_talla=' . $tallas['CodTalla'];
									$cantProd = consumoServicioGet($urlWS);
									print_r('urlCant: ' . $urlWS);
									$variacion['available_quantity'] = $cantProd['total'];

									$variacionTalla['id'] = $talla['id_cat'];
									$variacionTalla['value_name'] = $tallas['DescTalla'];
									array_push($variacion['attribute_combinations'], $variacionTalla);
									array_push($variaciones, $variacion);
								}
							}	
						}
					}
				}
			}
		}else{
			//print_r('Sin Variaciones </br>');
			//la categoria no tiene variaciones, se crean variaciones personalizadas
			$urlWS = $WS . 'service=productoservices&metodo=ObtenerDetalleProducto&p_cod_prod=' . $codProd;
			$detaProd = consumoServicioGet($urlWS);
			$variaciones = array();

			$cantColores = 0;
			foreach ($detaProd['producto']['ColoresProducto'] as $colores) {
				$cantColores++;
			}

			if($cantColores == 0){
				$cantColores = 1;
			}

			$cantFotos = floor(10/$cantColores);

			foreach ($detaProd['producto']['ColoresProducto'] as $colores) {
				$variacion['price'] = $precio;
				
				$urlWS = $WS . 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod=' . $codProd . '&p_color=' . $colores['CodColor'];
				$detaColor = consumoServicioGet($urlWS);
				//print_r($detaColor);

				$variacion['picture_ids'] = array();

				if(count($detaColor['colorProd']['ImagenesSelec']) > 0){
					$vueltas = 0;
					foreach ($detaColor['colorProd']['ImagenesSelec'] as $imagen) {
						if($vueltas > $cantFotos){
							break;
						}
						//$imagen = $detaColor['colorProd']['ImagenesSelec'][0];
						//$foto = $_SERVER['HTTP_HOST'] . $_SERVER['CONTEXT_PREFIX'] . str_replace('..', '', $rutaFotos) . $codProd . '/' . $colores['codColor'] . '/' . $imagen;
						$foto = $rutaFotos . $codProd . '/' . $colores['CodColor'] . '/' . $imagen;
						$dataFoto = array('source' => $foto);
						array_push($variacion['picture_ids'], $foto);
						//array_push($variacion['picture_ids'], $dataFoto);
						$vueltas++;
					}
				}
				//array_push($variacion['picture_ids'], "21150-MLA20204290696_112014");

				foreach ($detaColor['colorProd']['tallas'] as $tallas) {
					if($tallas['MostrarWeb'] == 'S'){
						$urlWS = $WS . 'service=productoservices&metodo=ObtenerProductosPorColor&p_cod_prod=' . $codProd . '&p_cod_color=' . $colores['CodColor'] . '&p_cod_talla=' . $tallas['CodTalla'];
						$cantProd = consumoServicioGet($urlWS);

						$variacion['available_quantity'] = $cantProd['total'];

						$variacion['attribute_combinations'] = array();
						$variacionColorTalla['name'] = 'Color - Talla';
						//$variacionColorTalla['value_name'] = $colores['DescColor'] . ' - ' . $tallas['DescTalla'];
						$variacionColorTalla['value_name'] = $colores['DescWeb'] . ' - ' . $tallas['DescTalla'];
						array_push($variacion['attribute_combinations'], $variacionColorTalla);

						array_push($variaciones, $variacion);
					}
				}
			}
		}

		return $variaciones;
	}

	function erroresML($errorML){

		$mensaje = 'Se ha producido un Problema con la publicaci&oacute;n en Mercado Libre:';

		if(strtolower($errorML['error']) == strtolower('item.price.invalid')){
			$mensaje .= '</br>&emsp;&emsp;*No est&aacute; permitido un Item sin precio en esta categor&iacute;a';
		}else if(strtolower($errorML['error']) == strtolower('item.buying_mode.invalid')){
			$mensaje .= '</br>&emsp;&emsp;*El m&eacute;todo de Pago <b>Mercado Pago</b> no es soportado en esta categor&iacute;a';
		}else{
			foreach ($errorML['cause'] as $causa) {
				if(strtolower($causa['code']) == strtolower('item.pictures.variation.quantity')){
					$mensaje .= '</br>&emsp;&emsp;*No existen im&aacute;genes para alguna variaci&oacute;n. Cada variaci&oacute;n debe tener entre 1 y 6 im&aacute;genes.';
				}else if(strtolower($causa['code']) == strtolower('item.variations.available_quantity.invalid')){
					$mensaje .= '</br>&emsp;&emsp;*La cantidad disponible para cada varicaci&oacute;n debe ser Mayor que 0.';
				}else if(strtolower($causa['code']) == strtolower('item.available_quantity.invalid')){
					$mensaje .= '</br>&emsp;&emsp;*La cantidad disponible para cada varicaci&oacute;n debe ser Mayor que 0.';
					$mensaje .= '</br>&emsp;&emsp;*Es posible que las tallas no esten configuradas para mostrarse v&iacute;a Web.';
				}else if(strtolower($causa['code']) == strtolower('item.listing_type_id.unavailable')){
					$mensaje .= '</br>&emsp;&emsp;*El plan seleccionado no est&aacute; disponible para la categor&iacute;a seleccionada, es posible que tu cuenta (ML) no tenga habilitado este plan.';
				}else if(strtolower($causa['code']) == strtolower('item.attributes.missing_required')){
					if(strpos($causa['message'], '11000, 103000') != false || strpos($causa['message'], '11000') != false || strpos($causa['message'], '103000') != false){
						if(strpos($causa['message'], '11000, 103000') != false){
							$mensaje .= '</br>&emsp;&emsp;*Las varicaciones de Color y/o Talla son requeridas, es posible que no hubiese coincidencia con Mercado Libre.';
						}else if(strpos($causa['message'], '11000') != false){
							$mensaje .= '</br>&emsp;&emsp;*La varicaci&oacute;n de Color es requerido, es posible que no hubiese coincidencia con Mercado Libre.';
						}else if(strpos($causa['message'], '103000') != false){
							$mensaje .= '</br>&emsp;&emsp;*La varicaci&oacute;n de Talla es requerida, es posible que no hubiese coincidencia con Mercado Libre.';
						}
					}
				}else if (strtolower($causa['code']) == strtolower('item.pictures.picture_not_found')) {
					$mensaje .= '</br>&emsp;&emsp;*Es posible que el art&iacute;culo no tenga im&aacute;genes disponibles para la publicaci&oacute;n.';
				}

			}
		}

		return $mensaje;
	} 
?>