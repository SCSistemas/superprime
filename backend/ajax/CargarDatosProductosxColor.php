<?php 
	include '../config/definitions.php';

	session_start();
	$salir = 0;
	$i = 0;
	while (($salir ==0) &&  ($_POST['CodProAnt']!='')){
		if ($_SESSION['product']['producto']['ColoresProducto'][$i]['CodColor'] == $_POST['CodProAnt']){
			$_SESSION['product']['producto']['ColoresProducto'][$i]['ColorPrinc'] = $_POST['ColorPri'];
			$_SESSION['product']['producto']['ColoresProducto'][$i]['DescWeb'] = $_POST['DesColorWeb'];
			if ($_POST['CheckSec'])
				$_SESSION['product']['producto']['ColoresProducto'][$i]['ColorSec'] = $_POST['ColorSec'];
			else
				$_SESSION['product']['producto']['ColoresProducto'][$i]['ColorSec'] = '';
			
			if ($_POST['CheckWeb'] == 1)
				$_SESSION['product']['producto']['ColoresProducto'][$i]['MostrarWeb'] = 'S';
			else
				$_SESSION['product']['producto']['ColoresProducto'][$i]['MostrarWeb'] = 'N';
			$salir = 1;
		}
		$i = $i + 1;;
	}
	if ($_SESSION['product'][$_POST['CodProAnt']]){

		// guardar precios
		
		$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['Precio'] = $_POST['PrecioWeb'];
		if ($_POST['PorcDesc'] > 0)
		{
			$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['PorcDesc'] = $_POST['PorcDesc'];
			
			//number_format($numero, 2, ',', '.')
			$precio = str_replace('.', '',$_POST['PrecioWeb']);
			$precio = str_replace(',','.',$precio);
			$precio = floatval($precio);

			$porDes = str_replace('.', '',$_POST['PorcDesc']);
			$porDes = str_replace(',','.',$porDes);
			$porDes = floatval($porDes);

			$montD = $precio  - (($precio /100) * $porDes);
			$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['MontoDesc'] = number_format($montD, 2, ',', '.');

			$iva = $montD * 0.12;
			$total = $montD + $iva;
			$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['Total'] = number_format($total, 2, ',', '.');
			$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['MontoIVA'] = number_format($iva, 2, ',', '.');

		}
		else
			{
				$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['MontoDesc'] = 0;
				$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['PorcDesc'] = 0;
				$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['Total'] = $_POST['PrecioWeb'];
				$precio = str_replace('.', '',$_POST['PrecioWeb']);
				$precio = str_replace(',','.',$precio);
				$precio = floatval($precio); 
				$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['PreciosWeb']['MontoIVA'] = number_format(($precio / 100) * 12, 2, ',', '.');

			}
		


		//guardar selección de fotos
        $imagenes = json_decode($_POST['imagenes']);

		$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['ImagenesSelec'] = $imagenes;


		//Inicio actualizar tallas seleccionadas

		$t = json_decode($_POST['tallasA']);
		for ($i=0; $i < count($_SESSION['product'][$_POST['CodProAnt']]['colorProd']['tallas']); $i++) 
		{ 
			$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['tallas'][$i]['MostrarWeb'] = 'N';

			foreach ($t as $value) 
			{
				if ($_SESSION['product'][$_POST['CodProAnt']]['colorProd']['tallas'][$i]['CodTalla'] == $value)
				{
					$_SESSION['product'][$_POST['CodProAnt']]['colorProd']['tallas'][$i]['MostrarWeb'] = 'S';
				}
			}
		}
		//Fin actualizar tallas seleccionadas

	}

	if (!$_SESSION['product'][$_POST['CodProAct']]){
	
			$url = $urlWS. 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod='. $_POST['CodProducto'].'&p_color='.$_POST['CodProAct'];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$resultDataProduct = curl_exec($ch);
			//print_r($resultDataProduct) ;

			curl_close($ch);

			$prueba = json_encode($_SESSION['product']);

			$prueba = str_replace('á','&aacute;',$prueba);
			$prueba = str_replace('é','&eacute;',$prueba);
			$prueba = str_replace('í','&iacute;',$prueba);
			$prueba = str_replace('ó','&oacute;',$prueba);
			$prueba = str_replace('ú','&uacute;',$prueba);
			$prueba = str_replace('ñ','&ntilde;',$prueba);

			$_SESSION['product'] = json_decode($prueba,true);
			
			//array_push($_SESSION['product'][$_POST['CodProAct']],json_decode($resultDataProduct,true));
			$_SESSION['product'][$_POST['CodProAct']] = json_decode($resultDataProduct,true);
	}

	//echo $resultDataProduct;
	echo json_encode($_SESSION['product'])
	//echo json_encode($_POST);
	//print_r($_SESSION['product']) ;
	
?>