<?php
	include '../config/definitions.php';
	$idCategoria = $_GET['idCategoria'];
	$urlcategorias = 'https://api.mercadolibre.com/categories/' . $idCategoria;
	//$respCategorias = file_get_contents($urlcategorias);
	//$respCategorias = json_decode($respCategorias, true);
	$respCategorias = consumoServicioGet($urlcategorias);
	$path = "";
	$resp = array();

	foreach ($respCategorias['path_from_root'] as $value) {
		if($path == ""){
			$path = $value['id'];
		}else{
			$path .= ";".$value['id'];
		}
	}

	array_push($resp, $path);

	foreach ($respCategorias['children_categories'] as $value) {
		array_push($resp, ($value['id'].";".$value['name']));		
	}

	echo(json_encode($resp));
?>