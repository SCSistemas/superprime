/**
 * Created by Admin on 11/01/16.
 */
$(document).ready(function () {


    $('#Tinventario').dataTable( {
        "language": {
            "decimal":        "",
            "emptyTable":     " <i class='fa fa-warning'></i> No hay Datos en la tabla",
            "info":           "Mostrando _START_ to _END_ de _TOTAL_ Registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 Registros",
            "infoFiltered":   "(filtered from _MAX_ total Registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ Registros",
            "loadingRecords": "Loading...",
            "processing":     "Processing...",
            "search":         "Buscar:",
            "zeroRecords":    "No matching records found",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }
    } )
});