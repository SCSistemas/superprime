<script src="core/modules/console/view/inventario/inventario.js"></script>
<div class="row">
    <div class="col-md-12" id="pedidos">
        <h1>Inventario</h1>
        <br>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <table id="Tinventario" class="display table" width="100%">
                <thead>
                <th>Código Tienda</th>
                <th>Código Producto</th>
                <th>Cantidad</th>
                </thead>
                <?php
                $productos = ProductoData::ObtenerInventario();
                if (count($productos) > 0) {
                ?>

                <tbody>
                <?php
                foreach ($productos as $producto) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $producto->CodTienda; ?></td>
                        <td align="center"><?php echo $producto->CodProducto; ?></td>
                        <td align="center"><?php echo $producto->Cantidad; ?></td>
                    </tr>
                    <?php

                }?>
                </tbody>
            </table>
            <?php
            } ?>

        </div>
    </div>
</div>
