<?php
global $lang;
$services=new GeneralServices();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        switch($_GET['metodo']) {
            case 'ObtenerColores' :
                $services->ObtenerColores();
            break;
            case 'ObtenerGeneros' :
                $services-> ObtenerGeneros();
            break;
            case 'ObtenerMarcas' :
                $services-> ObtenerMarcas();
            break;
            case 'ObtenerTallas' :
                $services-> ObtenerTallas();
            break;
            case 'ObtenerTipoProducto' :
                $services-> ObtenerTipoProducto();
            break;
            case 'ObtenerTotalesDashbord' :
                $services-> ObtenerTotales();
            break;
            case 'ObtenerDestinatarios' :
                $services-> ObtenerDestinatarios($_GET);
            break;
            case 'ObtenerActividadReciente' :
                $services-> ObtenerActividadReciente($_GET);
            break;
            case 'ObtenerDetalleColor' :
                $services-> ObtenerDetalleColor($_GET);
            break;
            case 'InsertarJetes' :
                $services-> InsertarJetes();
            break;
            case 'ObtenerEstados' :
                $services-> ObtenerEstados();
            break;
            case 'ObtenerCiudades' :
                $services-> ObtenerCiudades($_GET);
            break;
            case 'ObtenerMunicipios' :
                $services-> ObtenerMunicipios($_GET);
            break;
            case 'ObtenerParroquias' :
                $services-> ObtenerParroquias($_GET);
            break;
            case 'consultarClaveValor' :
                $services-> consultarClaveValor($_GET);
            break;
        }
}else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'insertarClaveValor' :
            $services->insertarClaveValor($body);
            break;
        case 'actualizarClaveValor' :
            $services->actualizarClaveValor($body);
            break;
        case 'actualizarInventarioML' :
            $services->actualizarInventarioML($body);
            break;
        case 'gestionarOrdenML' :
            $services->gestionarOrdenML($body);
            break;
    }



}

?>