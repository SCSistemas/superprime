<?php
global $lang;
$services=new UserServices();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        switch($_GET['metodo']) {
            case 'RecuperarContrasena' :
                $services->RecuperarContrasena($_GET,$lang);
            break;
            case 'ValidarToken':
                $services->ValidarToken($_GET,$lang);
            break;
            case 'ObtenerDatosPerfil':
                $services->ObtenerDatosPerfil($_GET);
            break;
            case 'ObtenerDirecciones':
                $services->ObtenerDirecciones($_GET);
            break;
            case 'ObtenerMenuUsuarioAdmin' :
                $services-> ObtenerMenuUsuarioAdmin($_GET);
                break;
            case 'ObtenerListaDeseos' :
                $services-> ObtenerListaDeseos($_GET);
                break;
            case 'ValidaAliasDireccion':
                $services-> ValidaAliasDireccion($_GET,$lang);
                break;
            case 'ObtenerListaReservas' :
                $services-> ObtenerListaReservas($_GET);
                break;
            case 'ObtenerListaCompras' :
                $services-> ObtenerListaCompras($_GET);
                break;
            case 'ObtenerDatosUsuarioExpress' :
                $services-> ObtenerDatosUsuarioExpress($_GET);
                break;
            case 'ObtenerCantidadCarrito' :
                $services-> ObtenerCantidadCarrito($_GET);
                break;
            case 'ObtenerCarrito' :
                $services-> ObtenerCarrito($_GET);
                break;
            case 'ObtenerFavoritoUsuario' :
                $services-> ObtenerFavoritoUsuario($_GET);
                break;

        }
}
else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'Login' :
            $services->Login($body,$lang);
            break;
        case 'LoginBackend' :
            $services->LoginBackend($body,$lang);
            break;
        case 'RegistrarUsuario' :
            $services->InsertarUsuario($body,$lang);
            break;
        case 'RegistrarPersona' :
            $services->InsertarPersona($body,$lang);
            break;
        case 'CambiarContrasena' :
            $services->CambiarContrasena($body,$lang);
            break;
        case 'CambiarContrasenaPerfil' :
            $services->CambiarContrasenaPerfil($body,$lang);
            break;
        case 'ActualizarDatosPerfil' :
            $services->ActualizaPersona($body,$lang);
            break;
        case 'InsertarDeseo' :
            $services->InsertarDeseo($body,$lang);
            break;
        case 'EliminarDeseo' :
            $services->EliminarDeseo($body,$lang);
            break;
        case 'RegistrarDireccion' :
            $services->InsertarDireccion($body, $lang);
            break;
        case 'EliminarDireccion' :
            $services->EliminarDireccion($body, $lang);
            break; 
        case 'ActualizarDireccion' :
            $services->ActualizaDireccion($body, $lang);
            break;
        case 'ActualizarPredeterminada' :
            $services-> ActualizaDireccionPredeterminada($body, $lang);
            break;
        case 'InsertarDireccionML' :
            $services-> InsertarDireccionML($body, $lang);
            break;
        case 'InsertarEnCarro' :
            $services-> InsertarEnCarro($body, $lang);
            break;
        case 'BorrarEnCarro' :
            $services-> BorrarEnCarro($body, $lang);
            break;
        case 'EditarEnCarro' :
            $services-> EditarEnCarro($body, $lang);
            break;
}

}


?>