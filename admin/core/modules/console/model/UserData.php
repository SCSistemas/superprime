<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/6/2016
 * Time: 8:32 PM
 */
class UserData
{
    public function UserData(){


}
public static function Login($p_usuario, $p_password, $admin)
{
    $sql = "CALL Login('$p_usuario','$p_password', $admin)";
    $query = Executor::doit($sql);
    return Model::one($query[0], new UserData());
}
    public static function ObtenerIdUsuario()
{
    $sql = "CALL ObtenerIdUsuario()";
    $query = Executor::doit($sql);
    return Model::one($query[0], new UserData());
}

public static function InsertarUsuario($id_usuario,$p_usuario, $p_password)
    {
        $sql = "CALL  InsertarUsuario($id_usuario,'$p_usuario','$p_password')";
        $query = Executor::doit($sql);
        if(is_a($query[0],'mysqli_result')==1) {
            return Model::one($query[0], new UserData());
        }
    }
    public static function InsertarPersona($p_tipoid,$p_numid,$p_nombre_persona,$p_apellido,$p_fec_nac,$p_sexo,$p_id_user,$p_persona_c)
    {
        $sql = "CALL  InsertarPersona('$p_tipoid',$p_numid,'$p_nombre_persona','$p_apellido','$p_fec_nac','$p_sexo',$p_id_user,'$p_persona_c')";
         $query = Executor::doit($sql);
        if(is_a($query[0],'mysqli_result')==1) {
            return Model::one($query[0], new UserData());
        }
    }
    public static function InsertarDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$p_iduser,$p_alias,$p_predeterminada,$longitudu,$latitudu)
    {
        $sql = "CALL  InsertarDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,'$p_postal','$p_calle','$p_calledos','$p_tipovi','$p_nombre','$p_piso','$p_apto','$p_telcel','$p_telhab','$p_telofi',$p_iduser,'$p_alias','$p_predeterminada',$longitudu,$latitudu)";
        Executor::doit($sql);
    }

    public static  function ObtenerUsuario($usuario){

        $sql = "CALL  ObtenerUsuario('$usuario')";
        $query = Executor::doit($sql);
        return Model::one($query[0],new UserData());
    }
    public static  function EliminarUsuario($p_id_user){

        $sql = "CALL EliminarUsuario($p_id_user)";
        Executor::doit($sql);
    }

    public static  function ObtenerPersonaUsuario($iduser){

        $sql = "CALL  ObtenerPersonaUsuario($iduser)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new UserData());
    }

    public static function CambiarContrasena($p_id_user,$p_contrasena)
    {
        $sql = "CALL CambiarContrasena($p_id_user,'$p_contrasena')";
        Executor::doit($sql);
    }

    public static function InsertarToken($p_codigo)
    {
        $sql = "CALL   InsertarToken('$p_codigo',now())";
        Executor::doit($sql);
    }
    public static  function ValidarToken($p_codigo){
        $sql = "CALL   ValidarToken('$p_codigo')";
        $query = Executor::doit($sql);
        return Model::one($query[0],new UserData());
    }
    public static  function  UsarToken($p_codigo){
        $sql = "CALL    UsarToken('$p_codigo')";
         Executor::doit($sql);

    }
    public static  function ObtenerUsuarioPorId($p_id_user){

        $sql = "CALL  ObtenerUsuarioPorId($p_id_user)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new UserData());
    }
    public static  function  ObtenerDireccionesUsuario($p_id_user,$p_desde,$p_hasta){
        $sql = "CALL   ObtenerDireccionesUsuario($p_id_user,$p_desde,$p_hasta)";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function  ContarDirecciones($p_id_user)
    {
        $sql = "CALL ContarDirecciones($p_id_user)";
        $query = Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }

    public static function ActualizaPersona($p_tipoid,$p_numid,$p_nombre_persona,$p_apellido,$p_fec_nac,$p_sexo,$p_persona_c,$p_imagen,$p_id_user)
    {
        $sql = "CALL  ActualizaPersona('$p_tipoid',$p_numid,'$p_nombre_persona','$p_apellido','$p_fec_nac','$p_sexo','$p_persona_c','$p_imagen',$p_id_user)";
        return Executor::doitDelorUpdate($sql);
    }
    public static function InsertarDeseo($p_id_user,$p_cod_producto)
    {
        $sql = "CALL InsertarDeseo($p_id_user,'$p_cod_producto')";
        Executor::doit($sql);
    }

    public static function EliminarDeseo($p_id_user,$p_cod_producto)
    {
        $sql = "CALL EliminarDeseo ($p_id_user,'$p_cod_producto')";
        Executor::doit($sql);
    }
    public static function EliminarDireccion($p_id_user,$p_id_direccion)
    {
        $sql = "CALL  EliminarDireccion($p_id_user,$p_id_direccion)";
        return Executor::doitDelorUpdate($sql);
    }

    public static function AcualizaDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$p_alias,$p_iduser,$p_id_direccion,$longitudu,$latitudu)
    {
        $sql = "CALL   ActualizaDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,'$p_postal','$p_calle','$p_calledos','$p_tipovi','$p_nombre','$p_piso','$p_apto','$p_telcel','$p_telhab','$p_telofi','$p_alias',$p_iduser,$p_id_direccion,$longitudu,$latitudu)";
        return Executor::doit($sql);
    }
    public static function  ActualizarDireccionPredeterminada($p_iduser,$p_id_direccion)
    {
        $sql = "CALL    ActualizarDireccionPredeterminada($p_iduser,$p_id_direccion)";
        Executor::doit($sql);
    }

    public static function  ObtenerMenusUsuario($p_iduser)
    {
        $sql = "CALL    ObtenerMenusUsuario($p_iduser)";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    } 
   

    public static function  ObtenerSubMenus($p_idmenu)
    {
        $sql = "CALL    ObtenerSubMenus($p_idmenu)";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }

    public static  function ValidaAliasDireccion($p_id_user,$p_id_direccion,$p_alias){
        $sql = "CALL ValidaAliasDireccion($p_id_user,$p_id_direccion,'$p_alias')";
        $query = Executor::doit($sql);
        return Model::one($query[0],new UserData());
    }
    
    public static function  ObtenerListaDeseos($p_iduser)
    {
        $sql = "CALL ObtenerListaDeseos($p_iduser)";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function ObtenerListaReservas($p_iduser,$p_desde,$p_hasta)
    {
        $sql = "CALL ObtenerListaReservas($p_iduser,'$p_desde','$p_hasta')";
        $query = Executor::doit($sql);

        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerListaCompras($p_iduser,$p_desde,$p_hasta)
    {
        $sql = "CALL  ObtenerListaCompras($p_iduser,'$p_desde','$p_hasta')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function InsertarDireccionML($p_cod_pedido,$p_nombre_persona,$p_correo,$p_tipoid,$p_numid,$p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$p_tipodi,$longitudu,$latitudu,$ordenML)
    {
        $sql = "CALL  InsertarDireccionML('$p_cod_pedido','$p_nombre_persona','$p_correo','$p_tipoid',$p_numid,$p_estado,$p_ciudad,$p_municipio,$p_parroquia,'$p_postal','$p_calle','$p_calledos','$p_tipovi','$p_nombre','$p_piso','$p_apto','$p_telcel','$p_telhab','$p_telofi',$p_tipodi,$longitudu,$latitudu,'$ordenML')";
        Executor::doit($sql);
    }

    public static  function  ObtenerDireccionPre($p_id_user){
        $sql = "CALL ObtenerDireccionPre($p_id_user)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new GeneralData());
    }
    public static  function   ObtenerDireccionEnvioML($p_id_pedido){
        $sql = "CALL  ObtenerDireccionEnvioML($p_id_pedido)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new GeneralData());
    }

    public static function  InsertarEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_cantidad,$p_id_user)
    {
        $sql = "CALL InsertarEnCarro('$p_cod_producto','$p_cod_color','$p_cod_talla',$p_cantidad,$p_id_user)";
        Executor::doit($sql);
    }
    public static function  BorrarEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_cantidad,$p_id_user)
    {
        $sql = "CALL BorrarEnCarro('$p_cod_producto','$p_cod_color','$p_cod_talla',$p_cantidad,$p_id_user)";
        Executor::doit($sql);
    }
    public static function EditarEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_cantidad,$p_id_user)
    {
        $sql = "CALL EditarEnCarro('$p_cod_producto','$p_cod_color','$p_cod_talla',$p_cantidad,$p_id_user)";
        Executor::doit($sql);
    }
    public static function  ActualizarEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_cantidad,$p_id_user)
    {
        $sql = "CALL ActualizarEnCarro('$p_cod_producto','$p_cod_color','$p_cod_talla',$p_cantidad,$p_id_user)";
        Executor::doit($sql);
    }

    public static function  ObtenerCarrito($p_iduser)
    {
        $sql = "CALL ObtenerCarrito($p_iduser)";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function ObtenerCantidadCarrito($p_iduser)
    {
        $sql = "CALL ObtenerCantidadCarrito($p_iduser)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new GeneralData());
    }

    public static  function ObtenerProductoEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_id_user){

        $sql = "CALL  ObtenerProductoEnCarro('$p_cod_producto','$p_cod_color','$p_cod_talla',$p_id_user)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new GeneralData());
    }

    public static function ObtenerFavoritoUsuario($p_cod_producto,$p_iduser)
    {
        $sql = "CALL ObtenerFavoritoUsuario('$p_cod_producto',$p_iduser)";
        $query = Executor::doit($sql);
        return Model::one($query[0],new GeneralData());
    }





}