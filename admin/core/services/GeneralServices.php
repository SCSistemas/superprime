<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18/5/2016
 * Time: 10:26 PM
 */
class GeneralServices
{
    public function ObtenerColores()
    {
        try {
            $colores = GeneralData::ObtenerColores();
            $posts['colores'] = $colores;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerGeneros()
    {
        try {
            $generos = GeneralData::ObtenerGeneros();
            $posts['generos'] = $generos;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerMarcas()
    {
        try {
            $marcas = GeneralData::ObtenerMarcas();
            $posts['marcas'] = $marcas;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerTallas()
    {
        try {
            $tallas = GeneralData::ObtenerTallas();
            $posts['tallas'] = $tallas;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerTipoProducto()
    {
        try {
            $t_productos = GeneralData::ObtenerTipoProducto();
            $posts['tipoproducto'] = $t_productos;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerTotales()
    {
        try {
            $t_productos = GeneralData::ObtenerTotales();
            $totales['Sincronizados'] = $t_productos[0]->Total;
            $totales['Publicados'] = $t_productos[1]->Total;
            $totales['MercadoLibre'] = $t_productos[2]->Total;
            $totales['SinFoto'] = $t_productos[3]->Total;
            $posts['totalesProductos'] = $totales;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDestinatarios($body)

    {
        try {
            $p_id_proceso = $body['p_id_proceso'] == '' ? 'null' : $body['p_id_proceso'];
            $posts = GeneralData::ObtenerDestinatarios($p_id_proceso);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerActividadReciente($body)

    {
        try {
            $p_cantidad = 5;
            $posts = GeneralData::ObtenerActividadReciente($p_cantidad);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDetalleColor($body)

    {
        try {
            $p_cod_prod = $body['p_cod_prod'];
            $p_color = $body['p_color'];
            $colorProd = null;
            $preciosap = null;
            $precioweb = null;
            $imagenes = GeneralData::ObtenerImagenesProducto($p_cod_prod, $p_color);
            $aimagenes = array();
            foreach ($imagenes as $imagen) {
                $aimagenes[] = $imagen->Ruta;
            }
            $colorProd->ImagenesSelec = $aimagenes;
            $colorProd->tallas = GeneralData::ObtenerTallasColor($p_cod_prod, $p_color);
            $precios = GeneralData::ObtenerPreciosProducto($p_cod_prod, $p_color);
            $preciosap->Precio = $precios->PrecioSap;
            $preciosap->MontoIVA = $precios->IvaSap;
            $preciosap->Total = $precios->TotalSap;
            $colorProd->PreciosSap = $preciosap;

            $precioweb->Precio = $precios->PrecioWeb;
            $precioweb->MontoIVA = $precios->IvaWeb;
            $precioweb->Total = $precios->TotalWeb;
            $precioweb->PorcDesc = $precios->PorcDesc;
            $precioweb->MontoDesc = $precios->MontoDesc;
            $colorProd->PreciosWeb = $precioweb;
            $posts['colorProd'] = $colorProd;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function InsertarJetes()

    {
        try {
            $tiempo_inicio = microtime(true);
            $estados = Util::ObtenerEstadosJetes();
            $i=1;
            foreach ($estados as $estado){
                GeneralData::InsertarEstado($i,$estado->_id,$estado->nombre);
                $ciudades=Util::ObtenerCiudadesJetes($estado->_id);
                $j=1;
                foreach ($ciudades as $ciudad){
                    GeneralData::InsertarCiudad($j,$i,$ciudad->_id,$ciudad->nombre);
                    $municipios=Util::ObtenerMunicipiosJetes($estado->_id,$ciudad->_id);
                    $k=1;
                    foreach ($municipios as $municipio){
                        GeneralData::InsertarMunicipio($i,$j,$k,$municipio->_id,$municipio->nombre);
                        $parroquias=Util::ObtenerParroquiasJetes($estado->_id,$ciudad->_id,$municipio->_id);
                        $l=1;
                        foreach ($parroquias as $parroquia){
                            GeneralData::InsertarParroquia($i,$j,$k,$l,$parroquia->_id,$parroquia->nombre);
                            $l++;
                        }

                        $k++;
                    }

                    $j++;
                }
                $i++;
            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $tiempo_fin = microtime(true);
        $tiempo = bcsub($tiempo_fin, $tiempo_inicio, 4);
        $posts='Listo en: '.$tiempo;
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerEstados()

    {
        try {
            $posts = GeneralData::ObtenerEstados(0);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerCiudades($body)

    {
        try {
            $p_cod_estado = $body['p_cod_estado'];
            $posts = GeneralData::ObtenerCiudades($p_cod_estado,0);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerMunicipios($body)

    {
        try {
            $p_cod_estado = $body['p_cod_estado'];
            $p_cod_ciudad = $body['p_cod_ciudad'];
            $posts = GeneralData::ObtenerMunicipios($p_cod_estado,$p_cod_ciudad,0);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerParroquias($body){
        try {
            $p_cod_estado = $body['p_cod_estado'];
            $p_cod_ciudad = $body['p_cod_ciudad'];
            $p_cod_municipio = $body['p_cod_municipio'];
            $posts = GeneralData::ObtenerParroquias($p_cod_estado,$p_cod_ciudad,$p_cod_municipio);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function insertarClaveValor($body){
        try {

            $insertResult = GeneralData::insertarClaveValor($body['p_clave'], $body['p_valor']);
                    
            $posts = array('success' => '1');
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function actualizarClaveValor($body){
        try {

            $insertResult = GeneralData::actualizarClaveValor($body['p_clave'], $body['p_valor']);
                    
            $posts = array('success' => '1');
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function consultarClaveValor($body){
        try {

            $selectResult = GeneralData::consultarClaveValor($body['p_clave']);
                            
            $posts = array('success' => '1', 
                           'valor' => $selectResult->valor);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function actualizarInventarioML($body){
        try {

            $variable = array('ml_id','ml_secretKey', 'ml_authCode', 'ml_token', 'ml_fecToken', 'ml_expires', 'ml_redirectUri');
            foreach ($variable as $key ) {
                $resp = GeneralData::consultarClaveValor($key);
                $dataML[$key] = $resp->valor;
            }
            
            $fecExpira = $dataML['ml_fecToken'] + $dataML['ml_expires'];
            
            if(time() > ($fecExpira) || ($fecExpira - time()) < 300){//si el token expiro o su tiempo de vida es menor de 5 min(300 Seg) se renueva
                $tokenPre = $dataML['ml_token'];
        
                $tokenParam = array('grant_type' => 'refresh_token',
                                    'client_id' => $dataML['ml_id'],
                                    'client_secret' => $dataML['ml_secretKey'],
                                    'refresh_token' => $dataML['ml_authCode']);

                $service_url = 'https://api.mercadolibre.com/oauth/token';
                $curl = curl_init($service_url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $tokenParam);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $responseToken = curl_exec($curl);
                curl_close($curl);

                if($responseToken != null){
                    $responseToken = json_decode($responseToken,true);

                    if(!isset($responseToken['message'])){
                        $dataML['ml_authCode'] = $responseToken['refresh_token'];
                        $dataML['ml_fecToken'] = (time() - 30);
                        $dataML['ml_expires'] = $responseToken['expires_in'];
                        $dataML['ml_token'] = $responseToken['access_token'];

                        GeneralData::actualizarClaveValor('ml_authCode', $dataML['ml_authCode']);
                        GeneralData::actualizarClaveValor('ml_fecToken', $dataML['ml_fecToken']);
                        GeneralData::actualizarClaveValor('ml_expires', $dataML['ml_expires']);
                        GeneralData::actualizarClaveValor('ml_token', $dataML['ml_token']);
                    }
                }

                $tokenPost = $dataML['ml_token']; 

                if($tokenPre == $tokenPost){//hubo una falla en la renovacion del token
                    $dataML['error'] = true;
                }else{
                    $dataML['error'] = false;
                }
            }else{//no hace falta la renovacion ya que el token aun esta activo
                $dataML['error'] = false;
            }   

            if(!$dataML['error']){

                $prodData = ProductoData::consultarPublicacionProducto($body['p_codProducto']); 

                $dataPublicacion = $prodData[0];

                //$WS = $urlWS . 'service=productoservices&metodo=ObtenerDetalleProducto&p_cod_prod=' . $body['p_codProducto'];
                //$detaProd = consumoServicioGet($WS);
                //$detaProd = array();
                $detaProd = ProductoData::ObtenerColoresProducto($body['p_codProducto']);

                foreach ($detaProd as $colores) {
                    if($body['p_codColor'] == $colores->CodColor){
                        //$descColor = $colores->DescColor;
                        $descColor = $colores->DescWeb;
                    }
                }

                //$WS = $urlWS . 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod=' . $body['p_codProducto'] . '&p_color=' . $body['p_codColor'];
                //$detaColorProd = consumoServicioGet($WS);

                $detaColorProd = GeneralData::ObtenerTallasColor($body['p_codProducto'], $body['p_codColor']);

                foreach ($detaColorProd as $tallas) {
                    if($body['p_codTalla'] == $tallas->CodTalla){
                        $descTalla = $tallas->DescTalla;
                    }
                }
                
                $WS = 'https://api.mercadolibre.com/items/' . $dataPublicacion->idPubML . '/variations';
                /*$detaML = file_get_contents($WS);
                $detaML = json_decode($detaML, true);*/
                $curl = curl_init($WS);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);   
                $detaML = curl_exec($curl);
                curl_close($curl);
                $detaML = json_decode($detaML, true);
                
                $idVariacion = 0;
                foreach ($detaML as $value) {
                    $b_color = 0;
                    $b_talla = 0;

                    foreach ($value['attribute_combinations'] as $variaciones) {
                        if(trim(strtoupper($variaciones['name'])) == trim(strtoupper('Color Primario'))){
                            if(trim(strtoupper($variaciones['value_name'])) == trim(strtoupper($descColor))){
                                $b_color = 1;
                            }
                        }else if(trim(strtoupper($variaciones['name'])) == trim(strtoupper('Talla'))){
                            if(trim(strtoupper($variaciones['value_name'])) == trim(strtoupper($descTalla))){
                                $b_talla = 1;
                            }
                        }else if(trim(strtoupper($variaciones['name'])) == trim(strtoupper('Color - Talla'))){
                            if(trim(strtoupper($variaciones['value_name'])) == trim(strtoupper($descColor . ' - ' . $descTalla))){
                                $b_color = 1;
                                $b_talla = 1;
                            }
                        }

                        if($b_color == 1 && $b_talla = 1){
                            $idVariacion = $value['id'];
                        }
                    }
                }

                $parametros = array('available_quantity' => $body['p_cantidad']);
                $WS = 'https://api.mercadolibre.com/items/' . $dataPublicacion->idPubML . '/variations/'. $idVariacion . '?access_token=' . $dataML['ml_token'];

                $posts = $WS;
                $parametros = json_encode($parametros);

                $curl = curl_init($WS);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $responseML = curl_exec($curl);
                curl_close($curl);
                $responseML = json_decode($responseML, true);

                if(!isset($responseML['error'])){
                    $posts = array('success' => '1', 
                                   'message' => 'Inventario modificado exitosamente en ML');    
                }else{
                    $posts = array('success' => '0', 
                                     'error' => 'Error al modificar el inventario en ML');
                }

            }else{
                $posts = array('success' => '0', 
                                 'error' => 'Error al modificar el inventario en ML');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function gestionarOrdenML($body){
        try {
            //ini_set('display_errors', '1');
            $variable = array('ml_id','ml_secretKey', 'ml_authCode', 'ml_token', 'ml_fecToken', 'ml_expires', 'ml_redirectUri');
            foreach ($variable as $key ) {
                $resp = GeneralData::consultarClaveValor($key);
                $dataML[$key] = $resp->valor;
            }
            
            $fecExpira = $dataML['ml_fecToken'] + $dataML['ml_expires'];
            
            if(time() > ($fecExpira) || ($fecExpira - time()) < 300){//si el token expiro o su tiempo de vida es menor de 5 min(300 Seg) se renueva
                $tokenPre = $dataML['ml_token'];
        
                $tokenParam = array('grant_type' => 'refresh_token',
                                    'client_id' => $dataML['ml_id'],
                                    'client_secret' => $dataML['ml_secretKey'],
                                    'refresh_token' => $dataML['ml_authCode']);

                $service_url = 'https://api.mercadolibre.com/oauth/token';
                $curl = curl_init($service_url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $tokenParam);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $responseToken = curl_exec($curl);
                curl_close($curl);

                if($responseToken != null){
                    $responseToken = json_decode($responseToken,true);

                    if(!isset($responseToken['message'])){
                        $dataML['ml_authCode'] = $responseToken['refresh_token'];
                        $dataML['ml_fecToken'] = (time() - 30);
                        $dataML['ml_expires'] = $responseToken['expires_in'];
                        $dataML['ml_token'] = $responseToken['access_token'];

                        GeneralData::actualizarClaveValor('ml_authCode', $dataML['ml_authCode']);
                        GeneralData::actualizarClaveValor('ml_fecToken', $dataML['ml_fecToken']);
                        GeneralData::actualizarClaveValor('ml_expires', $dataML['ml_expires']);
                        GeneralData::actualizarClaveValor('ml_token', $dataML['ml_token']);
                    }
                }

                $tokenPost = $dataML['ml_token']; 

                if($tokenPre == $tokenPost){//hubo una falla en la renovacion del token
                    $dataML['error'] = true;
                }else{
                    $dataML['error'] = false;
                }
            }else{//no hace falta la renovacion ya que el token aun esta activo
                $dataML['error'] = false;
            }

            if(!$dataML['error']){

                $order = 'https://api.mercadolibre.com' . $body['resource'] . '?access_token=' . $dataML['ml_token'];
                
                /*$respOrder = file_get_contents($order);
                $respOrder = json_decode($respOrder, true);*/
                
                $curl = curl_init($order);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);   
                $resp = curl_exec($curl);
                curl_close($curl);
                $respOrder = json_decode($resp, true);
                
                $email = $respOrder['buyer']['email']; 
                $alias = $respOrder['buyer']['nickname'];
                $comprador = $respOrder['buyer']['first_name'] . ' ' . $respOrder['buyer']['last_name'];
                $idMl = $respOrder['order_items'][0]['item']['id'];
                $titulo = $respOrder['order_items'][0]['item']['title']; 
                $cantidad = $respOrder['order_items'][0]['quantity']; 

                $codProd = ProductoData::ObtenerPublicacionProductoXIdML($idMl)->codProducto;

                foreach ($respOrder['order_items'][0]['item']['variation_attributes'] as $atributos) {
                    if(trim(strtoupper($atributos['name'])) == trim(strtoupper('Color - Talla'))){
                        $colorTalla = explode('-', $atributos['value_name']);
                        $color = trim($colorTalla[0]);
                        $talla = trim($colorTalla[1]);
                    }else if($atributos['id'] == 11000){//variacion de color
                        $color = trim($atributos['value_name']);
                    }else if($atributos['id'] == 103000){//variacion de talla
                        $talla = trim($atributos['value_name']);
                    }
                }

                $coloresProd = ProductoData::ObtenerColoresProducto($codProd);
                foreach ($coloresProd as $colores) {
                    if(strtoupper($color) == strtoupper($colores->DescWeb)){
                        $codColor = $colores->CodColor;
                    }
                }

                $tallasColorProd = GeneralData::ObtenerTallasColor($codProd, $codColor);
                foreach ($tallasColorProd as $tallas) {
                    if(strtoupper($talla) == strtoupper($tallas->DescTalla)){
                        $codTalla = $tallas->CodTalla;
                    }
                }                

                $dataOrder = explode('/', $body['resource']);

                $redir = "http://" . $_SERVER['HTTP_HOST'] . "/frontend/pages/facturaMl.php?orden=" . base64_encode($dataOrder[2]) . '&codProd=' . base64_encode($codProd) . '&codColor=' . base64_encode($codColor) . '&codTalla=' . base64_encode($codTalla) . '&cantidad=' . base64_encode($cantidad) . '&email=' . base64_encode($email);

                $bodymail  = '<p> Estimado usuario ' . $comprador . ' (' . $alias . '). </br>';
                $bodymail .= 'Se ha registrado una compra por el art&iacute;culo <bold>' . $titulo . '</bold>.  </br>';
                $bodymail .= 'Haga click en siguiente <a href="' . $redir . '">enlace</a> para completar su informaci&oacute;n de env&iacute;o.<p>';
                $bodymail .= 'correo: ' . $email . '</br>';
                $bodymail .= 'dataML: ' . json_encode($body);
                
                $email = 'chaudaryy@gmail.com';
                //$email = 'bloodshield@gmail.com';
                $send = Util::enviar_correo($email, 'Compra ' . $titulo, $bodymail);
                $send->Send();
				//codProd= 04180131911, codcolor = 0164, codtalla = 00000039, 00000040 
                /*$posts = array('body' => $body,
                			   'dataML' => $dataML,
                			   'idML' => $idMl,
                               'codProd' => $codProd,
                               'codColor' => $codColor,
                               'codTalla' => $codTalla,
                               'cantidad' => $cantidad);*/
            }

        } catch (Exception $e) {
            $error = $e->getMessage();
            exit;
        }
        
        //print_r($posts);
    }
}