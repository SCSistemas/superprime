<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/6/2016
 * Time: 8:39 PM
 */
class UserServices
{

    public function Login($body,$lang)
    {

        try{
        $p_usuario= $body['p_usuario'];
        $p_password=  Util::encrypt($body['p_password']);
        $usuario= UserData::Login($p_usuario,$p_password, 0);
        if (is_null($usuario)) {
            $post_msg = utf8_encode("Usuario o Contraseña errada");
            $posts = array('success' => '0', 'msg' => $post_msg);
        }else{
            $post_msg = $usuario->IdUser;
            $posts = array('success' => '1', 'IdUser' => utf8_encode($post_msg));

        }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }  
    public function LoginBackend($body,$lang)
    {

        try{
        $p_usuario= $body['p_usuario'];
        $p_password=  Util::encrypt($body['p_password']);
        $usuario= UserData::Login($p_usuario,$p_password, 1);
        if (is_null($usuario)) {
            $post_msg = utf8_encode("Usuario o Contraseña errada");
            $posts = array('success' => '0', 'msg' => $post_msg);
        }else{
            $post_msg = $usuario->IdUser;
            $posts = array('success' => '1', 'IdUser' => utf8_encode($post_msg));

        }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function InsertarUsuario($body,$lang)
    {

        try{
        $p_usuario= $body['p_usuario'];
        $p_password= Util::encrypt($body['p_password']);
        if (!filter_var($p_usuario, FILTER_VALIDATE_EMAIL)) {
            $post_msg =$lang['ES']['USUARIO_INVALIDO'];
            $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
        } else{

        $id_user=UserData::ObtenerIdUsuario()->Id_User;
        $usuario= UserData::InsertarUsuario($id_user,$p_usuario,$p_password);
        $persona=UserData::ObtenerPersonaUsuario(UserData::ObtenerUsuario($p_usuario)->IdUser);
        if (!is_null($usuario) && !is_null($persona)){
            $post_msg =$lang['ES']['USUARIO_ENCONTRADO'];
            $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

        }else{
            $post_msg =$lang['ES']['USUARIO_REGISTRADO'];
            $token=Util::ObtenerToken();
            UserData::InsertarToken($token);
            $subject =$lang['ES']['REGISTRO_USUARIO'];
            $bodymail =$lang['ES']['COMPLETAR_REGISTRO'].' <a href='.url_registro.'&token='.$token.'&idusuario='.$id_user.'>enlace</a> '.$lang['ES']['COMPLETAR_REGISTRO_C'];
            $send=Util::enviar_correo($p_usuario, $subject, $bodymail);
            //$send= mail($p_usuario, $subject, $bodymail);
            if ($send->Send()) {
                $posts = array('success' => '1','msg'=>utf8_encode($post_msg));
            } else {
                $email_msg =$lang['ES']['NO_SE_ENVIO'];
                $posts = array('success' => '0','msg'=>utf8_encode($email_msg));
                UserData::EliminarUsuario($id_user);
            }


        }
        }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function InsertarPersona($body,$lang)
    {

        try{
        $p_tipoid= $body['p_tipoid'];
        $p_numid=$body['p_numid'];
        $p_nombre_persona= $body['p_nombre_persona'];
        $p_apellido=$body['p_apellido'];
        $p_fec_nac=$body['p_fec_nac'];
        $p_sexo=$body['p_sexo'];
        $p_id_user=$body['p_id_user'];
            $p_estado=$body['p_estado'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($body['p_estado']);
                foreach ($estados as $estado)
                    $p_destado = $estado->DesEstado;


            } else {
                $p_destado = '';
            }

            $p_ciudad = $body['p_ciudad'];

            if (!empty($p_ciudad)) {
                $ciudades= GeneralData::ObtenerCiudades($p_estado, $p_ciudad);
                foreach ($ciudades as $ciudad)
                    $p_dciudad = $ciudad->DescCiudad;

            } else {
                $p_dciudad = '';
            }


            $p_municipio = $body['p_municipio'];

            if (!empty($p_municipio)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estado, $p_ciudad, $p_municipio);
                foreach ($municipios as $municipio)
                    $p_dmunicipio = $municipio->DescMunicipio;


            } else {
                $p_dmunicipio = '';
            }
        $p_parroquia=$body['p_parroquia'];
        $p_postal=$body['p_postal'];
        $p_calle=$body['p_calle'];
        $p_calledos=$body['p_calledos'];
        $p_tipovi=$body['p_tipovi'];
        $p_nombre=$body['p_nombre'];
        $p_piso=$body['p_piso'];
        $p_apto=$body['p_apto'];
        $p_telcel=$body['p_telcel'];
        $p_telhab=$body['p_telhab'];
        $p_telofi=$body['p_telofi'];
        $p_token=$body['p_token'];
        $p_persona_c=$body['p_persona_c'];
        $valor= UserData::ValidarToken($p_token)->total;
            $latitudu=null;
            $longitudu=null;
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu= $p_destado.', '.$p_dciudad.', '.$p_dmunicipio.', '.$p_calle.', '.$p_calledos;
            $direcciong=Util::ObtenerDireccionGoogle($direccionu);
            $latitudu=$direcciong->results[0]->geometry->location->lat;
            $longitudu=$direcciong->results[0]->geometry->location->lng;


            if ($valor>0) {
                $usuario=UserData::ObtenerUsuarioPorId($p_id_user);
                if(is_null($usuario)){
                    $post_msg = $lang['ES']['USUARIO_INVALIDO'];
                    $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

                } else {
                    $persona=UserData::InsertarPersona($p_tipoid,$p_numid,$p_nombre_persona,$p_apellido,$p_fec_nac,$p_sexo,$p_id_user,$p_persona_c);
                    if (is_null($persona)) {
                        UserData::InsertarDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$p_id_user,'Facturacion','S',$longitudu,$latitudu);
                        $post_msg = $lang['ES']['PERSONA_REGISTRADA'];
                        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
                        UserData::UsarToken($p_token);
                    }else{

                        $post_msg = $lang['ES']['PERSONA_ENCONTRADA'];
                        $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

                    }
                }
            }else{
                $post_msg = $lang['ES']['TOKEN_I'];
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function RecuperarContrasena($body,$lang)
    {
        try{
            $username = $body['p_usuario'];
            $usuario = UserData::ObtenerUsuario($username);
            if (is_null($usuario)) {
                $post_msg = $lang['ES']['NO_SE_ENCUENTRA'];
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
            } else {
                $token=Util::ObtenerToken();
                UserData::InsertarToken($token);
                $subject =$lang['ES']['SUBJ_RECUPERAR_CONTRASENA'];
                $url=url_recuperacion.'&token='.$token.'&idusuario='.$usuario->IdUser;
                $bodymail = $lang['ES']['RECUPERAR_CONTRASENA'].' <a href='.$url.'>aquí</a> '.$lang['ES']['RECUPERAR_CONTRASENA_C'].' <a href='.$url.'>'.$url.'</a><br> '.$lang['ES']['RECUPERAR_CONTRASENA_D'];
                //$send= mail($usuario->Usuario, $subject, $bodymail);
                $send=Util::enviar_correo($usuario->Usuario, $subject, $bodymail);
                //$send= mail($p_usuario, $subject, $bodymail);
                if ($send->Send()) {
                    $post_msg ='OK';
                    $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
                } else {
                    $post_msg =$lang['ES']['NO_SE_ENVIO'];
                    $posts = array('success' => '0','msg'=>utf8_encode($post_msg));
                }
            }

        }catch(Exception $e){
            $error = $e->getMessage();
            $posts = array('success' => '0',  'error' => $error);
        }



        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

    }

    public function CambiarContrasena($body,$lang)
    {

        try{
            $p_id_user= $body['p_id_usuario'];
            $p_contrasena=  Util::encrypt($body['p_contrasena']);
            $p_token=$body['p_token'];
            $valor= UserData::ValidarToken($p_token)->total;
            if ($valor>0) {
                UserData::CambiarContrasena($p_id_user, $p_contrasena);
                UserData::UsarToken($p_token);
                $post_msg = $lang['ES']['CAMBIO_CONTRASENA'];
                $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
            } else{
                $post_msg = $lang['ES']['TOKEN_I'];
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

            }

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ValidarToken($body,$lang)
    {

        try{
            $p_codigo= $body['p_codigo'];
            $valor= UserData::ValidarToken($p_codigo)->total;
            if ($valor>0) {
                $post_msg = $lang['ES']['TOKEN_V'];
                $posts = array('success' => '1', 'msg' => $post_msg);
            }else{
                $post_msg = $lang['ES']['TOKEN_I'];
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function CambiarContrasenaPerfil($body,$lang)
    {
        try{
            $p_usuario= $body['p_usuario'];
            $p_password=  Util::encrypt($body['p_password']);
            $p_npassword=  Util::encrypt($body['p_npassword']);
            $usuario= UserData::Login($p_usuario,$p_password,0);
            if (is_null($usuario)) {
                $post_msg = utf8_encode("Contraseña actual errada");
                $posts = array('success' => '0', 'msg' => $post_msg);
            }else{
                UserData::CambiarContrasena($usuario->IdUser, $p_npassword);
                $post_msg = $lang['ES']['CAMBIO_CONTRASENA'];
                $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));

            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDatosPerfil($body)
    {
        try {

            $id_usuario = $body['p_id_usuario'];
            $persona = UserData::ObtenerPersonaUsuario($id_usuario);
            //$persona->DireccionPred=UserData::ObtenerDireccionPreUsuario($id_usuario);
            $posts['Persona'] = $persona;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ActualizaPersona($body,$lang)
    {

        try{
            $p_tipoid= $body['p_tipoid'];
            $p_numid=$body['p_numid'];
            $p_nombre= $body['p_nombre'];
            $p_apellido=$body['p_apellido'];
            $p_fec_nac=$body['p_fec_nac'];
            $p_sexo=$body['p_sexo'];
            $p_persona_c=$body['p_persona_c'];
            $p_imagen=$body['p_imagen'];
            $p_id_user=$body['p_id_user'];
            $p_image_file=$body['p_image_file'];
            $img=$p_imagen!=''?"/superprime/admin/userImages/".$p_id_user."_".$p_numid.'_'.$p_imagen:"";
            if(UserData::ActualizaPersona($p_tipoid,$p_numid,$p_nombre,$p_apellido,$p_fec_nac,$p_sexo,$p_persona_c,$img,$p_id_user)==0){
                $post_msg = 'Persona a actualizar no existe';
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));

            }else{
                $post_msg = $lang['ES']['PERSONA_ACTUALIZADA'];
                $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
                if ($p_image_file!=''){
                    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $p_image_file));
                    file_put_contents($_SERVER['DOCUMENT_ROOT']."/superprime/admin/userImages/".$p_id_user."_".$p_numid.'_'.$p_imagen, $data);
                }
            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    } 
    public function InsertarDeseo($body,$lang)
    {

        try{
            $p_id_user= $body['p_id_user'];
            $p_cod_producto=$body['p_cod_producto'];
            UserData::InsertarDeseo($p_id_user,$p_cod_producto);
            $post_msg = $lang['ES']['DESEO_REGISTRADO'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function EliminarDeseo($body,$lang)
    {

        try{
            $p_id_user= $body['p_id_user'];
            $p_cod_producto=$body['p_cod_producto'];
            UserData::EliminarDeseo($p_id_user,$p_cod_producto);
            $post_msg = $lang['ES']['DESEO_ELIMINADO'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDirecciones($body)
    {
        try {
            $p_id_user = $body['p_id_user'] == '' ? 'null' : $body['p_id_user'];
            $p_desde = $body['start'];
            $p_hasta = $body['length'];

            $direcciones= UserData::ObtenerDireccionesUsuario($p_id_user,$p_desde, $p_hasta);
            $posts['direcciones']=$direcciones;
            $total = UserData::ContarDirecciones($p_id_user)->total;

            $data = array(
                "draw" => (intval($body['draw'])),
                "recordsTotal" => intval($total),
                "recordsFiltered" => intval($total),
                "data" => array()
            );


            foreach ($direcciones as $direccion) {
                $dataaux = array();
                $dataaux[] = $direccion->Id;
                $dataaux[] = $direccion->CodEstado;
                $dataaux[] = $direccion->CodCiudad;
                $dataaux[] = $direccion->CodMunicipio;
                $dataaux[] = $direccion->CodParroquia;
                $dataaux[] = $direccion->CodPostal;
                $dataaux[] = $direccion->CalleUno;
                $dataaux[] = $direccion->CalleDos;
                $dataaux[] = $direccion->TipoVivienda;
                $dataaux[] = $direccion->Nombre;
                $dataaux[] = $direccion->Piso;
                $dataaux[] = $direccion->Apto;
                $dataaux[] = $direccion->TelCel;
                $dataaux[] = $direccion->TelHab;
                $dataaux[] = $direccion->TelOfi;
                $dataaux[] = $direccion->Prederterminada;
                $dataaux[] = $direccion->Alias;
                $data["data"][] = $dataaux;
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function InsertarDireccion($body,$lang)
    {

        try{
            $p_id_user=$body['p_id_user'];
            $p_estado=$body['p_estado'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($body['p_estado']);
                foreach ($estados as $estado)
                    $p_destado = $estado->DesEstado;


            } else {
                $p_destado = '';
            }

            $p_ciudad = $body['p_ciudad'];

            if (!empty($p_ciudad)) {
                $ciudades= GeneralData::ObtenerCiudades($p_estado, $p_ciudad);
                foreach ($ciudades as $ciudad)
                    $p_dciudad = $ciudad->DescCiudad;

            } else {
                $p_dciudad = '';
            }


            $p_municipio = $body['p_municipio'];

            if (!empty($p_municipio)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estado, $p_ciudad, $p_municipio);
                foreach ($municipios as $municipio)
                    $p_dmunicipio = $municipio->DescMunicipio;


            } else {
                $p_dmunicipio = '';
            }
            $p_parroquia=$body['p_parroquia'];
            $p_postal=$body['p_postal'];
            $p_calle=$body['p_calle'];
            $p_calledos=$body['p_calledos'];
            $p_tipovi=$body['p_tipovi'];
            $p_nombre=$body['p_nombre'];
            $p_piso=$body['p_piso'];
            $p_apto=$body['p_apto'];
            $p_telcel=$body['p_telcel'];
            $p_telhab=$body['p_telhab'];
            $p_telofi=$body['p_telofi'];
            $p_alias=$body['p_alias'];
            $p_prede='N';
            $latitudu=null;
            $longitudu=null;
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu= $p_destado.', '.$p_dciudad.', '.$p_dmunicipio.', '.$p_calle.', '.$p_calledos;
            $direcciong=Util::ObtenerDireccionGoogle($direccionu);
            $latitudu=$direcciong->results[0]->geometry->location->lat;
            $longitudu=$direcciong->results[0]->geometry->location->lng;

            UserData::InsertarDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$p_id_user,$p_alias,$p_prede,$longitudu,$latitudu);
            $post_msg = $lang['ES']['DIRECION_REGISTRADA'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
            
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function EliminarDireccion($body,$lang)
    {

        try{
            $p_id_user= $body['p_id_user'];
            $p_id_direccion=$body['p_id_direccion'];
            if(UserData:: EliminarDireccion($p_id_user,$p_id_direccion)==0){
                $post_msg = $lang['ES']['DIRECION_NOENCONTRADA'];
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
                
            }else{
                
            $post_msg = $lang['ES']['DIRECION_ELIMINADA'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
            }

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ActualizaDireccion($body,$lang)
    {

        try{
            $p_id_user=$body['p_id_user'];
            $p_id_direccion=$body['p_id_direccion'];
            $p_estado=$body['p_estado'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($body['p_estado']);
                foreach ($estados as $estado)
                    $p_destado = $estado->DesEstado;


            } else {
                $p_destado = '';
            }

            $p_ciudad = $body['p_ciudad'];

            if (!empty($p_ciudad)) {
                $ciudades= GeneralData::ObtenerCiudades($p_estado, $p_ciudad);
                foreach ($ciudades as $ciudad)
                    $p_dciudad = $ciudad->DescCiudad;

            } else {
                $p_dciudad = '';
            }


            $p_municipio = $body['p_municipio'];

            if (!empty($p_municipio)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estado, $p_ciudad, $p_municipio);
                foreach ($municipios as $municipio)
                    $p_dmunicipio = $municipio->DescMunicipio;


            } else {
                $p_dmunicipio = '';
            }


            $p_parroquia=$body['p_parroquia'];
            $p_postal=$body['p_postal'];
            $p_calle=$body['p_calle'];
            $p_calledos=$body['p_calledos'];
            $p_tipovi=$body['p_tipovi'];
            $p_nombre=$body['p_nombre'];
            $p_piso=$body['p_piso'];
            $p_apto=$body['p_apto'];
            $p_telcel=$body['p_telcel'];
            $p_telhab=$body['p_telhab'];
            $p_telofi=$body['p_telofi'];
            $p_alias=$body['p_alias'];
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu= $p_destado.', '.$p_dciudad.', '.$p_dmunicipio.', '.$p_calle.', '.$p_calledos;
            $direcciong=Util::ObtenerDireccionGoogle($direccionu);
            $latitudu=$direcciong->results[0]->geometry->location->lat;
            $longitudu=$direcciong->results[0]->geometry->location->lng;

            if(UserData::AcualizaDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$p_alias,$p_id_user,$p_id_direccion,$longitudu,$latitudu)==0){
                $post_msg =$lang['ES']['DIRECION_NOACTULIZADA'];
                $posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
            }else {
            $post_msg = $lang['ES']['DIRECION_ACTUALIZADA'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));}

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ActualizaDireccionPredeterminada($body,$lang)
    {

        try{
            $p_id_user=$body['p_id_user'];
            $p_id_direccion=$body['p_id_direccion'];

            UserData:: ActualizarDireccionPredeterminada($p_id_user,$p_id_direccion);
            $post_msg = $lang['ES']['DIRECION_ACTUALIZADA'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerMenuUsuarioAdmin($body)
    {
        try {

            $id_usuario = $body['p_id_usuario'];
            $menus = UserData::ObtenerMenusUsuario($id_usuario);
            $vmenus  = array();
            foreach ($menus as $menu) {
                $menu->submenus = UserData::ObtenerSubMenus($menu->id);
                $vmenus[] = $menu;
            }
            $posts = array('success' => '0', 'menus' => $vmenus);

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerListaDeseos($body)
    {
        try {

            $id_usuario = $body['p_id_usuario'];
            $productos = UserData::ObtenerListaDeseos($id_usuario);
            $vproductos  = array();
            foreach ($productos as $producto) {
                $imagenes = GeneralData::ObtenerImagenesProducto($producto->codigo_producto,$producto->codcolor);
                $aimagenes = array();
                foreach ($imagenes as $imagen) {
                    $aimagenes[] = $imagen->Ruta;
                }
                $producto->imagenes = $aimagenes;
                $vproductos[] = $producto;
            }
            $posts = array('mi_lista_de_deseo' => $vproductos );

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    
    public function ObtenerListaReservas($body)
    {
        try {

            $id_usuario = $body['p_id_usuario'];
            $p_desde = $body['p_desde'];
            $p_hasta = $body['p_hasta'];
            $p_categoria = $body['p_categoria'] == '' ? 'null' : $body['p_categoria'];
            $ordenes = UserData::ObtenerListaReservas($id_usuario,$p_desde,$p_hasta);
            $vordenes  = array();
            foreach ($ordenes as $orden) {
                $productos = ProductoData::ObtenerProductosPorPedido($orden->CodPedido,$p_categoria);
                $aproductos = array();
                foreach ($productos as $producto) {
                    $aproductos[] = $producto;
                    $imagenes = GeneralData::ObtenerImagenesProducto($producto->codigo_producto,$producto->CodColor);
                    $aimagenes = array();
                    foreach ($imagenes as $imagen) {
                        $aimagenes[] = $imagen->Ruta;
                    }
                    $producto->imagenes = $aimagenes;
                }
                $orden->productos = $aproductos;
                $vordenes[] = $orden;
            }
            $posts = array('mis_reservas' => $vordenes );

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerListaCompras($body)
    {
        try {

            $id_usuario = $body['p_id_usuario'];
            $p_desde = $body['p_desde'];
            $p_hasta = $body['p_hasta'];
            $p_categoria = $body['p_categoria'] == '' ? 'null' : $body['p_categoria'];
            $ordenes = UserData::ObtenerListaCompras($id_usuario,$p_desde,$p_hasta);
            $vordenes  = array();
            foreach ($ordenes as $orden) {
                $productos = ProductoData::ObtenerProductosPorPedido($orden->CodPedido,$p_categoria);
                $aproductos = array();
                foreach ($productos as $producto) {
                    $aproductos[] = $producto;
                    $imagenes = GeneralData::ObtenerImagenesProducto($producto->codigo_producto,$producto->CodColor);
                    $aimagenes = array();
                    foreach ($imagenes as $imagen) {
                        $aimagenes[] = $imagen->Ruta;
                    }
                    $producto->imagenes = $aimagenes;

                }
                $orden->productos = $aproductos;
                $vordenes[] = $orden;
            }
            $posts = array('mis_compras' => $vordenes);

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function InsertarDireccionML($body,$lang)
    {

        try{
            $p_tipoid= $body['p_tipoid'];
            $p_numid=$body['p_numid'];
            $p_nombre_persona= $body['p_nombre_persona'];
            $p_cod_pedido=$body['p_cod_pedido'];
            $p_correo=$body['p_correo'];

            $p_estado=$body['p_estado'];
            $p_ciudad=$body['p_ciudad'];
            $p_municipio=$body['p_municipio'];
            $p_parroquia=$body['p_parroquia'];
            $p_postal=$body['p_postal'];
            $p_calle=$body['p_calle'];
            $p_calledos=$body['p_calledos'];
            $p_tipovi=$body['p_tipovi'];
            $p_nombre=$body['p_nombre'];
            $p_piso=$body['p_piso'];
            $p_apto=$body['p_apto'];
            $p_telcel=$body['p_telcel'];
            $p_telhab=$body['p_telhab'];
            $p_telofi=$body['p_telofi'];

            $p_estadof=$body['p_estadof'];
            $p_ciudadf=$body['p_ciudadf'];
            $p_municipiof=$body['p_municipiof'];
            $p_parroquiaf=$body['p_parroquiaf'];
            $p_postalf=$body['p_postalf'];
            $p_callef=$body['p_callef'];
            $p_calledosf=$body['p_calledosf'];
            $p_tipovif=$body['p_tipovif'];
            $p_nombref=$body['p_nombref'];
            $p_pisof=$body['p_pisof'];
            $p_aptof=$body['p_aptof'];
            $p_telcelf=$body['p_telcelf'];
            $p_telhabf=$body['p_telhabf'];
            $p_telofif=$body['p_telofif'];

            UserData::InsertarDireccionML($p_cod_pedido,$p_nombre_persona,$p_correo,$p_tipoid,$p_numid,$p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,1);
            UserData::InsertarDireccionML($p_cod_pedido,$p_nombre_persona,$p_correo,$p_tipoid,$p_numid,$p_estadof,$p_ciudadf,$p_municipiof,$p_parroquiaf,$p_postalf,$p_callef,$p_calledosf,$p_tipovif,$p_nombref,$p_pisof,$p_aptof,$p_telcelf,$p_telhabf,$p_telofif,2);
            $post_msg = $lang['ES']['PERSONA_REGISTRADA'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ValidaAliasDireccion($body,$lang)
    {

        try{
            $p_id_user= $body['p_id_user'];
            $p_id_direccion= $body['p_id_direccion'] == '' ? 'null' : $body['p_id_direccion'];
            $p_alias= $body['p_alias'];
            $valor= UserData::ValidaAliasDireccion($p_id_user,$p_id_direccion,$p_alias)->total;
            if ($valor>0) {
                $post_msg = $lang['ES']['ALIAS_I'];
                $posts = array('success' => '0', 'msg' => $post_msg);
            }else{
                $post_msg = $lang['ES']['ALIAS_V'];
                $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));

            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDatosUsuarioExpress($body)

    {
        try {
            $p_usuario=$body['p_usuario'];
            $usuario=UserData::ObtenerUsuario($p_usuario);
            if (is_null($usuario)) {
                $posts = array('success' => '0');
            } else {
                $persona=UserData::ObtenerPersonaUsuario($usuario->IdUser);
                $persona->Direcciones=UserData::ObtenerDireccionesUsuario($usuario->IdUser,0,2);
                $posts = array('success' => '1', 'Usuario' => $persona);
            }

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


    public function InsertarEnCarro($body,$lang)
    {

        try {
            $p_cod_producto = $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'];
            $p_cod_talla = $body['p_cod_talla'];
            $p_cantidad = $body['p_cantidad'];
            $p_id_user = $body['p_id_user'];

            $cantidadExistente=UserData::ObtenerProductoEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_id_user)->Cantidad;

            if($cantidadExistente==0) {
                UserData::InsertarEnCarro($p_cod_producto, $p_cod_color, $p_cod_talla, $p_cantidad, $p_id_user);
            }else{
                UserData::ActualizarEnCarro($p_cod_producto, $p_cod_color, $p_cod_talla, $p_cantidad+$cantidadExistente, $p_id_user);
            }
            $post_msg = $lang['ES']['CARRRO_REGISTRADO'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
public function BorrarEnCarro($body,$lang)
    {

        try {
            $p_cod_producto = $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'];
            $p_cod_talla = $body['p_cod_talla'];
            $p_cantidad = $body['p_cantidad'];
            $p_id_user = $body['p_id_user'];
            UserData::BorrarEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_cantidad,$p_id_user);
            $post_msg = $lang['ES']['CARRRO_ELIMINADO'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function EditarEnCarro($body,$lang)
    {

        try {
            $p_cod_producto = $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'];
            $p_cod_talla = $body['p_cod_talla'];
            $p_cantidad = $body['p_cantidad'];
            $p_id_user = $body['p_id_user'];
            UserData::EditarEnCarro($p_cod_producto,$p_cod_color,$p_cod_talla,$p_cantidad,$p_id_user);
            $post_msg = $lang['ES']['CARRRO_EDITADO'];
            $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerCarrito($body)
    {
        try {

            $p_id_usuario= $body['p_id_usuario'];
            $productos = UserData::ObtenerCarrito($p_id_usuario);
            $aproductos = array();
            if (count($productos) > 0) {
                foreach ($productos as $productocarro) {
                    $producto = ProductoData::ObtenerDetalleProductoListado($productocarro->CodProducto);
                    $aimagenes = array();
                    $imagenes = GeneralData::ObtenerImagenesProducto($productocarro->CodProducto,$productocarro->CodColor);
                    foreach ($imagenes as $imagen) {
                        $Oimagen=new stdClass();
                        $Oimagen->nombre= $imagen->Ruta;
                        $Oimagen->color= $imagen->CodColor;
                        $aimagenes[] =$Oimagen;
                    }
                    $producto->ImagenesSelec = $aimagenes;
                    $precios = GeneralData::ObtenerPreciosProducto($productocarro->CodProducto,$productocarro->CodColor);
                    $precio=$precios->PrecioWeb*$productocarro->Cantidad;
                    $iva=$precios->IvaWeb*$productocarro->Cantidad;
                    $producto->Precio =$precio ;
                    $producto->Iva = $iva;
                    $producto->SubTotal = $precio+$iva;
                    $producto->PrecioUnitario =  $precios->PrecioWeb;
                    $producto->Cantidad =  $productocarro->Cantidad;
                    $producto->CodTalla =  $productocarro->CodTalla;
                    $producto->CodColor =  $productocarro->CodColor;
                    $aproductos[] = $producto;
                }

            }
            $posts = array('Productos' => $aproductos);

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerCantidadCarrito($body)

    {
        try {
            $posts = array('Total' =>'('.UserData::ObtenerCantidadCarrito($body['p_id_usuario'])->Total.')');

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerFavoritoUsuario($body)

    {
        try {
            $posts = array('success' => '1','Total' =>UserData::ObtenerFavoritoUsuario($body['p_cod_producto'],$body['p_id_usuario'])->Total);

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }







}