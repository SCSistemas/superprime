<?php

class ProductoServices
{

    public function ObtenerProductos($body)
    {
        try {
            $fecha_desde = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_fecha_desde'] == '' ? 'null' : $body['p_fecha_desde']);
            $fecha_hasta = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_fecha_hasta'] == '' ? 'null' : $body['p_fecha_hasta']);
            $tipo_prod = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_tipo_prod'] == '' ? 'null' : $body['p_tipo_prod']);
            $marca = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_marca'] == '' ? 'null' : $body['p_marca']);
            $genero = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_genero'] == '' ? 'null' : $body['p_genero']);
            $publicado = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_publicado'] == '' ? 'null' : $body['p_publicado']);
            $p_publicado_ml = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'null' : ($body['p_publicado_ml'] == '' ? 'null' : $body['p_publicado_ml']);
            $p_desde = $body['start'];
            $p_hasta = $body['length'];
            $p_cod_prod = $body['p_cod_prod'] == '' ? 'null' : $body['p_cod_prod'];
            $p_desc_sap = $body['p_desc_sap'] == '' ? 'null' : strtoupper($body['p_desc_sap']);
            $p_foto = ($body['ignorados'] == 'S' || $body['p_cod_prod'] != '' || $body['p_desc_sap'] != '') ? 'N' : ($body['p_foto'] == '' ? 'N' : strtoupper($body['p_foto']));
            $p_ignorados = $body['ignorados'] == '' ? 'null' : $body['ignorados'];

            $productos = ProductoData::ObtenerProductos($fecha_desde, $fecha_hasta, $tipo_prod, $marca, $genero, $publicado, $p_desde, $p_hasta, $p_cod_prod, $p_desc_sap, $p_foto, $p_publicado_ml, $p_ignorados);
            $posts['productos'] = $productos;
            $total = ProductoData::ContarProductos($fecha_desde, $fecha_hasta, $tipo_prod, $marca, $genero, $publicado, $p_cod_prod, $p_desc_sap, $p_foto, $p_publicado_ml, $p_ignorados)->total;

            $data = array(
                "draw" => (intval($body['draw'])),
                "recordsTotal" => intval($total),
                "recordsFiltered" => intval($total),
                "data" => array()
            );


            foreach ($productos as $producto) {
                $dataaux = array();
                $dataaux[] = $producto->CodProducto;
                $dataaux[] = $producto->DescMarca;
                $dataaux[] = $producto->DescSap;
                $dataaux[] = $producto->DescTipoProd;
                $dataaux[] = $producto->DescGenero;
                $dataaux[] = $producto->FecSinc;
                $dataaux[] = $producto->Publicado;
                $dataaux[] = $producto->PublicadoMl;
                $dataaux[] = $producto->NoImagenes;
                $dataaux[] = $producto->Ignorado;
                $data["data"][] = $dataaux;
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerProductosSincronizados($body)
    {

        try {
            $id_sinc = $body['p_id_sinc'] == '' ? 'null' : $body['p_id_sinc'];
            $p_desde = $body['start'];
            $p_hasta = $body['length'];
            $productos = ProductoData::ObtenerProductosSincronizados($id_sinc, $p_desde, $p_hasta);
            $posts['productos'] = $productos;
            $total = ProductoData::ContarProductosSincronizados($id_sinc)->total;

            $data = array(
                "draw" => (intval($body['draw'])),
                "recordsTotal" => intval($total),
                "recordsFiltered" => intval($total),
                "data" => array()
            );

            foreach ($productos as $producto) {
                $dataaux = array();
                $dataaux[] = $producto->CodProducto;
                $dataaux[] = $producto->DescMarca;
                $dataaux[] = $producto->DescSap;
                $dataaux[] = $producto->DescTipoProd;
                $dataaux[] = $producto->DescGenero;
                $dataaux[] = $producto->FecSinc;
                $dataaux[] = $producto->Publicado;
                $dataaux[] = $producto->PublicadoMl;
                $data["data"][] = $dataaux;
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    public function ObtenerDetalleProducto($body)
    {
        try {

            $cod_prod = $body['p_cod_prod'] == '' ? 'null' : $body['p_cod_prod'];
            $producto = ProductoData::ObtenerDetalleProducto($cod_prod);
            $producto->ColoresProducto = ProductoData::ObtenerColoresProducto($cod_prod);
            $videos = ProductoData::ObtenerVideosProducto($cod_prod);
            $avideos = array();
            foreach ($videos as $video) {
                $avideos[] = $video->nombre;
            }
            $producto->Videos = $avideos;
            $producto->Caracteristicas = ProductoData::ObtenerCaracProducto($cod_prod);
            $posts['producto'] = $producto;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerDetalleProductoListado($body)
    {
        try {
            $cod_prod = $body['p_cod_prod'] == '' ? 'null' : $body['p_cod_prod'];
            $id_user = $body['p_id_usuario'] == '' ? 'null' : $body['p_id_usuario'];
            $producto = ProductoData::ObtenerDetalleProductoListado($cod_prod);
            $producto->ColoresProducto = ProductoData::ObtenerColoresProducto($cod_prod);

            $colores = ProductoData::ObtenerColoresProductoWeb($cod_prod);
            $acolores = array();
            $aimagenes = array();
            $ccolores = array();
            //Agrego Color, Colores e Imagenes
            foreach ($colores as $color) {
                $acolores[] = $color;
                $ccolores[] = $color->CodColor;

            }
            $imagenes = GeneralData::ObtenerImagenesProducto($cod_prod,$ccolores[0]);
            foreach ($imagenes as $imagen) {
                $Oimagen=new stdClass();
                $Oimagen->nombre= $imagen->Ruta;
                $Oimagen->color= $imagen->CodColor;
                $aimagenes[] =$Oimagen;
            }

            $producto->ImagenesSelec = $aimagenes;
            $producto->Color = $acolores[0];
            $producto->ColorDisp = $acolores;

            $tallas = ProductoData::ObtenerTallasProductoWeb($cod_prod,$ccolores[0]);
            $producto->Talla = $tallas;
            $producto->Caracteristicas = ProductoData::ObtenerCaracProducto($cod_prod);
            $precio = GeneralData::ObtenerPreciosProducto($cod_prod, $ccolores[0])->TotalWeb;
            $cantidad =  ProductoData::ObtenerInventarioTallaColor($cod_prod, $ccolores[0], $tallas[0]->CodTalla)->Total;
            $producto->Precio = $precio;
            $producto->Cantidad = $cantidad;
            $producto->Favorito = UserData::ObtenerFavoritoUsuario($cod_prod,$id_user)->Total;

            $posts = $producto;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDetalleProductoSeleccionado($body)
    {
        try {
            $cod_prod = $body['p_cod_prod'] == '' ? 'null' : $body['p_cod_prod'];
            $cod_color = $body['p_cod_color'] == '' ? 'null' : $body['p_cod_color'];
            $cod_talla = $body['p_cod_talla'] == '' ? 'null' : $body['p_cod_talla'];
            $producto = ProductoData::ObtenerDetalleProductoListado($cod_prod);
            $producto->ColoresProducto = ProductoData::ObtenerColoresProducto($cod_prod);

            $colores = ProductoData::ObtenerColoresProductoWeb($cod_prod);
            $aimagenes = array();

            $imagenes = GeneralData::ObtenerImagenesProducto($cod_prod,$cod_color);
            foreach ($imagenes as $imagen) {
                $Oimagen=new stdClass();
                $Oimagen->nombre= $imagen->Ruta;
                $Oimagen->color= $imagen->CodColor;
                $aimagenes[] =$Oimagen;
            }
            $producto->ImagenesSelec = $aimagenes;
            $producto->Color = $cod_color;
            $producto->ColorDisp =$colores;

            $tallas = ProductoData::ObtenerTallasProductoWeb($cod_prod,$cod_color);
            $producto->Talla = $tallas;
            $producto->Caracteristicas = ProductoData::ObtenerCaracProducto($cod_prod);
            $precio = GeneralData::ObtenerPreciosProducto($cod_prod, $cod_color)->TotalWeb;
            $cantidad =  ProductoData::ObtenerInventarioTallaColor($cod_prod, $cod_color,$cod_talla)->Total;
            $producto->Precio = $precio;
            $producto->Cantidad = $cantidad;
            $posts = $producto;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


    public function ActualizaProducto($body)
    {

        try {
            $producto = $body["producto"];
            $p_cod_prod = $producto['CodProducto'];
            $p_descsap = $producto['DescSap'];
            $p_nombre = $producto['Nombre'];
            $p_modelo = $producto['Modelo'];
            $p_alto = $producto['Alto'];
            $p_ancho = $producto['Ancho'];
            $p_profundidad = $producto['Profundidad'];
            $p_peso = $producto['Peso'];
            $p_brief = $producto['Brief'];
            $p_busquedas = $producto['Busquedas'];
            $p_prime = $producto['Prime'];
            $p_publicado = $producto['Publicado'];
            $p_notificaciones = $producto['Notificaciones'];
            $publicado_ml = $producto['PublicadoML'];
            ProductoData::ActualizaProducto($p_cod_prod, $p_descsap, $p_nombre, $p_modelo, $p_alto, $p_ancho, $p_profundidad,
                $p_peso, $p_brief, $p_busquedas, $p_prime, $p_publicado, $p_notificaciones, $publicado_ml);
            $coloresporducto = $producto["ColoresProducto"];
            if (count($coloresporducto) > 0) {
                foreach ($coloresporducto as $obj) {
                    //Actualizamos Color del Producto
                    ProductoData::ActualizaColorProducto($p_cod_prod, $obj["CodColor"], $obj["DescWeb"], $obj["MostrarWeb"], $obj["ColorPrinc"], $obj["ColorSec"]);

                    //Actualizamos las Tallas
                    $tallas = $obj['tallas'];
                    if (count($tallas) > 0) {
                        foreach ($tallas as $talla)
                            ProductoData::ActualizarColoresTallas($p_cod_prod, $obj["CodColor"], $talla["CodTalla"], $talla["MostrarWeb"]);
                    }
                    //Eliminamos las Imagenes viejas e insertamos las nuevas
                    $imagenes = $obj['ImagenesSelec'];
                    if($obj['ImagenesSelec']){
                    ProductoData::EliminarImagenesColor($p_cod_prod, $obj["CodColor"]);
                    if (count($imagenes) > 0) {
                        foreach ($imagenes as $imagen)
                            ProductoData::InsertarImagenProducto($p_cod_prod, $obj["CodColor"], $imagen);
                     }
                    }
                    //Actualizamos los Precios
                    $precios = $obj['PreciosWeb'];
                    ProductoData::ActualizarPrecios($p_cod_prod, $obj["CodColor"], Util::Getfloat($precios['Precio']), Util::Getfloat($precios['MontoIVA']), Util::Getfloat($precios['Total']), Util::Getfloat($precios['PorcDesc']), Util::Getfloat($precios['MontoDesc']));
                }
            }
            //Eliminamos los videos viejos e insertamos los nuevos
            $videos = $producto['videos'];
            ProductoData::EliminarVideosProducto($p_cod_prod);

            if (count($videos) > 0) {
                foreach ($videos as $video)
                    ProductoData::InsertarVideoProducto($p_cod_prod, $video);

            }

            //Eliminamos las Caracteristicas viejas e insertamos las nuevas
            $caracteristicas = $producto['Caracteristicas'];
            ProductoData::EliminarCaractProducto($p_cod_prod);
            if (count($caracteristicas) > 0) {
                foreach ($caracteristicas as $caracteristica)
                    ProductoData::InsertarCaracProducto($p_cod_prod, $caracteristica);

            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'OK';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function SincronizarProductos()
    {
        try {


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'OK';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg), 'IdSinc' => '1');
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ActualizaInventario($body)
    {

        try {
            $general = new GeneralServices();
            $tiendas = $body["Tiendas"];
            if (count($tiendas) > 0) {
                foreach ($tiendas as $tienda) {
                    $productos = $tienda['Productos'];
                    $cod_tienda = $tienda['CodTienda'];
                    if (count($productos) > 0) {
                        foreach ($productos as $producto) {
                            $desc_talla = substr($body["CodProducto"], -3);
                            $ulti_desc_talla = substr($desc_talla, -1);
                            if($ulti_desc_talla=='0'){
                               $desc_talla = substr($desc_talla, 0, 2);
                            }
                            if(!is_numeric(substr($desc_talla, 0, 1))){
                               $desc_talla = str_replace("0", "", $desc_talla);
                            }  
                            $cod_talla = ProductoData::ObtenerCodigoTalla($desc_talla);                          
                            $cod_color = substr($producto["CodProducto"], 11, -3);
                            $cod_producto = substr($producto["CodProducto"], 0, 11);
                            ProductoData::ActualizaInventario($cod_tienda, $cod_producto, $producto["Cantidad"], $cod_color, $cod_talla);
                            //BUSCO EL INVENTARIO DEL PRODUCTO EN GENERAL y LLAMO AL SERVICIO DE ALEJANDRO
                            $totalprod = ProductoData::ObtenerInventarioTallaColor($cod_producto, $cod_color, $cod_talla)->Total;
                            $body['p_codProducto'] = $cod_producto;
                            $body['p_codColor'] = $cod_color;
                            $body['p_codTalla'] = $cod_talla;
                            $body['p_cantidad'] = $totalprod;
                            //$general->actualizarInventarioML($body);

                        }
                    } else {
                        throw new Exception('El formato del Json está errado');
                    }
                }
            } else {
                throw new Exception('El formato del Json está errado');

            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'OK';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ActualizaPedido($body)
    {

        try {
            $p_cod_tienda = $body["CodTienda"];
            $p_cod_pedido = $body["CodPedido"];
            $desc_talla = substr($body["CodProducto"], -3);
            $ulti_desc_talla = substr($desc_talla, -1);
            if($ulti_desc_talla=='0'){
                 $desc_talla = substr($desc_talla, 0, 2);
            }
            if(!is_numeric(substr($desc_talla, 0, 1))){
                 $desc_talla = str_replace("0", "", $desc_talla);

            }
            $cod_talla = ProductoData::ObtenerCodigoTalla($desc_talla);
            $cod_color = substr($body["CodProducto"], 11, -3);
            $p_cod_producto = substr($body["CodProducto"], 0, 11);
            $p_cantidad = $body["Cantidad"];
            if ($p_cod_pedido == null || $p_cod_producto == null || $p_cantidad == null)
                throw new Exception('El formato del Json está errado');


            if ($p_cantidad == 0) {
                ///BUSCAR OTRA TIENDA Y HACER UPDATE EN LA TABLA DE PEDIDOS PARA ASIGNAR LA ORDEN A LA NUEVA TIENDA

            } else {
                $pedido = ProductoData::ObtenerPedidoProductosTienda($p_cod_pedido, $p_cod_producto, $p_cod_tienda, $cod_color, $cod_talla);
                if (count($pedido) > 0) {
                    /*SI LA CANTIDAD QUE TIENE EL PEDIDO EN LA TABLA ES DIFERENTE A LA QUE MANDÖ LA TIENDA
                      SE ACTUALIZA LA TABLA Y SE BUSCA OTRA TIENDA PARA  EL RESTO */
                    if ($pedido->Cantidad != $p_cantidad) {
                        /*SI LA CANTIDAD DE PRODUCTOS ES MAYOR A LA DEL PEDIDO LANZA UN ERROR */
                        if ($p_cantidad > $pedido->Cantidad) {
                            throw new Exception('La cantidad de producto es mayor a la del pedido');
                        } else if ($p_cantidad < $pedido->Cantidad) {
                            $p_total = $pedido->Cantidad - $p_cantidad;
                            /*SI ES MENORLA CANTIDAD DE PRODUCTOS DE LA TIENDA AL DEL PEDIO
                            HAY QUE BUSCAR EL RESTO EN OTRAS TIENDAS CON ESE TOTAL
                            CON EL MISMO NUMERO DE PEDIDO , PRODUCTO Y $p_total que son los productos que faltaron*/

                        }
                    }
                    ///ESTATUS 2 (Reservado por inventario)
                    $p_estatus = 1;
                    ProductoData::ActualizaPedido($p_cod_pedido, $p_cod_producto, $p_estatus, $p_cantidad, $p_cod_tienda, $cod_color, $cod_talla);

                } else {
                    throw new Exception('El pedido no existe');
                }


            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'Recibido';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg), 'Productos Actualizados' => $p_cantidad);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function NotificaNotaCredito($body)
    {

        try {
            $p_cod_tienda = $body["CodTienda"];
            $p_cod_pedido = $body["CodPedido"];
            $desc_talla = substr($body["CodProducto"], -3);
            $ulti_desc_talla = substr($desc_talla, -1);
            if($ulti_desc_talla=='0'){
              $desc_talla = substr($desc_talla, 0, 2);
            }
            if(!is_numeric(substr($desc_talla, 0, 1))){
              $desc_talla = str_replace("0", "", $desc_talla);
            }  
            $cod_talla = ProductoData::ObtenerCodigoTalla($desc_talla);                          
            $cod_color = substr($body["CodProducto"], 11, -3);
            $p_cod_producto = substr($body["CodProducto"], 0, 11);
            $p_cantidad = $body["Cantidad"];
            $p_estatus = 6;
            if ($p_cod_pedido == null || $p_cod_producto == null || $p_cantidad == null)
                throw new Exception('El formato del Json está errado');
            $pedido = ProductoData::ObtenerPedidoProductosTienda($p_cod_pedido, $p_cod_producto, $p_cod_tienda, $cod_color, $cod_talla);
            if (count($pedido) > 0)
                ProductoData::ActualizaPedido($p_cod_pedido, $p_cod_producto, $p_estatus, $p_cantidad, $p_cod_tienda, $cod_color, $cod_talla);
            else
                throw new Exception('El pedido no existe');
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'Recibido';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function NotificaPedidoEnviado($body)
    {

        try {

            $p_cod_tienda = $body["CodTienda"];//FALATA AGREGAR A ESTE PROCESO
            $p_cod_pedido = $body["CodPedido"];
            $desc_talla = substr($body["CodProducto"], -3);
            $ulti_desc_talla = substr($desc_talla, -1);
            if($ulti_desc_talla=='0'){
              $desc_talla = substr($desc_talla, 0, 2);
            }
            if(!is_numeric(substr($desc_talla, 0, 1))){
              $desc_talla = str_replace("0", "", $desc_talla);
            }  
            $cod_talla = ProductoData::ObtenerCodigoTalla($desc_talla);                          
            $cod_color = substr($body["CodProducto"], 11, -3);
            $p_cod_producto = substr($body["CodProducto"], 0, 11);
            $p_cantidad = $body["Cantidad"];
            $p_estatus = 4;
            if ($p_cod_pedido == null || $p_cod_producto == null || $p_cantidad == null)
                throw new Exception('El formato del Json está errado');

            $pedido = ProductoData::ObtenerPedidoProductosTienda($p_cod_pedido, $p_cod_producto, $p_cod_tienda, $cod_color, $cod_talla);
            if (count($pedido) > 0) {
                if ($pedido->Estatus <> 5) {
                    ProductoData::ActualizaPedido($p_cod_pedido, $p_cod_producto, $p_estatus, $p_cantidad, $p_cod_tienda, $cod_color, $cod_talla);
                    $post_msg = 'Recibido';
                } else {
                    throw new Exception('El envío ya fue notificado');
                }


            } else {
                throw new Exception('El pedido no existe');
            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


    public function NotificaPedidoFacturado($body)
    {

        try {
            $p_cod_tienda = $body["CodTienda"];
            $p_cod_pedido = $body["CodPedido"];
            $desc_talla = substr($body["CodProducto"], -3);
            $ulti_desc_talla = substr($desc_talla, -1);
            if($ulti_desc_talla=='0'){
                 $desc_talla = substr($desc_talla, 0, 2);
            }
            if(!is_numeric(substr($desc_talla, 0, 1))){
                 $desc_talla = str_replace("0", "", $desc_talla);

            }
            $cod_talla = ProductoData::ObtenerCodigoTalla($desc_talla);
            $cod_color = substr($body["CodProducto"], 11, -3);
            $p_cod_producto = substr($body["CodProducto"], 0, 11);
            $p_cantidad = $body["Cantidad"];
            $p_estatus = 4;
            $guia = null;
            $producto = ProductoData::ObtenerDetalleProducto($p_cod_producto);
            $precios = GeneralData::ObtenerPreciosProducto($p_cod_producto, $cod_color);
            $producto->Precios = $precios;
            $producto->Pedido = $p_cod_pedido;
            $producto->Cantidad = $p_cantidad;

            if ($p_cod_pedido == null || $p_cod_producto == null || $p_cantidad == null)
                throw new Exception('El formato del Json está errado');

            $pedido = ProductoData::ObtenerPedidoProductosTienda($p_cod_pedido, $p_cod_producto, $p_cod_tienda, $cod_color, $cod_talla);
            if (count($pedido) > 0) {
                if ($pedido->Estatus <> 4) {
                    ProductoData::ActualizaPedido($p_cod_pedido, $p_cod_producto, $p_estatus, $p_cantidad, $p_cod_tienda, $cod_color, $cod_talla);
                    if ($pedido->Enviar == 'S') {
                        //Buscamos la Direccion
                        if ($pedido->IdUser == 0) {
                            $direccion = UserData::ObtenerDireccionEnvioML($p_cod_pedido);
                        } else {
                            $direccion = UserData::ObtenerDireccionPre($pedido->IdUser);
                            $persona = UserData::ObtenerPersonaUsuario($pedido->IdUser);
                            $direccion->NombrePersona = $persona->Nombre . ' ' . $persona->Apellido;
                            $direccion->NumId = $persona->NumId;

                        }
                        $producto->Direccion = $direccion;
                        ///Consumir servicio JETES para crear guia de envio y retornarla
                        $guia = Util::CrearEnvio($producto);

                    }


                    $post_msg = 'Recibido';
                } else {
                    throw new Exception('El pedido ya fue facturado');
                }


            } else {
                throw new Exception('El pedido no existe');
            }


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg), 'guia' => $guia);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerProductosPorColor($body)

    {
        try {
            $p_cod_prod = $body['p_cod_prod'] == '' ? 'null' : $body['p_cod_prod'];
            $p_cod_color = $body['p_cod_color'] == '' ? 'null' : $body['p_cod_color'];
            $p_cod_talla = $body['p_cod_talla'] == '' ? 'null' : $body['p_cod_talla'];
            $posts['total'] = ProductoData::ObtenerProductosPorColor($p_cod_prod, $p_cod_color, $p_cod_talla)->Cantidad;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerMarcasProducto($body)

    {
        try {
            $p_cod_tipo_prod = $body['p_cod_tipo_prod'];
            $marcas = ProductoData::ObtenerMarcasProducto($p_cod_tipo_prod);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($marcas, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerMarcasListado($body)

    {
        try {

            $p_cod_tipo_prod = $body['p_subcategorias'] == '' ? 0 : $body['p_subcategorias'];
            $marcas = ProductoData::ObtenerMarcasListado($p_cod_tipo_prod);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($marcas, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerGenerosProducto($body)

    {
        try {
            $p_cod_tipo_prod = $body['p_cod_tipo_prod'];
            $generos = ProductoData::ObtenerGenerosProducto($p_cod_tipo_prod);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($generos, JSON_UNESCAPED_UNICODE);
    }

    public function InsertarNoImagen($body)

    {
        try {
            $p_cod_prod = $body['p_cod_prod'];
            $p_cod_color = $body['p_cod_color'];
            ProductoData::InsertarNoImagen($p_cod_prod, $p_cod_color);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'OK';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function EliminarNoImagen($body)

    {
        try {
            $p_cod_prod = $body['p_cod_prod'];
            $p_cod_color = $body['p_cod_color'];
            ProductoData:: EliminarNoImagen($p_cod_prod, $p_cod_color);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'OK';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function PagarProducto($body)
    {

        try {
            $productos = $body["Productos"];
            $aproductos = array();
            if (count($productos) > 0) {
                foreach ($productos as $obj) {
                    $obj['IdPedido'] = '001';
                    $obj['IdCarro'] = '001';
                    $aproductos[] = $obj;
                }
            }
            $posts['Productos'] = $aproductos;


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerCategorias()
    {
        try {


            $categorias = ProductoData::ObtenerCategorias();
            $acategorias = array();
            foreach ($categorias as $categoria) {
                $subcategorias = ProductoData::ObtenerSubCategPorCateg($categoria->Id);
                $categoria->subCategorias = $subcategorias;
                $acategorias[] = $categoria;
            }
            $posts['Categorias'] = $acategorias;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerSubCategoria($body)
    {
        try {

            $p_categoria= $body['Categoria'] == '' ? 0 : $body['Categoria'];
            $subcategorias = ProductoData::ObtenerSubCategPorCateg($p_categoria);
            $posts['SubCategorias'] = $subcategorias;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ActualizaIgnorado($body)

    {
        try {
            $p_cod_prod = $body['p_cod_prod'];
            $p_ignorado = $body['p_ignorado'];
            ProductoData:: ActualizaIgnorado($p_cod_prod, $p_ignorado);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $post_msg = 'OK';
        $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function insertarPublicacion($body)
    {
        try {
            $insertResult = ProductoData::insertarPublicacion($body['p_codProducto'], $body['p_codColor'], $body['p_plan'], $body['p_categorias'], $body['p_idPubML']);
            $updateResult = ProductoData::actualizarPublicadoProducto($body['p_codProducto'], $body['p_publicado']);

            $posts = array('success' => '1', 'idML' => $body['p_idPubML']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function consultarPublicacionProducto($body)
    {
        try {

            $selectResult = ProductoData::consultarPublicacionProducto($body['p_codProducto']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($selectResult, JSON_UNESCAPED_UNICODE);
    }

    public function actualizarPublicacion($body)
    {
        try {
            $insertResult = ProductoData::actualizarPublicacion($body['p_codProducto'], $body['p_codColor'], $body['p_plan'], $body['p_categorias'], $body['p_idPubML']);
            $updateResult = ProductoData::actualizarPublicadoProducto($body['p_codProducto'], $body['p_publicado']);

            $posts = array('success' => '1', 'idML' => $body['p_idPubML']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function GestionarCompraML($body)
    {
        try {

            $selectResult = GeneralData::consultarClaveValor('ml_token');

            $urlOrderDetail = 'https://api.mercadolibre.com' . $body['resource'] . '?access_token=' . $selectResult->valor;
            $respOrderDetail = file_get_contents($urlOrderDetail);
            $respOrderDetail = json_decode($respOrderDetail, true);

            $idMl = $respOrderDetail['order_items']['item']['id'];
            $cantCompra = $respOrderDetail['order_items']['quantity'];
            $fechaCompra = $respOrderDetail['date_closed'];
            $aliasComprador = $respOrderDetail['buyer']['nickname'];
            $emailComprador = $respOrderDetail['buyer']['email'];
            $telfComprador = $respOrderDetail['buyer']['phone']['area_code'] . '-' . $respOrderDetail['buyer']['phone']['number'];
            $nomComprador = $respOrderDetail['buyer']['first_name'] . ' ' . $respOrderDetail['buyer']['last_name'];
            $docComprador = $respOrderDetail['buyer']['doc_type'] . '-' . $respOrderDetail['buyer']['doc_number'];

            $p_cod_prod = $body['p_cod_prod'] == '' ? 'null' : $body['p_cod_prod'];
            $p_cod_color = $body['p_cod_color'] == '' ? 'null' : $body['p_cod_color'];
            $p_cod_talla = $body['p_cod_talla'] == '' ? 'null' : $body['p_cod_talla'];
            $posts['total'] = ProductoData::ObtenerProductosPorColor($p_cod_prod, $p_cod_color, $p_cod_talla)->Cantidad;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerListadoProductos($body)
    {
        try {
            $p_id_categoria = $body['p_id_categoria'] == '' ? 'null' : $body['p_id_categoria'];
            $p_id_subcategoria = $body['p_id_subcategoria'] == '' ? 'null' : $body['p_id_subcategoria'];
            $p_marca = $body['p_marca'] == '' ? 'null' : $body['p_marca'];
            $p_minimo = $body['p_minimo'] == '' ? 'null' : $body['p_minimo'];
            $p_maximo = $body['p_maximo'] == '' ? 'null' : $body['p_maximo'];
            $p_number = $body['p_number'] == '' ? 1 : $body['p_number'];
            $p_num_re = $body['p_num_re'] == '' ? 10 : $body['p_num_re'];
            $p_oferta = $body['p_oferta'] == '' ? 'null' : $body['p_oferta'];
            $p_prime = $body['p_prime'] == '' ? 'null' : $body['p_prime'];
            $p_nombre = $body['p_nombre'] == '' ? 'null' :  strtoupper($body['p_nombre']);
            $p_id_usuario = $body['p_id_usuario']== '' ? 'null' : $body['p_id_usuario'];
            $p_desde = (($p_number-1) * $p_num_re);

            $preciosdisponibles=ProductoData::ObtenerMaxMinPrecios($p_id_categoria, $p_id_subcategoria,$p_marca,$p_oferta,$p_prime,$p_nombre);
            $posts['Maximo']=$preciosdisponibles->Maximo;
            $posts['Minimo']=$preciosdisponibles->Minimo;
            $total=ProductoData::ContarListadoProductos($p_id_categoria, $p_id_subcategoria,$p_marca,$p_minimo,$p_maximo,$p_oferta,$p_prime,$p_nombre)->Total;
            $posts['Total']=$total;
            if ($total <10)
                $p_num_re=$total;
            $productos = ProductoData::ObtenerListadoProductos($p_id_categoria, $p_id_subcategoria,$p_desde,$p_num_re,$p_marca,$p_minimo,$p_maximo,$p_oferta,$p_prime,$p_nombre);
            $vproductos = array();
            foreach ($productos as $producto) {
                $colores = ProductoData::ObtenerColoresProductoWeb($producto->CodProducto);
                $acolores = array();
                $aimagenes = array();
                $ccolores = array();
                //Agrego Color, Colores e Imagenes
                foreach ($colores as $color) {
                    $acolores[] = $color;
                    $ccolores[] = $color->CodColor;



                }

                $imagenes = GeneralData::ObtenerImagenesProducto($producto->CodProducto, $ccolores[0]);
                foreach ($imagenes as $imagen) {
                    $Oimagen=new stdClass();
                    $Oimagen->nombre= $imagen->Ruta;
                    $Oimagen->color= $imagen->CodColor;
                    $aimagenes[] =$Oimagen;
                }
                $producto->ImagenesSelec = $aimagenes;
                $producto->Color = $acolores[0];
                $producto->ColorDisp = $acolores;

                $tallas = ProductoData::ObtenerTallasProductoWeb($producto->CodProducto,$ccolores[0]);
                /* $atallas = array();
                foreach ($tallas as $talla) {
                    $atallas[] = $talla->DescTalla;
                }*/

                $producto->Talla = $tallas;

                $precio = GeneralData::ObtenerPreciosProducto($producto->CodProducto, $ccolores[0])->TotalWeb;
                $cantidad =  ProductoData::ObtenerInventarioTallaColor($producto->CodProducto, $ccolores[0], $tallas[0]->CodTalla)->Total;
                $producto->Precio = $precio;
                $producto->Cantidad = $cantidad;
                $producto->Favorito=UserData::ObtenerFavoritoUsuario($producto->CodProducto,$p_id_usuario)->Total;




                $vproductos[] = $producto;
            }


            $posts['productos'] = $vproductos;


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerTopProductos($body)
    {
        try {
            $p_cod_producto = $body['p_cod_producto'];
            $productos = ProductoData::ObtenerTopProductos($p_cod_producto);
            $vproductos = array();
            foreach ($productos as $producto) {
                $colores = ProductoData::ObtenerColoresProductoWeb($producto->CodProducto);
                $acolores = array();
                $aimagenes = array();
                $ccolores = array();
                //Agrego Color, Colores e Imagenes
                foreach ($colores as $color) {
                    $acolores[] = $color;
                    $ccolores[] = $color->CodColor;
                    $imagenes = GeneralData::ObtenerImagenesProducto($producto->CodProducto, $color->CodColor);
                    foreach ($imagenes as $imagen) {
                        $Oimagen=new stdClass();
                        $Oimagen->nombre= $imagen->Ruta;
                        $Oimagen->color= $imagen->CodColor;
                        $aimagenes[] =$Oimagen;
                    }


                }
                $producto->ImagenesSelec = $aimagenes;
                $producto->Color = $acolores[0];
                $producto->ColorDisp = $acolores;
                $precio = GeneralData::ObtenerPreciosProducto($producto->CodProducto, $ccolores[0])->TotalWeb;
                $producto->Precio = $precio;


                $vproductos[] = $producto;
            }


            $posts['productos'] = $vproductos;


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerPrecioProducto($body)
    {
        try {
            $p_cod_producto = $body['p_cod_producto'] == '' ? 'null' : $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'] == '' ? 'null' : $body['p_cod_color'];
            $precio = GeneralData::ObtenerPreciosProducto($p_cod_producto,$p_cod_color)->TotalWeb;
            $posts['precio']= $precio;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerInventarioTallaColor($body)
    {
        try {
            $p_cod_producto = $body['p_cod_producto'] == '' ? 'null' : $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'] == '' ? 'null' : $body['p_cod_color'];
            $p_cod_talla = $body['p_cod_talla'] == '' ? 'null' : $body['p_cod_talla'];
            $totalprod = ProductoData::ObtenerInventarioTallaColor($p_cod_producto, $p_cod_color, $p_cod_talla)->Total;
            $posts['total']= $totalprod;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function CrearPedidoML($body)
        //MERCADO LIBRE
    {

        try {

            $p_tipoid = $body['p_tipoid'];
            $p_numid = $body['p_numid'];
            $p_nombre_persona = $body['p_nombre_persona'];
            $p_correo = $body['p_correo'];

            $p_estado = $body['p_estado'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($p_estado);
                foreach ($estados as $estado)
                      $p_destado = $estado->DesEstado;


            } else {
                $p_destado = '';
            }

            $p_ciudad = $body['p_ciudad'];

            if (!empty($p_ciudad)) {
                 $ciudades= GeneralData::ObtenerCiudades($p_estado, $p_ciudad);
                foreach ($ciudades as $ciudad)
                    $p_dciudad = $ciudad->DescCiudad;

            } else {
                $p_dciudad = '';
            }


            $p_municipio = $body['p_municipio'];

            if (!empty($p_municipio)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estado, $p_ciudad, $p_municipio);
                foreach ($municipios as $municipio)
                    $p_dmunicipio = $municipio->DescMunicipio;


            } else {
                $p_dmunicipio = '';
            }


            $p_parroquia = $body['p_parroquia'];
            $p_postal = $body['p_postal'];
            $p_calle = $body['p_calle'];
            $p_calledos = $body['p_calledos'];
            $p_tipovi = $body['p_tipovi'];
            $p_nombre = $body['p_nombre'];
            $p_piso = $body['p_piso'];
            $p_apto = $body['p_apto'];
            $p_telcel = $body['p_telcel'];
            $p_telhab = $body['p_telhab'];
            $p_telofi = $body['p_telofi'];

            $p_estadof = $body['p_estadof'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($p_estadof);
                foreach ($estados as $estado)
                    $p_destadof = $estado->DesEstado;


            } else {
                $p_estadof = '';
            }
            $p_ciudadf = $body['p_ciudadf'];
            if (!empty($p_ciudadf)) {
                $ciudades= GeneralData::ObtenerCiudades($p_estadof, $p_ciudadf);
                foreach ($ciudades as $ciudad)
                    $p_dciudadf = $ciudad->DescCiudad;

            } else {
                $p_dciudadf = '';
            }

            $p_municipiof = $body['p_municipiof'];
            if (!empty($p_municipiof)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estadof, $p_ciudadf, $p_municipiof);
                foreach ($municipios as $municipio)
                    $p_dmunicipiof = $municipio->DescMunicipio;


            } else {
                $p_dmunicipiof = '';
            }


            $p_parroquiaf = $body['p_parroquiaf'];
            $p_postalf = $body['p_postalf'];
            $p_callef = $body['p_callef'];
            $p_calledosf = $body['p_calledosf'];
            $p_tipovif = $body['p_tipovif'];
            $p_nombref = $body['p_nombref'];
            $p_pisof = $body['p_pisof'];
            $p_aptof = $body['p_aptof'];
            $p_telcelf = $body['p_telcelf'];
            $p_telhabf = $body['p_telhabf'];
            $p_telofif = $body['p_telofif'];
            $p_envio=$body['p_envio'];

            $p_orden_ml = $body['p_orden_ml'];

            $latitudu = null;
            $longitudu = null;
            $p_ML = 'S';
            $cod_pedido = ProductoData::CrearOrden(0)->intRecordKey;
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu = $p_destado . ', ' . $p_dciudad . ', ' . $p_dmunicipio . ', ' . $p_calle . ', ' . $p_calledos;
            $direccionf = $p_destadof . ', ' . $p_dciudadf . ', ' . $p_dmunicipiof . ', ' . $p_callef . ', ' . $p_calledosf;
            $direcciong = Util::ObtenerDireccionGoogle($direccionu);
            $latitudu = $direcciong->results[0]->geometry->location->lat;
            $longitudu = $direcciong->results[0]->geometry->location->lng;
            // Si tildo el check no creo esta dirección
            if($p_envio=='S')
            UserData::InsertarDireccionML($cod_pedido, $p_nombre_persona, $p_correo, $p_tipoid, $p_numid, $p_estado, $p_ciudad, $p_municipio, $p_parroquia, $p_postal, $p_calle, $p_calledos, $p_tipovi, $p_nombre, $p_piso, $p_apto, $p_telcel, $p_telhab, $p_telofi, 2, $longitudu, $latitudu, $p_orden_ml);

            UserData::InsertarDireccionML($cod_pedido, $p_nombre_persona, $p_correo, $p_tipoid, $p_numid, $p_estadof, $p_ciudadf, $p_municipiof, $p_parroquiaf, $p_postalf, $p_callef, $p_calledosf, $p_tipovif, $p_nombref, $p_pisof, $p_aptof, $p_telcelf, $p_telhabf, $p_telofif, 1, 0, 0, $p_orden_ml);

            $producto = new ProductoData();
            $producto->CodProducto = $body["p_cod_producto"];
            $producto->CodColor = $body["p_cod_color"];
            $producto->Cantidad = $body["p_cantidad"];
            $producto->CodTalla = $body["p_cod_talla"];
            $precios = GeneralData::ObtenerPreciosProducto($body["p_cod_producto"], $body["p_cod_color"]);
            $producto->Precios = $precios;


            $cliente = new GeneralData();
            $cliente->Nombre = $p_nombre_persona;
            $cliente->Direccion = $direccionu;
            $cliente->DireccionF = $direccionf;
            $cliente->Numid = $p_numid;


            //Obtengo la tienda Ganadora Enviando la direccion y el producto
            if (Util::ObtenerTiendaGanadoraML($latitudu, $longitudu,$p_estado,$p_ciudad, $producto, $p_ML, $cod_pedido, $cliente,$p_envio))
                /// ENVIO CORREO O LE DIGO AL FRONT QUE TODO SALIO FINO
                null;
            else
                null;
            //RESPONDO QUE HUBO UN ERROR    ;


        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $posts = array('success' => '1', 'cod_pedido' => $cod_pedido);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function CrearCompraExpress($body,$lang)



        //Compra Express
    {
        try {
            $p_id_dir_f=$body['p_id_dir_f'];
            $p_id_dir_e=$body['p_id_dir_e'];

            $p_estado = $body['p_estado'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($p_estado);
                foreach ($estados as $estado)
                    $p_destado = $estado->DesEstado;


            } else {
                $p_destado = '';
            }

            $p_ciudad = $body['p_ciudad'];

            if (!empty($p_ciudad)) {
                $ciudades= GeneralData::ObtenerCiudades($p_estado, $p_ciudad);
                foreach ($ciudades as $ciudad)
                    $p_dciudad = $ciudad->DescCiudad;

            } else {
                $p_dciudad = '';
            }


            $p_municipio = $body['p_municipio'];

            if (!empty($p_municipio)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estado, $p_ciudad, $p_municipio);
                foreach ($municipios as $municipio)
                    $p_dmunicipio = $municipio->DescMunicipio;


            } else {
                $p_dmunicipio = '';
            }


            $p_parroquia = $body['p_parroquia'];
            $p_postal = $body['p_postal'];
            $p_calle = $body['p_calle'];
            $p_calledos = $body['p_calledos'];
            $p_tipovi = $body['p_tipovi'];
            $p_nombre = $body['p_nombre'];
            $p_piso = $body['p_piso'];
            $p_apto = $body['p_apto'];
            $p_telcel = $body['p_telcel'];
            $p_telhab = $body['p_telhab'];
            $p_telofi = $body['p_telofi'];

            $p_estadof = $body['p_estadof'];
            if (!empty($p_estado)) {
                $estados = GeneralData::ObtenerEstados($p_estadof);
                foreach ($estados as $estado)
                    $p_destadof = $estado->DesEstado;


            } else {
                $p_estadof = '';
            }
            $p_ciudadf = $body['p_ciudadf'];
            if (!empty($p_ciudadf)) {
                $ciudades= GeneralData::ObtenerCiudades($p_estadof, $p_ciudadf);
                foreach ($ciudades as $ciudad)
                    $p_dciudadf = $ciudad->DescCiudad;

            } else {
                $p_dciudadf = '';
            }

            $p_municipiof = $body['p_municipiof'];
            if (!empty($p_municipiof)) {
                $municipios = GeneralData::ObtenerMunicipios($p_estadof, $p_ciudadf, $p_municipiof);
                foreach ($municipios as $municipio)
                    $p_dmunicipiof = $municipio->DescMunicipio;


            } else {
                $p_dmunicipiof = '';
            }





            $p_tipoid = $body['p_tipoid'];
            $p_numid = $body['p_numid'];
            $p_nombre_persona = $body['p_nombre_persona'];
            $p_parroquiaf = $body['p_parroquiaf'];
            $p_postalf = $body['p_postalf'];
            $p_callef = $body['p_callef'];
            $p_calledosf = $body['p_calledosf'];
            $p_tipovif = $body['p_tipovif'];
            $p_nombref = $body['p_nombref'];
            $p_pisof = $body['p_pisof'];
            $p_aptof = $body['p_aptof'];
            $p_telcelf = $body['p_telcelf'];
            $p_telhabf = $body['p_telhabf'];
            $p_telofif = $body['p_telofif'];



            $p_correo= $body['p_correo'];

            $latitudu = null;
            $longitudu = null;
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu = $p_destado . ', ' . $p_dciudad . ', ' . $p_dmunicipio . ', ' . $p_calle . ', ' . $p_calledos;
            $direccionf = $p_destadof . ', ' . $p_dciudadf . ', ' . $p_dmunicipiof . ', ' . $p_callef . ', ' . $p_calledosf;
            $direcciong = Util::ObtenerDireccionGoogle($direccionu);
            $latitudu = $direcciong->results[0]->geometry->location->lat;
            $longitudu = $direcciong->results[0]->geometry->location->lng;

            if(!empty($body['p_id_user'])){
                $id_user=$body['p_id_user'];
                UserData::AcualizaDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,'ENVIO',$id_user,$p_id_dir_e,$longitudu,$latitudu);
                UserData::AcualizaDireccion($p_estadof,$p_ciudadf,$p_municipiof,$p_parroquiaf,$p_postalf,$p_callef,$p_calledosf,$p_tipovif,$p_nombref,$p_pisof,$p_aptof,$p_telcelf,$p_telhabf,$p_telofif,'FACTURACION',$id_user,$p_id_dir_f,$longitudu,$latitudu);
            }else{
            $id_user=UserData::ObtenerIdUsuario()->Id_User;
            $p_password= Util::encrypt('');
            UserData::InsertarUsuario($id_user,$p_correo,$p_password);
            UserData::InsertarPersona($p_tipoid,$p_numid,$p_nombre_persona,'','','',$id_user,'');
            UserData::InsertarDireccion($p_estado,$p_ciudad,$p_municipio,$p_parroquia,$p_postal,$p_calle,$p_calledos,$p_tipovi,$p_nombre,$p_piso,$p_apto,$p_telcel,$p_telhab,$p_telofi,$id_user,'ENVIO','S',$longitudu,$latitudu);
            UserData::InsertarDireccion($p_estadof,$p_ciudadf,$p_municipiof,$p_parroquiaf,$p_postalf,$p_callef,$p_calledosf,$p_tipovif,$p_nombref,$p_pisof,$p_aptof,$p_telcelf,$p_telhabf,$p_telofif,$id_user,'FACTURACION','N',0,0);
            }



            $cod_pedido = ProductoData::CrearOrden($id_user)->intRecordKey;
            $producto = new ProductoData();
            $producto->CodProducto = $body["p_cod_producto"];
            $producto->CodColor = $body["p_cod_color"];
            $producto->Cantidad = $body["p_cantidad"];
            $producto->CodTalla = $body["p_cod_talla"];
            $precios = GeneralData::ObtenerPreciosProducto($body["p_cod_producto"], $body["p_cod_color"]);
            $producto->Precios = $precios;
            $cliente = new GeneralData();
            $cliente->Nombre = $p_nombre_persona;
            $cliente->Direccion = $direccionu;
            $cliente->DireccionF = $direccionf;
            $cliente->Numid = $p_numid;


            //Obtengo la tienda Ganadora Enviando la direccion y el producto
            if (Util::ObtenerTiendaGanadoraCompra($latitudu, $longitudu,$p_estado,$p_ciudad, $producto, 'N', $cod_pedido, $cliente,'S'))
                /// ENVIO CORREO O LE DIGO AL FRONT QUE |TODO SALIO FINO
                null;
            else
                null;
            //RESPONDO QUE HUBO UN ERROR;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $posts = array('success' => '1', 'cod_pedido' => $cod_pedido,'id_user'=>$id_user,'json'=>$body);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function CrearCompraExpressLogin($body,$lang)

        //Compra Express con usuario Logueado
    {
        try {

            $id_user=$body['p_id_user'];
            $direcciones= UserData::ObtenerDireccionesUsuario($id_user,0,20);
            $persona=UserData::ObtenerPersonaUsuario($id_user);

            if (count($direcciones) > 0) {
                foreach ($direcciones as $obj) {
                    if ($obj->Prederterminada=='S'){
                    $estados=GeneralData::ObtenerEstados($obj->CodEstado);
                    foreach ($estados as $estado){
                        $p_destado = $estado->DesEstado;
                        $p_estado=$estado->CodEstado;

                    }

                    $ciudades=GeneralData::ObtenerCiudades($obj->CodEstado,$obj->CodCiudad);
                    foreach ($ciudades as $ciudad) {
                        $p_dciudad = $ciudad->DescCiudad;
                        $p_ciudad = $ciudad->CodCiudad;
                    }

                    $municipios= GeneralData::ObtenerMunicipios($obj->CodEstado,$obj->CodCiudad,$obj->CodMunicipio);
                    foreach ($municipios as $municipio)
                        $p_dmunicipio =$municipio->DescMunicipio;

                        $p_calle=$obj->CalleUno;
                        $p_calledos=$obj->CalleDos;
                    }
                    if ($obj->Alias=='FACTURACION'){
                        $estados=GeneralData::ObtenerEstados($obj->CodEstado);
                        foreach ($estados as $estado){
                            $p_destadof = $estado->DesEstado;
                            $p_estadof=$estado->CodEstado;

                        }

                        $ciudades=GeneralData::ObtenerCiudades($obj->CodEstado,$obj->CodCiudad);
                        foreach ($ciudades as $ciudad) {
                            $p_dciudadf = $ciudad->DescCiudad;
                            $p_ciudadf = $ciudad->CodCiudad;
                        }

                        $municipios= GeneralData::ObtenerMunicipios($obj->CodEstado,$obj->CodCiudad,$obj->CodMunicipio);
                        foreach ($municipios as $municipio)
                            $p_dmunicipiof =$municipio->DescMunicipio;

                        $p_callef=$obj->CalleUno;
                        $p_calledosf=$obj->CalleDos;
                    }

                }

            }
            $latitudu = null;
            $longitudu = null;
            $cod_pedido = ProductoData::CrearOrden($id_user)->intRecordKey;
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu = $p_destado . ', ' . $p_dciudad . ', ' . $p_dmunicipio . ', ' . $p_calle . ', ' . $p_calledos;
            $direccionf = $p_destadof . ', ' . $p_dciudadf . ', ' . $p_dmunicipiof . ', ' . $p_callef . ', ' . $p_calledosf;
            $direcciong = Util::ObtenerDireccionGoogle($direccionu);
            $latitudu = $direcciong->results[0]->geometry->location->lat;
            $longitudu = $direcciong->results[0]->geometry->location->lng;




            $producto = new ProductoData();
            $producto->CodProducto = $body["p_cod_producto"];
            $producto->CodColor = $body["p_cod_color"];
            $producto->Cantidad = $body["p_cantidad"];
            $producto->CodTalla = $body["p_cod_talla"];
            $precios = GeneralData::ObtenerPreciosProducto($body["p_cod_producto"], $body["p_cod_color"]);
            $producto->Precios = $precios;
            $cliente = new GeneralData();
            $cliente->Nombre = $persona->Nombre.' '.$persona->Apellido;
            $cliente->Direccion = $direccionu;
            $cliente->DireccionF = $direccionf;
            $cliente->Numid = $persona->NumId;


            //Obtengo la tienda Ganadora Enviando la direccion y el producto
            if (Util::ObtenerTiendaGanadoraCompra($latitudu, $longitudu,$p_estado,$p_ciudad, $producto, 'N', $cod_pedido, $cliente,'S'))
                /// ENVIO CORREO O LE DIGO AL FRONT QUE |TODO SALIO FINO
                null;
            else
                null;
            //RESPONDO QUE HUBO UN ERROR;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $posts = array('success' => '1', 'cod_pedido' => $cod_pedido,'id_user'=>$id_user);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ProcesarCarro($body)

    {
        try {

            $id_user=$body['id_user'];
            $productos=$body['productos'];
            $direcciones= UserData::ObtenerDireccionesUsuario($id_user,0,20);
            $persona=UserData::ObtenerPersonaUsuario($id_user);

            if (count($direcciones) > 0) {
                foreach ($direcciones as $obj) {
                    if ($obj->Prederterminada=='S'){
                    $estados=GeneralData::ObtenerEstados($obj->CodEstado);
                    foreach ($estados as $estado){
                        $p_destado = $estado->DesEstado;
                        $p_estado=$estado->CodEstado;

                    }

                    $ciudades=GeneralData::ObtenerCiudades($obj->CodEstado,$obj->CodCiudad);
                    foreach ($ciudades as $ciudad) {
                        $p_dciudad = $ciudad->DescCiudad;
                        $p_ciudad = $ciudad->CodCiudad;
                    }

                    $municipios= GeneralData::ObtenerMunicipios($obj->CodEstado,$obj->CodCiudad,$obj->CodMunicipio);
                    foreach ($municipios as $municipio)
                        $p_dmunicipio =$municipio->DescMunicipio;

                        $p_calle=$obj->CalleUno;
                        $p_calledos=$obj->CalleDos;
                    }
                    if ($obj->Alias=='FACTURACION'){
                        $estados=GeneralData::ObtenerEstados($obj->CodEstado);
                        foreach ($estados as $estado){
                            $p_destadof = $estado->DesEstado;
                            $p_estadof=$estado->CodEstado;

                        }

                        $ciudades=GeneralData::ObtenerCiudades($obj->CodEstado,$obj->CodCiudad);
                        foreach ($ciudades as $ciudad) {
                            $p_dciudadf = $ciudad->DescCiudad;
                            $p_ciudadf = $ciudad->CodCiudad;
                        }

                        $municipios= GeneralData::ObtenerMunicipios($obj->CodEstado,$obj->CodCiudad,$obj->CodMunicipio);
                        foreach ($municipios as $municipio)
                            $p_dmunicipiof =$municipio->DescMunicipio;

                        $p_callef=$obj->CalleUno;
                        $p_calledosf=$obj->CalleDos;
                    }

                }

            }
            $latitudu = null;
            $longitudu = null;
            $cod_pedido = ProductoData::CrearOrden($id_user)->intRecordKey;
            ///BUSCO LA LATITUD Y LONGITUD EN GOOGLE CON LA DIRECCION
            $direccionu = $p_destado . ', ' . $p_dciudad . ', ' . $p_dmunicipio . ', ' . $p_calle . ', ' . $p_calledos;
            $direccionf = $p_destadof . ', ' . $p_dciudadf . ', ' . $p_dmunicipiof . ', ' . $p_callef . ', ' . $p_calledosf;
            $direcciong = Util::ObtenerDireccionGoogle($direccionu);
            $latitudu = $direcciong->results[0]->geometry->location->lat;
            $longitudu = $direcciong->results[0]->geometry->location->lng;


            foreach ($productos['producto'] as $obj) {
                $producto = new ProductoData();
                $producto->CodProducto = $obj["CodProducto"];
                $producto->CodColor = $obj["CodColor"];
                $producto->Cantidad = $obj["Cantidad"];
                $producto->CodTalla = $obj["CodTalla"];
                $precios = GeneralData::ObtenerPreciosProducto($body["p_cod_producto"], $body["p_cod_color"]);
                $producto->Precios = $precios;
                $cliente = new GeneralData();
                $cliente->Nombre = $persona->Nombre.' '.$persona->Apellido;
                $cliente->Direccion = $direccionu;
                $cliente->DireccionF = $direccionf;
                $cliente->Numid = $persona->NumId;
                //Obtengo la tienda Ganadora Enviando la direccion y el producto
                if (Util::ObtenerTiendaGanadoraCompra($latitudu, $longitudu,$p_estado,$p_ciudad, $producto, 'N', $cod_pedido, $cliente,'S'))
                    null;
                else
                    null;

                UserData::BorrarEnCarro($obj["CodProducto"],$obj["CodColor"],$obj["CodTalla"],$obj["Cantidad"],$id_user);

            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $posts = array('success' => '1', 'cod_pedido' => $cod_pedido,'id_user'=>$id_user);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }





    public function CrearPedido($body)
    {

        try {

            $productos = $body["Productos"];
            $id_user = $body["id_user"];
            $p_ML = 'N';
            $date = new DateTime();
            $hoy = $date->format('d-m-Y H:i:s');

            //OBTENGO LA DIRECCION DEL USUARIO PARA SABER EN QUE CIUDAD DEBO BUSCAR LAS TIENDAS
            $direccion = UserData::ObtenerDireccionPre($id_user);
            $cod_pedido = ProductoData::ObtenerCodigoPedido()->CodPedido;
            $aproductos = array();
            if (count($productos) > 0) {
                foreach ($productos as $obj) {
                    //Obtengo la tienda Ganadora Enviando la direccion y el producto
                    $tienda = Util::ObtenerTiendaGanadora($direccion, $obj, $p_ML);

                    ProductoData::CrearOrdenPedido($cod_pedido, $id_user, $obj['CodProducto'], $obj['CodColor'], $tienda['codtienda'], $obj['Cantidad'], $tienda['estatus'], $hoy, $obj['CodTalla'], $obj['Enviar']);
                    $obj['CodTienda'] = $tienda['codtienda'];
                    $obj['Estatus'] = $tienda['estatus'];
                    $aproductos[] = $obj;
                }

            }

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        // ENVIAR CORREO
        $posts = array('success' => '1', 'IdPedido' => $cod_pedido, 'Productos' => $aproductos);
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


    public function ValidaOrdenML($body, $lang)
    {

        try {
            $p_orden_ml = $body['p_orden_ml'];
            $valor = ProductoData::ValidaOrdenML($p_orden_ml)->total;
            if ($valor > 0) {
                $post_msg = $lang['ES']['ORDEN_I'];
                $posts = array('success' => '0', 'msg' => $post_msg);
            } else {
                $post_msg = $lang['ES']['ORDEN_V'];
                $posts = array('success' => '1', 'msg' => utf8_encode($post_msg));

            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


    public function ObtenerTallaColor($body)
    {

        try {
            $p_cod_prod = $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'];
            $posts['tallas'] = ProductoData::ObtenerTallasProductoWeb($p_cod_prod,$p_cod_color);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
public function ObtenerImagenesColor($body)
    {

        try {
            $p_cod_prod = $body['p_cod_producto'];
            $p_cod_color = $body['p_cod_color'];
            $posts['CodProd']=$p_cod_prod;
            $posts['imagenes'] = GeneralData::ObtenerImagenesProducto($p_cod_prod,$p_cod_color);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }
    public function ObtenerDetalleCompraExpress($body)
    {

        try {
            $p_cod_pedido = $body['p_cod_pedido'];
            $p_id_user = $body['p_id_user'];
            $cantidad=0;

            $productos=ProductoData::ObtenerDetalleCompraExpress($p_cod_pedido,$p_id_user);
            if (count($productos) > 0) {
                foreach ($productos as $obj) {
                    $cod_producto=$obj->CodProducto;
                    $cod_color=$obj->CodColor;
                    $cod_talla=$obj->CodTalla;
                    $cantidad=$cantidad+ $obj->Cantidad;

                }

            }

            $producto = ProductoData::ObtenerDetalleProductoListado($cod_producto);
            $aimagenes = array();

            $imagenes = GeneralData::ObtenerImagenesProducto($cod_producto,$cod_color);
            foreach ($imagenes as $imagen) {
                $Oimagen=new stdClass();
                $Oimagen->nombre= $imagen->Ruta;
                $Oimagen->color= $imagen->CodColor;
                $aimagenes[] =$Oimagen;
            }
            $producto->ImagenesSelec = $aimagenes;
            $producto->Color = $cod_color;
            $precios = GeneralData::ObtenerPreciosProducto($cod_producto, $cod_color);
            $precio=$precios->PrecioWeb*$cantidad;
            $iva=$precios->IvaWeb*$cantidad;
            $envio=1200;
            $producto->Precio =$precio ;
            $producto->Iva = $iva;
            $producto->SubTotal = $precio+$iva;
            $producto->CostoEnvio = $envio;
            $producto->Total =  $precio+$iva+$envio;
            $producto->Cantidad = $cantidad;
            $direcciones= UserData::ObtenerDireccionesUsuario($p_id_user,0, 2);

            $adirecciones = array();
            if (count($direcciones) > 0) {
                foreach ($direcciones as $obj) {
                     $estados=GeneralData::ObtenerEstados($obj->CodEstado);
                    foreach ($estados as $estado)
                        $obj->DesEstado = $estado->DesEstado;

                    $ciudades=GeneralData::ObtenerCiudades($obj->CodEstado,$obj->CodCiudad);
                    foreach ($ciudades as $ciudad)
                    $obj->DesCiudad = $ciudad->DescCiudad;

                    $municipios= GeneralData::ObtenerMunicipios($obj->CodEstado,$obj->CodCiudad,$obj->CodMunicipio);
                    foreach ($municipios as $municipio)
                    $obj->DesMunicipio =$municipio->DescMunicipio;
                    $adirecciones[] = $obj;
                }

            }
            $producto->Direcciones=$adirecciones;

            $posts = $producto;

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function PagarPedido($body)
    {

        try {

            $id_user = $body["id_user"];
            $cod_pedido = $body["cod_pedido"];

            //Estas son constantes para la autenticacion con Sitef
            $merchant = 'tienda';
            $mUSN = $cod_pedido; //Numero unico y secuencial del comercio. Si el Numero de orden es solo numerico puede ser el mismo. Es un Numero.
            $orderId = $cod_pedido; //Orden de pedido u orden de compra. Es un String.
            $merchantKey = 'EFC2810318F31FA330BE29E96821E48F6271DC6FA150873D'; //Llave proporcionada por Sitef.
            $endpoint = 'https://esitef-homolog-pci.softwareexpress.com.br/e-sitef-hml/Payment2?wsdl'; //Este valor se debe sustituir cuando se haga el pase a produccion

            //Llamada a la funcion. Las variables a continuacion deben venir del formulario

            $ccNumber =$body["numta"]; //'4966 3816 0905 7706';//'5000 0000 0000 0001'; //Numero de la tarjeta de credito. Es un String.
            $cvc =$body["cod"]; //'30123'; //Codigo de seguridad al reverso de la tarjeta. Es un String.
            $ccType = $body["tipo"];//'1'; //Tipo de tarjeta: 1 para visa, 2 para Mastercard. Es un String.
            $monto = $body["monto"];//'10700,00'; //El monto a pagar. Es un String
            $ccFechaExpiracion = $body["fec"];//'12/2016'; //Fecha de expiracion en la tarjeta. Debe ser de la forma mm/aaaa. Es un String.
            $ccNombre = $body["nomta"]; ////El nombre en la tarjeta.
            $tipoCliente =  $body["tipoid"];//'V'; //Tipo de documento de identidad: V, E, J, etc. Es un String.
            $cedulaCliente =  $body["numid"];//'10345657'; //Documento de identidad. Es un String.
            $t = new UtilSitef();
            $responseCode = $t->callSitef($endpoint, $mUSN, $orderId, $merchant, $merchantKey, $ccNumber, $cvc, $ccType, $monto, $ccFechaExpiracion, $ccNombre, $tipoCliente, $cedulaCliente);
            switch ($responseCode) {
                case null:
                    $posts = array('success' => '0', 'Mensaje' => 'Transaccion no aprobada');
                    break;
                case 0:
                    $posts = array('success' => '1', 'Mensaje' => '!Transaccion aprobada!');
                    $productos=ProductoData::ObtenerDetalleCompraExpress($cod_pedido,$id_user);
                    if (count($productos) > 0)
                        foreach ($productos as $obj)
                            ProductoData::ActualizaPedido($cod_pedido,$obj->CodProducto,3,$obj->Cantidad,$obj->CodTienda,$obj->CodColor,$obj->CodTalla);
                    break;
                case 9:
                    $posts = array('success' => '0', 'Mensaje' => 'Fecha de Expiración de la tarjeta no es válida');
                    break;
                case 21:
                    $posts = array('success' => '0', 'Mensaje' => 'Documento de Identidad no válido');
                    break;
                case 255:
                    $posts = array('success' => '0', 'Mensaje' => 'Pago Rechazado / Tarjeta Negada');
                    break;
                default:
                    $posts = array('success' => '0', 'Mensaje' => 'Ha ocurrido un error con el pago, intente mas tarde');
                    break;
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'Mensaje' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function ObtenerDetalleCompraCarro($body)
    {

        try {
            $p_cod_pedido = $body['p_cod_pedido'];
            $p_id_user = $body['p_id_user'];
            $aproductos= array();
            $productos=ProductoData::ObtenerDetalleCompraExpress($p_cod_pedido,$p_id_user);
            $preciototal=0;
            $totaliva=0;
            $totalsub=0;
            $totalenvio=0;
            $totaltotal=0;
            if (count($productos) > 0) {
                foreach ($productos as $obj) {
                    $cod_producto=$obj->CodProducto;
                    $cod_color=$obj->CodColor;
                    $cod_talla=$obj->CodTalla;
                    $cantidad=$obj->Cantidad;
                    $producto = ProductoData::ObtenerDetalleProductoListado($cod_producto);
                    $aimagenes = array();

                    $imagenes = GeneralData::ObtenerImagenesProducto($cod_producto,$cod_color);
                    foreach ($imagenes as $imagen) {
                        $Oimagen=new stdClass();
                        $Oimagen->nombre= $imagen->Ruta;
                        $Oimagen->color= $imagen->CodColor;
                        $aimagenes[] =$Oimagen;
                    }
                    $producto->ImagenesSelec = $aimagenes;
                    $producto->Color = $cod_color;
                    $precios = GeneralData::ObtenerPreciosProducto($cod_producto, $cod_color);
                    $precio=$precios->PrecioWeb*$cantidad;
                    $preciototal=$preciototal+$precio;
                    $iva=$precios->IvaWeb*$cantidad;
                    $totaliva=$totaliva+$iva;
                    $envio=1200;
                    $producto->Precio =$precio ;
                    $producto->Iva = $iva;
                    $subtotal= $precio+$iva;
                    $producto->SubTotal = $subtotal;
                    $totalsub=$totalsub+$subtotal;
                    $totalenvio=$totalenvio+$envio;
                    $producto->CostoEnvio = $envio;
                    $total= $precio+$iva+$envio;
                    $totaltotal=$totaltotal+$total;
                    $producto->Total = $total ;
                    $producto->Cantidad = $cantidad;
                    $aproductos[]=$producto;
                }

            }

            $direcciones= UserData::ObtenerDireccionesUsuario($p_id_user,0, 2);
            $adirecciones = array();
            if (count($direcciones) > 0) {
                foreach ($direcciones as $direccion) {
                    $estados=GeneralData::ObtenerEstados($direccion->CodEstado);
                    foreach ($estados as $estado)
                        $direccion->DesEstado = $estado->DesEstado;

                    $ciudades=GeneralData::ObtenerCiudades($direccion->CodEstado,$direccion->CodCiudad);
                    foreach ($ciudades as $ciudad)
                        $direccion->DesCiudad = $ciudad->DescCiudad;

                    $municipios= GeneralData::ObtenerMunicipios($direccion->CodEstado,$direccion->CodCiudad,$direccion->CodMunicipio);
                    foreach ($municipios as $municipio)
                        $direccion->DesMunicipio =$municipio->DescMunicipio;
                    $adirecciones[] = $direccion;
                }

            }
            $posts['Productos'] = $aproductos;
            $posts['Direcciones']=$adirecciones;
            $posts['Precio']=$preciototal;
            $posts['Iva']=$totaliva;
            $posts['SubTotal']=$totalsub;
            $posts['CostoEnvio']=$totalenvio;
            $posts['Total']=$totaltotal;

        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }



}

?>
