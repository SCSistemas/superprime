<?php

class Executor {

	public static function doit($sql){
       $con = Database::getCon();
		if ($con->connect_error) {
			throw new Exception($con->connect_error);
		}
		return array($con->query($sql),$con->insert_id);
	}
	public static function doitDelorUpdate($sql){
       $con = Database::getCon();
		if ($con->connect_error) {
			throw new Exception($con->connect_error);
		}
		mysqli_query($con,$sql);

		return mysqli_affected_rows($con);

	}
	public static function doitInsert($sql){
       $con = Database::getCon();
		if ($con->connect_error) {
			throw new Exception($con->connect_error);
		}

		mysqli_query($con,$sql);
		return  $con->insert_id;
	}
}
?>